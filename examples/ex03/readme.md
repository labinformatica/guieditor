# Example 03: basic plot

Aspects of interest:

- In order to simplify graphing, a control called "Image" is incorporated, which corresponds to an "axis" element. With this control it is possible to place, within a window, a particular graph.
- It is also possible to add multiple "Image" controls (axis) in order to achieve representations of different functions.
- All the GNU Octave's own functions are available from the applications developed in guiEditor, because the product of guiEditor are script files executed in GNU Octave.
- When axis is used to position graphs, the default value of the axes will not necessarily be the same as after making the graph. In order to show the same scale, it is possible to initialize the control when showing the application window. For this type of initialization tasks, an event has been generated that is not implemented through callbacks. The default action of a dialog window is to execute the set of actions associated with it. The example presented uses this event to initialize the Image_1 control.
This same event can be used to load toolbox, files, or perform initialization tasks as necessary.
