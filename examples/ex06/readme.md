# Example 06: callback control

Aspects of interest:

- A callback function is a function defined in a particular way, which once associated with an event, will be executed when it occurs.
- If we consider the case of a button, when we activate the generateCallBack property, a function is automatically defined and associated with the default event of the control (click in this case).
- There are other events or callbacks that may be of interest in order to achieve a particular behavior. In order to cover this possibility, guiEditor incorporates a control called "callback". When this control is added, the definition of a function is incorporated into the definition of the dialog window to which it belongs. The name of this function can be set through the "functionName" property. The argument or arguments used by this function can be indicated through the "Arguments" property and the implementation of the function will be done by selecting the interest callback control and adding the necessary source code in the code editor window. Because it is an independent function, the indicated argument does not have a particular meaning until the function is invoked. As with any function that is defined.
- The functions defined using the callback control can be associated with different controls (See example a). In this case 4 buttons were linked to the same callback function. Note that automatic callback function generation was not activated. Instead, when the window becomes visible, the callback property of each button is bound to a function defined through the callbak control. In this way, all buttons will perform the same task when clicked on.
- In example a, the callback control is called "cbUpdateCounter", the function defined and associated with it is called "updateCounter" and the argument name of this function is "mainDlg" (coincident with the name of the main window but without a direct link yet).
When the window is created, the link between the buttons and the callback function is done like this:
```
set (mainDlg.Button_1, 'callback', {mainDlg.cbUpdateCounter, mainDlg});
set (mainDlg.Button_2, 'callback', {mainDlg.cbUpdateCounter, mainDlg});
set (mainDlg.Button_3, 'callback', {mainDlg.cbUpdateCounter, mainDlg});
set (mainDlg.Button_4, 'callback', {mainDlg.cbUpdateCounter, mainDlg});
```

In this case, the last argument of the set function indicates the callback to use and the value of the argument. Thus, the identifier of the main window is an argument of the callback function.
- In order to persist information after the completion of the execution of a callback function, the [setappdata](https://octave.org/doc/v5.2.0/Application_002ddefined-Data.html) function is used. This function is part of a set that allows identifiers and values ​​to be linked to figures. So different callbacks can share and / or alter values ​​of interest.
- Example b presents a more significant case of the use of callback control. Figures have a property called "closerequestfcn" linked to the default action to be taken when closing a window. It is similar to the "callback" property of the buttons in example a, but in a different sense. For a figure to be effectively closed, the "closerequestfcn" property will be bound to the "closereq" function. This function will effectively close the window (figure) and free up your resources. If we modify its default behavior so that it invokes another function, we can control the actions to be performed when a window is closed beyond the way in which it was closed (alt + F4, close button or system menu ).
- In example b, because the default value of the "closerequestfcn" property is modified, the window will not close if the closereq function is not invoked. From this it is possible to implement a completion confirmation window for the application. By default, when the window is closed, a callback function will be invoked (onClose in this example). From this function a secondary confirmation window becomes visible. If you choose to end the application, the "closereq" function will be invoked.
- The information between the main window and the secondary window is shared using a variable defined with the global modifier.
- Note that in example b, the link between the callback and the function is done like this:
```
set (mainDlg.figure, "closerequestfcn", {@onClose, mainDlg});
```
This is equivalent to:
```
set (mainDlg.figure, "closerequestfcn", {mainDlg.callBack_1, mainDlg});
```
Where callBack_1 is the name of the callback control. This is so because the "onClose" function is defined at the window level, so it is possible to use it or make the link through the control that has the same value assigned to it.
- Example c integrates several of the previous topics. A child window is used to load the parameter to graph the hat function.
When the "Param" button is pressed, a secondary dialog window becomes visible. Using the setappdata function, the value of the figure identifier in the main window is copied with the name "parentWnd". In this way, it is possible to access information from the application linked to the figure in the main window.
- The secondary window, when the "Apply" button is pressed, stores in the main window the new value of the parameter using the provided value "parentWnd" and the setappdata function.
- When the child window is closed, because the value of the "closerequestfcn" property has been modified, a callback function defined in the main window will be invoked. In this way it is possible to update the graph.
Note how asynchronous behavior is achieved and information is shared between both dialog windows.
- The callback control included in guiEditor is a way to extend its functionalities and cover unforeseen circumstances. From a practical point of view, it makes it possible to implement particular callback functions that are not in common use.
