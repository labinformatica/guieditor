
# Example 04: using function files and toolbox

Aspects of interest:

- This example takes the application developed in Example 2, performing addition and subtraction operations through external script files.
- Script files can be added directly to a project, as many as necessary. Every time a new script is added a new file with a basic function structure is created.
- The structure of the application is analyzed when the project is loaded, making it possible to copy files with functions developed in another application to the fcn folder and it will be recognized as part of the project when it is opened and analyzed.
- Files containing function definitions are added to the project's fcn folder. The path to this folder is automatically added so that to invoke a function it is only necessary to indicate its name.
- The generation of installable packages can be done through the project manager. For this it is necessary that the gzip and tar utilities are installed, running correctly. When creating a toolbox, the entire structure of the application is packed.
- To generate packages it is necessary to include text files with information about it. These files are created with basic information, but can be modified from the editor by clicking on them.
- An application prepared as a package, does not require a guiEditor installed. It can be installed in GNU Octave like any other package, being necessary to load it before executing it.
- The invocation to the application is done through the project name.
