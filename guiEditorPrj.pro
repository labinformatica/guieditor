
TEMPLATE = subdirs

CONFIG += ordered
CONFIG += staticlib
SUBDIRS = \
    QScintilla \
    guiEditor

QScintilla.file = QScintilla_src-2.13.2/src/qscintilla.pro
guiEditor.file = srcGuiEditor/src/srcGuiEditor.pro

guiEditor.depends = QScintilla
