#include "iconchooserwnd.h"
#include "ui_iconchooserwnd.h"
#include <QDir>

iconChooserWnd::iconChooserWnd(QWidget *parent, QStringList imgList, QString imgPath,
                               QString actualIcon) :
    QDialog(parent),
    ui(new Ui::iconChooserWnd)
{
    int k;
    ui->setupUi(this);
    QListWidgetItem * item = nullptr;
    this->imgList = imgList;
    this->imgPath = imgPath;
    this->selIcon = actualIcon;
    for(k = 0; k < imgList.count(); k++){
        ui->lstFileNames->addItem(imgList[k]);
        if(imgList[k] == actualIcon)
            item = ui->lstFileNames->item(k);
    }
    ui->lstFileNames->sortItems();
    ui->lstFileNames->insertItem(0, tr("[None]"));
    if (item == nullptr)
        ui->lstFileNames->setCurrentRow(0);
    else
        ui->lstFileNames->setCurrentItem(item);
}

iconChooserWnd::~iconChooserWnd()
{
    delete ui;
}

QString iconChooserWnd::getSelectedIcon(void){
    return this->selIcon;
}

void iconChooserWnd::on_lstFileNames_itemSelectionChanged()
{
    this->selIcon = ui->lstFileNames->currentItem()->text();
    if(this->selIcon == tr("[None]")){
        ui->labIconPreview->setPixmap(QPixmap());
    }
    else{
      QPixmap pix(this->imgPath + QDir::separator() + ui->lstFileNames->currentItem()->text());
      ui->labWidth->setText(QString::number(pix.width()));
      ui->labHeight->setText(QString::number(pix.height()));
      ui->labIconPreview->setPixmap(pix);
    }
}
