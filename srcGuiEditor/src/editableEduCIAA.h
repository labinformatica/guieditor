/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDITABLEEDUCIAA_H
#define EDITABLEEDUCIAA_H

#include <QLabel>
#include <QMouseEvent>
#include "abstractuicctrl.h"

typedef enum {pinNone, pinOff, pinOn} eduCIAAPinValue;
class editableEduCIAA : public QLabel
{
    Q_OBJECT
private:
    bool underMovement;
    int moveXInit;
    int moveYInit;
    abstractUICCtrl *parentUICtrl;
    QPixmap pixImg;
    eduCIAAPinValue ledOn[7];
    unsigned int dacVal;
    QString serialPortName;
    bool isHidden;
public:
    void setSerialPortName(QString v)       { this->serialPortName = v;      }
    QString getSerialPortName(void)         { return this->serialPortName;   }

    explicit editableEduCIAA(QWidget *parent, abstractUICCtrl *uiCtrl);    
    void setParents(QWidget *parent, abstractUICCtrl *uiCtrl);
    int cordToGrid(int);
    void setHidden(bool isHidden);
    virtual ~editableEduCIAA();

    eduCIAAPinValue getPinValue(int pin);
    void setPinValue(int pin, eduCIAAPinValue v);


    void setDacVal(unsigned int val);
    unsigned int getDacVal(void);
protected:
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void moveEvent(QMoveEvent * event);
    virtual void resizeEvent(QResizeEvent  * event);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void paintEvent(QPaintEvent *e);
signals:
public slots:

};

#endif // editableEduCIAA_H
