/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "configsettings.h"
#include "propertywndclass.h"
#include "ui_propertywndclass.h"
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QTranslator>
#include <QFontDatabase>
#include <QStandardPaths>

configSettings::configSettings()
{
  this->loadCfg();
}

void configSettings::loadCfg()
{
    QByteArray snapPath;
    int countLibPath;
    int i;
    QString pathLib;
    QString prjName;
    QFontDatabase fontDb;
    QSettings cfg("GuiEditor", "GuiEditor");
    snapPath = qgetenv("SNAP");
    if(snapPath.length() > 0){
        this->octavePath = QString(snapPath) + QDir::separator() + "usr/bin/octave";
        this->gzipPath = QString(snapPath) + QDir::separator() + "bin/gzip";
        this->tarPath = QString(snapPath) + QDir::separator() + "bin/tar";
    }
    else{
        this->octavePath = cfg.value("octavePath").toString();
        this->gzipPath = cfg.value("gzipPath", "").toString();
        this->tarPath = cfg.value("tarPath", "").toString();
    }
    this->tempDir = cfg.value("tmpDir", QDir::tempPath()).toString();
    this->language = cfg.value("language", "English").toString();
    this->qt5PlugginPath = cfg.value("qt5PlugginPath", "").toString();
    this->ctrlFont.fromString(cfg.value("controlFont", fontDb.font("Arial", "Light", 10).toString()).toString());
    this->editorFont.fromString(cfg.value("editorFont", fontDb.font("Courier", "Light", 11).toString()).toString());
    this->editorTabSize = cfg.value("editorTabSize", 4).toInt();
    this->forceCodec = cfg.value("forceCodec", true).toBool();
    this->outputCodec = cfg.value("outputCodec", "UTF-8").toString();

    if (QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).length()>0)
        this->workingDir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0];
    else
        this->workingDir = QApplication::applicationDirPath();

    this->workingDir =  cfg.value("workingDir", this->workingDir).toString();

    countLibPath =  cfg.value("pathLibCount", 0).toInt();
    pathLib.clear();
    for(i=0; i < countLibPath; i++)
    {
        pathLib = cfg.value("pathLib" + QString::number(i), "").toString();
        if(pathLib.length() > 0)
          this->libPaths.push_back(pathLib);
    }
    for (i = 0; i < 5; i++){
      prjName = cfg.value("Recent_0" + QString::number(i + 1),"").toString();
      if (prjName != ""){
        this->recentPrj.push_back(prjName);
      }
    }
}

void configSettings::saveCfg()
{
    int i;
    QString prjName;
    QSettings cfg("GuiEditor", "GuiEditor");
    cfg.setValue("octavePath", this->octavePath );
    cfg.setValue("tmpDir", this->tempDir);
    cfg.setValue("language", this->language);
    cfg.setValue("pathLibCount", this->libPaths.count());
    cfg.setValue("qt5PlugginPath", this->qt5PlugginPath);
    cfg.setValue("gzipPath", this->gzipPath);
    cfg.setValue("tarPath", this->tarPath);

    cfg.setValue("controlFont", this->ctrlFont.toString());
    cfg.setValue("editorFont", this->editorFont.toString());
    cfg.setValue("editorTabSize", this->editorTabSize);

    cfg.setValue("forceCodec", this->forceCodec);
    cfg.setValue("outputCodec", this->outputCodec);

    cfg.setValue("workingDir", this->workingDir);

    for(i=0; i < this->libPaths.count(); i++)
        cfg.setValue("pathLib" + QString::number(i), this->libPaths[i]);

    for (i = 0; i < this->recentPrj.count(); i++){
      cfg.setValue("Recent_0"+QString::number(i+1), this->recentPrj[i]);
    }
}

bool configSettings::octavePathOk()
{
    return QFile::exists(octavePath);
}

bool configSettings::tmpDirOk()
{
    QDir qDirTmp(tempDir);
    return qDirTmp.exists();
}

bool configSettings::gzipPathOk()
{
    return QFile::exists(this->gzipPath);
}

bool configSettings::tarPathOk()
{
    return QFile::exists(this->tarPath);
}

void configSettings::paramToWnd(propertyWndClass *wnd)
{
    int i;
    QByteArray snapPath;
    wnd->ui->labOctavePath->setText(this->octavePath);
    wnd->ui->labOctavePath->setToolTip(this->octavePath);
    wnd->ui->labTempDir->setText(this->tempDir);
    wnd->ui->labTempDir->setToolTip(this->tempDir);

    wnd->ui->labQt5Pluggin->setText(this->qt5PlugginPath);
    wnd->ui->labQt5Pluggin->setToolTip(this->qt5PlugginPath);

    wnd->ui->labGZipPath->setText(this->getGzipPath());
    wnd->ui->labGZipPath->setToolTip(this->getGzipPath());

    wnd->ui->labTarPath->setText(this->getTarPath());
    wnd->ui->labTarPath->setToolTip(this->getTarPath());

    wnd->ui->labFontCtrl->setText(this->getCtrlFont().family() + ", " + QString::number(this->getCtrlFont().pointSize()));
    wnd->ui->labSrcFont->setText(this->getEditorFont().family() + ", " + QString::number(this->getEditorFont().pointSize()));

    wnd->ui->sbTabSize->setValue(this->getEditorTabSize());

    wnd->editor = this->getEditorFont();
    wnd->ctrls  = this->getCtrlFont();

    wnd->ui->labWorkingDir->setToolTip(this->workingDir);

    if (this->workingDir.length() > 30)
        wnd->ui->labWorkingDir->setText(this->workingDir.left(30) + "...");
    else
        wnd->ui->labWorkingDir->setText(this->workingDir);

    if(this->language == "Spanish")
        wnd->ui->cmbLen->setCurrentIndex(0);
    else if(this->language == "English")
        wnd->ui->cmbLen->setCurrentIndex(1);

    for(i=0; i < this->libPaths.count(); i++)
        wnd->ui->lstPathLib->addItem(this->libPaths[i]);
    wnd->ui->cmbOutputCodec->addItem("Big5");
    wnd->ui->cmbOutputCodec->addItem("Big5-HKSCS");
    wnd->ui->cmbOutputCodec->addItem("CP949");
    wnd->ui->cmbOutputCodec->addItem("EUC-JP");
    wnd->ui->cmbOutputCodec->addItem("EUC-KR");
    wnd->ui->cmbOutputCodec->addItem("GB18030");
    wnd->ui->cmbOutputCodec->addItem("HP-ROMAN8");
    wnd->ui->cmbOutputCodec->addItem("IBM 850");
    wnd->ui->cmbOutputCodec->addItem("IBM 866");
    wnd->ui->cmbOutputCodec->addItem("IBM 874");
    wnd->ui->cmbOutputCodec->addItem("ISO 2022-JP");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-1");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-2");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-3");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-4");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-5");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-6");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-7");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-8");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-9");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-10");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-13");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-14");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-15");
    wnd->ui->cmbOutputCodec->addItem("ISO 8859-16");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Bng");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Dev");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Gjr");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Knd");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Mlm");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Ori");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Pnj");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Tlg");
    wnd->ui->cmbOutputCodec->addItem("Iscii-Tml");
    wnd->ui->cmbOutputCodec->addItem("KOI8-R");
    wnd->ui->cmbOutputCodec->addItem("KOI8-U");
    wnd->ui->cmbOutputCodec->addItem("Macintosh");
    wnd->ui->cmbOutputCodec->addItem("Shift-JIS");
    wnd->ui->cmbOutputCodec->addItem("TIS-620");
    wnd->ui->cmbOutputCodec->addItem("TSCII");
    wnd->ui->cmbOutputCodec->addItem("UTF-8");
    wnd->ui->cmbOutputCodec->addItem("UTF-16");
    wnd->ui->cmbOutputCodec->addItem("UTF-16BE");
    wnd->ui->cmbOutputCodec->addItem("UTF-16LE");
    wnd->ui->cmbOutputCodec->addItem("UTF-32");
    wnd->ui->cmbOutputCodec->addItem("UTF-32BE");
    wnd->ui->cmbOutputCodec->addItem("UTF-32LE");
    wnd->ui->cmbOutputCodec->addItem("Windows-1250");
    wnd->ui->cmbOutputCodec->addItem("Windows-1251");
    wnd->ui->cmbOutputCodec->addItem("Windows-1252");
    wnd->ui->cmbOutputCodec->addItem("Windows-1253");
    wnd->ui->cmbOutputCodec->addItem("Windows-1254");
    wnd->ui->cmbOutputCodec->addItem("Windows-1255");
    wnd->ui->cmbOutputCodec->addItem("Windows-1256");
    wnd->ui->cmbOutputCodec->addItem("Windows-1257");
    wnd->ui->cmbOutputCodec->addItem("Windows-1258");
    wnd->ui->cmbOutputCodec->setCurrentText(this->outputCodec);

    wnd->ui->cbForceCodec->setChecked(this->forceCodec);
    snapPath = qgetenv("SNAP");
    if(snapPath.length() > 0){
        wnd->ui->btnChangeOctavePath->setEnabled(false);
        wnd->ui->btnTarPath->setEnabled(false);
        wnd->ui->btnGZipPath->setEnabled(false);
    }
}

void configSettings::wndToParam(propertyWndClass *wnd)
{
    int i;
    bool mustRestart = false;
    this->octavePath = wnd->ui->labOctavePath->text();
    this->tempDir = wnd->ui->labTempDir->text();
    this->qt5PlugginPath = wnd->ui->labQt5Pluggin->text();
    this->tarPath = wnd->ui->labTarPath->text();
    this->gzipPath = wnd->ui->labGZipPath->text();
    this->editorTabSize = wnd->ui->sbTabSize->value();
    mustRestart = (this->editorFont != wnd->editor) || (this->ctrlFont != wnd->ctrls);
    this->editorFont = wnd->editor;
    this->ctrlFont = wnd->ctrls;
    this->workingDir = wnd->ui->labWorkingDir->toolTip();
    if(mustRestart)
        QMessageBox::information(NULL, QObject::tr("Configuration changed"), QObject::tr("To apply the selected fonts, guiEditor must be restart. Please, save your work and restart it."));

    if(wnd->ui->cmbLen->currentIndex() == 0)
    {
        if(this->language != "Spanish")
            QMessageBox::information(NULL,QObject::tr("Cambio de configuración"), QObject::tr("El idioma será actualizado la próxima vez que inicie la aplicación"));
        this->language = "Spanish";
    }
    else if(wnd->ui->cmbLen->currentIndex() == 1)
    {
        if(this->language != "English")
            QMessageBox::information(NULL,QObject::tr("Cambio de configuración"), QObject::tr("El idioma será actualizado la próxima vez que inicie la aplicación"));
        this->language = "English";
    }
    this->libPaths.clear();    
    for(i=0; i < wnd->ui->lstPathLib->count(); i++)
        this->libPaths.push_back(wnd->ui->lstPathLib->item(i)->text());
    this->outputCodec = wnd->ui->cmbOutputCodec->currentText();
    this->forceCodec = wnd->ui->cbForceCodec->isChecked();
}

int configSettings::getEditorTabSize(){
    return this->editorTabSize;
}

QString configSettings::getQt5PlugginPath()
{
    return this->qt5PlugginPath;
}

QFont configSettings::getCtrlFont()
{
    return this->ctrlFont;
}

QFont configSettings::getEditorFont()
{
    return this->editorFont;
}

QStringList configSettings::getLibPaths()
{
    return QStringList(this->libPaths);
}

QString configSettings::getOctavePath()
{
    return this->octavePath;
}

QString configSettings::getTempDir()
{
    return this->tempDir;
}
QString configSettings::getLanguage()
{
    return this->language;
}

QString configSettings::getWorkingDir(void){
    return this->workingDir;
}

void configSettings::setWorkingDir(QString wd){
    this->workingDir = wd;
}

QString configSettings::getGzipPath()
{
    return this->gzipPath;
}

QString configSettings::getTarPath()
{
    return this->tarPath;
}

void configSettings::registerRecentPrj(QString prjPath){
    QString temp;
    int k;
    if (QDir::separator() != "/")
        prjPath = prjPath.replace("\\", "/");
    int pos = this->recentPrj.indexOf(prjPath);
    if(pos != -1){
        for (k = pos; k > 0; k--){
            this->recentPrj[k] = this->recentPrj[k - 1];
        }
        this->recentPrj[0] = prjPath;
    }
    else{
        if (this->recentPrj.count() == 5){
          for (k = 4; k > 0; k--){
            this->recentPrj[k] = this->recentPrj[k - 1];
          }
          this->recentPrj[0] = prjPath;
        }
        else
            this->recentPrj.push_front(prjPath);
    }
}

QString configSettings::getRecentPrj(int prjNumber){
    QString prjName="";
    if (prjNumber <= this->recentPrj.count() - 1)
        prjName = this->recentPrj[prjNumber];
    return prjName;
}

int configSettings::getRecentPrjCount(void){
    return this->recentPrj.count();
}

bool configSettings::getForceOutputCodec(void){
    return this->forceCodec;
}

QString configSettings::getOutputCodec(void){
    return this->outputCodec;
}

configSettings::~configSettings()
{
    this->libPaths.clear();
}
