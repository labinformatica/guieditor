/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ARDUNOPRGWND_H
#define ARDUNOPRGWND_H

#include <QDialog>
#include "editableANano.h"
#include "prgArduino/commanager.h"

namespace Ui {
class ardUnoPrgWnd;
}

class ardUnoPrgWnd : public QDialog
{
    Q_OBJECT

public:
    explicit ardUnoPrgWnd(editableANano *edNano, QWidget *parent = 0);
    ~ardUnoPrgWnd();

private slots:
    void on_btnClose_clicked();

    void on_btnProgram_clicked();

    void on_btnRefresh_clicked();
public slots:
    void procUpdateAct(int doIt, int count);
    void procUpdateError(QString);
    void updateStateCmd(QString);

private:
    Ui::ardUnoPrgWnd *ui;
    editableANano * edNano;
    comManager * ardProgrammer;
};

#endif // ARDUNOPRGWND_H
