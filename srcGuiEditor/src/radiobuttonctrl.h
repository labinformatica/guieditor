/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef RADIOBUTTONCTRL_H
#define RADIOBUTTONCTRL_H

#include <QWidget>
#include "abstractuicctrl.h"
#include <QRadioButton>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"


class rbCheckPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    rbCheckPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void rbCheckedChanged(QString text);
};

class rbTextPropEditor: public abstractPropEditor
{
public:
    rbTextPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QRadioButton *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal)
    {        
        //QFontMetrics fm(((QRadioButton *)this->ctrl->associatedWidget())->font());
        ((QRadioButton *)this->ctrl->associatedWidget())->setText(newVal);
        //((QRadioButton *)this->ctrl->associatedWidget())->resize(fm.width(newVal), fm.height());

    }

    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};

class rbBgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    rbBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class rbFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    rbFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class radioButtonCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

public:
    static unsigned int getNameCounter();
    radioButtonCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg *parentWnd);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    QStringList createCallBack(QString path);
    virtual QString className() { return "radioButtonCtrl";}
    virtual bool isAutoSize();
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~radioButtonCtrl();
};

class rbCtrlGen: public controlGenerator
{
public:
  rbCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg *dlg);
};

#endif // RADIOBUTTONCTRL_H


