/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "guiproject.h"
#include <QMenu>
#include <QMessageBox>
#include <QObject>
#include <QMdiSubWindow>
#include "newfilewnd.h"
#include "ui_newfilewnd.h"
#include "mainwnd.h"
#include "ui_mainwnd.h"

#include "childwnddlg.h"
#include "childwndmfile.h"
#include "childwndimg.h"
#include "childwndtextfile.h"

#include "ui_mfileeditorwidget.h"
#include "mfileeditorwidget.h"

#include "framedlg.h"
#include <QDir>

#include <QFile>
#include <QTextStream>
#include "storagemanager.h"

#include <QMessageBox>

#include "prjpropwnd.h"
#include "ui_prjpropwnd.h"

#include <QFileDialog>
#include <QStandardPaths>
#include <QDateTime>

#include "pkggenwnd.h"
#include "ui_pkggenwnd.h"

#include "runargswnd.h"
#include "ui_runargswnd.h"

treeItemProject::treeItemProject(guiProject * p, QTreeWidget * parent):
    abstractPrjTreeItem(parent)
{
    this->setText(0, p->name());
    this->setIcon(0, QPixmap(":/img/projectItem"));
    this->prj = p;
    this->prj->setTreeItem(this);
}

void treeItemProject::activate()
{

}

void treeItemProject::populateMenu(QMenu &m)
{
    m.addAction(tr("Add a new dialog"), this, SLOT(actAddNewDlg(bool)));
    m.addAction(tr("Add a new m file"), this, SLOT(actAddNewMFile(bool)));
    m.addAction(tr("Add a new image"), this, SLOT(actAddNewImage(bool)));
    m.addSeparator();
    m.addAction(tr("Export project"), this, SLOT(actExport(bool)));
    m.addAction(tr("Generate octave package"), this, SLOT(actGenPkg(bool)));
    m.addSeparator();
    m.addAction(tr("Save project"), this, SLOT(actSave(bool)));
    m.addAction(tr("Close project"), this, SLOT(actClose(bool)));
    m.addSeparator();
    m.addAction(tr("Run project"), this, SLOT(actRun(bool)));
    m.addAction(tr("Set run arguments"), this, SLOT(actRunArguments(bool)));
    m.addSeparator();
    m.addAction(tr("Properties"), this, SLOT(actProjectProperties(bool)));
}

void treeItemProject::selectProject()
{
    mainWnd::getPrjWnd()->setActiveProject(this->prj);
}

void treeItemProject::actAddNewDlg(bool checked __attribute__((unused)))
{
    newFileWnd wnd(this->prj);
    if(wnd.exec() == QDialog::Accepted)
    {
      childWndDlg * dlg = new childWndDlg(mainWnd::getMainWnd(),
                                    0,
                                    mainWnd::getSrcWnd(),
                                    mainWnd::getPropPan(), this->prj);

      dlg->getFrame()->setCtrlName(wnd.ui->leFileName->text());
      dlg->save(this->prj->guiPath() + QDir::separator() + wnd.ui->leFileName->text() + ".xml");

      mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
      dlg->setGeometry(1, 1, 250, 100);
      dlg->show();
      dlg->setWindowModified(false);
      dlg->getFrame()->updateSrcWndCtrls();
      dlg->getFrame()->setCanChangeName(false);
      abstractPrjTreeItem * dlgItem = dlg->treeItemWidget(this);
      this->addChild(dlgItem);
      this->setExpanded(true);
      if(!this->prj->getMainDlg())
      {
          this->prj->setMainDlg(dlg);
          dlgItem->setText(0, dlgItem->text(0) + "[*]");
          this->prj->save();
      }
      this->prj->items.push_back(dlg);
    }
}

void treeItemProject::actAddNewMFile(bool checked __attribute__((unused)))
{
    QString fcnTemp;
    newFileWnd wnd(this->prj);
    if(wnd.exec() == QDialog::Accepted)
    {
      childWndMFile * dlg = new childWndMFile(mainWnd::getMainWnd(), 0, this->prj);
      dlg->setWindowModified(false);
      dlg->save(this->prj->fcnPath() + QDir::separator() + wnd.ui->leFileName->text() + ".m");
      mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
      QFile f(":/template/templates/fcn.m");
      f.open(QIODevice::ReadOnly);
      fcnTemp = f.readAll();
      f.close();
      fcnTemp.replace("<fcnName>",
                      wnd.ui->leFileName->text());
      dlg->getSrcEditor()->ui->srcEditor->setText(fcnTemp);

      dlg->setGeometry(1, 1, 450, 250);
      dlg->show();
      abstractPrjTreeItem * dlgItem = dlg->treeItemWidget(this);
      this->addChild(dlgItem);
      this->setExpanded(true);
      this->prj->items.push_back(dlg);
    }
}

void treeItemProject::actAddNewImage(bool checked __attribute__((unused)))
{
    QString basePath;
    if(QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).count() > 0)
        basePath = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0];
    else
        basePath = QApplication::applicationDirPath();
    QString imgFile = QFileDialog::getOpenFileName(mainWnd::getMainWnd(),
                                                   tr("Select image to add"),
                                                   basePath,
                                                   tr("PNG Files (*.png);;SVG Files (*.svg);;All files (*.*);;"));
    QString outFile;
    if(!imgFile.isNull())
    {
        QFileInfo fileInfo(imgFile);
        outFile = this->getPrj()->imgPath() + QDir::separator() + fileInfo.fileName();
        childWndImg * dlg = new childWndImg(mainWnd::getMainWnd(), 0, this->prj);
        QFile::copy(imgFile, outFile);
        dlg->load(outFile);
        dlg->setWindowModified(false);
        dlg->setWindowTitle(dlg->getName());
        mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
        dlg->setGeometry(1, 1, 450, 250);
        dlg->show();
        abstractPrjTreeItem * dlgItem = dlg->treeItemWidget(this);
        this->addChild(dlgItem);
        this->setExpanded(true);
        this->prj->items.push_back(dlg);
    }
}

void treeItemProject::actRun(bool checked __attribute__((unused)))
{
    this->prj->run();
}

void treeItemProject::actRunArguments(bool checked __attribute__((unused))){

    runArgsWnd args(0);
    args.setArguments(this->prj->runArguments());
    if(args.exec() == QDialog::Accepted)
      this->prj->setRunArguments(args.getArguments());
}

void treeItemProject::actExport(bool checked __attribute__((unused)))
{
    this->prj->exportPrj();
}

void treeItemProject::actGenPkg(bool checked __attribute__((unused)))
{
    this->prj->generatePkg();
}

void treeItemProject::actProjectProperties(bool checkded __attribute__((unused)))
{
    int vA, vB, vC;
    prjPropWnd wnd(mainWnd::getMainWnd());

    wnd.ui->cbDefBasePath->setChecked(this->prj->defBasePath());
    wnd.ui->cbDefBasePath->setText(tr("Define ") + "_" + this->prj->name() + "BasePath");

    wnd.ui->cbDefImgPath->setChecked(this->prj->defImgPath());
    wnd.ui->cbDefImgPath->setText(tr("Define ") + "_" + this->prj->name() + "ImgPath");
    this->prj->version(vA, vB, vC);
    wnd.ui->sbVerA->setValue(vA);
    wnd.ui->sbVerB->setValue(vB);
    wnd.ui->sbVerC->setValue(vC);
    if(wnd.exec())
    {
        this->prj->setDefBasePath(wnd.ui->cbDefBasePath->isChecked());
        this->prj->setDefImgPath(wnd.ui->cbDefImgPath->isChecked());
        vA = wnd.ui->sbVerA->value();
        vB = wnd.ui->sbVerB->value();
        vC = wnd.ui->sbVerC->value();
        this->prj->setVersion(vA, vB, vC);
    }
}

void treeItemProject::actSave(bool checked __attribute__((unused)))
{
    this->prj->save();
}

void treeItemProject::actClose(bool checked __attribute__((unused)))
{
    mainWnd::getPrjWnd()->closeActiveProject();
}

void guiProject::copyFolder(QString src, QString dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyFolder(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

guiProject::guiProject(QString path, QString name)
{
    vPath = path;
    vName = name;
    treeItem = NULL;
    QDir().mkdir(path);
    QDir prjPath(path);
    if(!prjPath.exists("export"))
        prjPath.mkdir("export");
    if(!prjPath.exists("fcn"))
        prjPath.mkdir("fcn");
    if(!prjPath.exists("gui"))
        prjPath.mkdir("gui");
    if(!prjPath.exists("img"))
        prjPath.mkdir("img");
    if(!prjPath.exists("other"))
        prjPath.mkdir("other");
    if(!prjPath.exists("pkg"))
        prjPath.mkdir("pkg");
    if(!prjPath.exists("run"))
        prjPath.mkdir("run");
    if(!prjPath.exists("libs"))
        prjPath.mkdir("libs");

    vMainDlgName = NULL;
    this->vDefBasePath = true;
    this->vDefImgPath = true;
}

guiProject::~guiProject()
{
    QList<projectItem *>::iterator it;
    //for(it = this->items.begin(); it != this->items.end(); it++)
    //    delete (*it);
    this->items.clear();
}

void guiProject::setVersion(int A, int B, int C)
{
    vVerA = A;
    vVerB = B;
    vVerC = C;
}

QString guiProject::name()
{
    return this->vName;
}


QString guiProject::path()
{
    return this->vPath;
}

QString guiProject::fullPath()
{
    return this->vPath + QDir::separator() + this->vName;
}


bool guiProject::defBasePath()
{
    return this->vDefBasePath;
}

bool guiProject::defImgPath()
{
    return this->vDefImgPath;
}


void guiProject::setDefBasePath(bool v)
{
    this->vDefBasePath = v;
}

void guiProject::setDefImgPath(bool v)
{
    this->vDefImgPath = v;
}

QString guiProject::exportPath()
{
    return path() + QDir::separator() + "export";
}

QString guiProject::fcnPath()
{
    return path() + QDir::separator() + "fcn";
}

QString guiProject::guiPath()
{
    return path() + QDir::separator() + "gui";
}

QString guiProject::imgPath()
{
    return path() + QDir::separator() + "img";
}

QString guiProject::otherPath()
{
    return path() + QDir::separator() + "other";
}

QString guiProject::pkgPath()
{
    return path() + QDir::separator() + "pkg";
}

QString guiProject::runPath()
{
    return path() + QDir::separator() + "run";
}

QString guiProject::libsPath()
{
    return path() + QDir::separator() + "libs";
}

void guiProject::version(int &A, int &B, int &C)
{
  A = vVerA;
  B = vVerB;
  C = vVerC;
}

void guiProject::setMainDlg(QString dlgName)
{
    QList<projectItem *>::iterator it;
    bool finded = false;
    for(it = items.begin(); (it != items.end()) && (!finded); it++)
    {
        if ((*it)->getName() == dlgName)
        {
            vMainDlgName = dynamic_cast<childWndDlg *>((*it));
            finded = true;
        }
    }
}

void guiProject::setMainDlg(childWndDlg *dlg)
{
  this->vMainDlgName = dlg;
}

childWndDlg * guiProject::getMainDlg()
{
    return this->vMainDlgName;
}

void guiProject::setSearchPath(QStringList searchPath)
{
    vSearchPath = searchPath;
}

QStringList guiProject::searchPath()
{
    return vSearchPath;
}

void guiProject::show(QTreeWidgetItem *)
{
    this->update();
}

void guiProject::update()
{
    QDir guiFiles(this->guiPath());
    QString guiFile;
    QStringList fileList;
    int i;
    fileList = guiFiles.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for(i = 0; i < fileList.count(); i++)
    {
        guiFile = fileList[i];
        childWndDlg * dlg = new childWndDlg(mainWnd::getMainWnd(), 0, mainWnd::getSrcWnd(), mainWnd::getPropPan(), this);
        storageManager::loadDlgFromFile(dlg, this->guiPath() + QDir::separator()  + guiFile);

        mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
        //
        // Esto es necesario debido a que si no se hacen visibles las ventanas
        // los atributos de las mismas no se aplican. El problema es que al hacer
        // visibles todas las ventanas, una a una, se pierde el código asociada a
        // cada una ya que se almacena cuando la ventana se cierra o pierde el foco.
        dlg->show();
        mainWnd::getSrcWnd()->closeChildWnd();
        dlg->close();
        dlg->getFrame()->setCanChangeName(false);
        items.push_back(dlg);
    }

    QDir mFiles(this->fcnPath());
    QString mFile;
    fileList = mFiles.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for(i = 0; i < fileList.count(); i++)
    {
        mFile = fileList[i];
        childWndMFile  * dlg = new childWndMFile(mainWnd::getMainWnd(), 0, this);
        QFile f(this->fcnPath() + QDir::separator() + mFile);
        f.open(QIODevice::ReadOnly | QIODevice::Text);
        dlg->getSrcEditor()->ui->srcEditor->setText(f.readAll());
        dlg->documentHaveName = true;
        dlg->documentName = this->fcnPath() + QDir::separator() + mFile;
        dlg->setWindowTitle(dlg->getName());
        f.close();
        mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
        items.push_back(dlg);
    }

    QDir imgFiles(this->imgPath());
    QString imgFile;
    fileList = imgFiles.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for(i = 0; i < fileList.count(); i++)
    {
        imgFile = fileList[i];
        childWndImg  * dlg = new childWndImg(mainWnd::getMainWnd(), 0, this);

        dlg->documentHaveName = true;
        dlg->documentName = this->imgPath() + QDir::separator()  + imgFile;
        dlg->load(dlg->documentName);
        dlg->setWindowTitle(dlg->getName());
        mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
        items.push_back(dlg);
    }

    QDir otherFiles(this->otherPath());
    QString otherFile;
    fileList = otherFiles.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for(i = 0; i < fileList.count(); i++)
    {
        otherFile = fileList[i];
        childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
        dlg->documentHaveName = true;
        dlg->documentName = this->otherPath() + QDir::separator() + otherFile;
        dlg->load(dlg->documentName);
        dlg->setWindowTitle(dlg->getName());
        mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
        items.push_back(dlg);
    }
}

void guiProject::exportPrj()
{
    int k;
    this->save();
    QString exportPath = this->exportPath();
    QStringList guiLibsFiles;
    QDir addDir(exportPath);
    addDir.mkdir("fcn");
    addDir.mkdir("img");
    addDir.mkdir("guilib");
    addDir.mkdir("wnd");

    guiLibsFiles << "_loadIcon.m";

    for(k = 0; k < guiLibsFiles.count(); k++){
      QFile::copy(":/guilib/guilib/" + guiLibsFiles[k],
                this->exportPath() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]);
      QFile(this->exportPath() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
    }

    if(addDir.exists("libs"))
    {
        QDir rmLibs(addDir);
        rmLibs.cd("libs");
        rmLibs.removeRecursively();
    }
    addDir.mkdir("libs");
    copyFolder(this->path() + QDir::separator() + "libs", this->exportPath() + QDir::separator() + "libs");

    QList<projectItem *>::iterator it;

    for(it = items.begin(); it != items.end(); it++)
    {
        (*it)->exportAsScript(exportPath);
        if((*it) == this->vMainDlgName)
        {
            QFile f;
            f.setFileName(exportPath + QDir::separator() + this->name() + ".m");
            f.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream txt(&f);
            txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
            txt << "## -*- texinfo -*-" << endl;
            txt << "## @deftypefn  {} {@var{ret} =} " + this->name() + " (varargin)" << endl;
            txt << "##" << endl;
            txt << "## Create and show the main dialog, return a struct as representation of dialog." << endl;
            txt << "##" << endl;
            txt << "## @end deftypefn" << endl;
            txt << "function ret = " + this->name() + "(varargin)" << endl;
            txt << "  [dir, name, ext] = fileparts( mfilename('fullpathext') );" << endl;
            if(mainWnd::getAppConfig()->getForceOutputCodec()){
                txt << "  _initialCodec = __mfile_encoding__;" << endl;
                txt << "  __mfile_encoding__(\"" + mainWnd::getAppConfig()->getOutputCodec() + "\");" << endl;;
                txt << "  clear (\"functions\");" << endl;
            }
            if(this->defBasePath())
                txt << "  global _" << this->name() << "BasePath = dir;" << endl;
            if(this->defImgPath())
                txt << "  global _" << this->name() << "ImgPath = [dir filesep() 'img'];" << endl;
            txt << "  addpath( [dir filesep() \"img\" ]);" << endl;
            txt << "  addpath( [dir filesep() \"fcn\" ]);" << endl;
            txt << "  addpath( [dir filesep() \"libs\" ]);" << endl;
            txt << "  addpath( [dir filesep() \"wnd\" ]);" << endl;
            txt << "  addpath([dir filesep() \"guilib\" ]);" << endl;
            //txt << "  ret = " << (*it)->getName() << "(varargin{:});" << endl;
            txt << "  " << (*it)->getName() << "(varargin{:});" << endl;
            if(mainWnd::getAppConfig()->getForceOutputCodec()){
                txt << "  __mfile_encoding__(_initialCodec);" << endl;
            }
            txt << "end" << endl;
            f.close();
        }
    }
    QMessageBox::information(mainWnd::getMainWnd(), QObject::tr("Export project"), QObject::tr("The project was exported to folder ")+ exportPath);
}

void guiProject::run()
{
    int k;
    QStringList paths;
    QStringList guiLibsFiles;
    QDir addDir(this->runPath());
    this->save();
    QString runPath = this->runPath();
    QList<projectItem *>::iterator it;
    addDir.mkdir("fcn");
    addDir.mkdir("guilib");
    addDir.mkdir("img");
    addDir.mkdir("wnd");
    guiLibsFiles << "_loadIcon.m";
    for(k = 0; k < guiLibsFiles.count(); k++){
      QFile::copy(":/guilib/guilib/" + guiLibsFiles[k],
                this->runPath() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]);
      QFile(this->runPath() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
    }
    if(addDir.exists("libs"))
    {
        QDir rmLibs(addDir);
        rmLibs.cd("libs");
        rmLibs.removeRecursively();
    }

    addDir.mkdir("libs");
    copyFolder(this->path() + QDir::separator() + "libs", this->runPath() + QDir::separator() + "libs");

    bool okRun = false;
    for(it = items.begin(); it != items.end(); it++)
        if((*it) == this->vMainDlgName)
            okRun = true;
    if(okRun)
    {
        for(it = items.begin(); it != items.end(); it++)
        {
            (*it)->exportAsScript(runPath);
            if((*it) == this->vMainDlgName)
            {
                QFile f;
                f.setFileName(this->runPath() + QDir::separator() + "runApp.m");
                f.open(QIODevice::WriteOnly | QIODevice::Text);
                QTextStream txt(&f);
                txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
                txt << "function ret = runApp(varargin)" << endl;
                txt << "  [dir, name, ext] = fileparts( mfilename('fullpathext') );" << endl;
                if(mainWnd::getAppConfig()->getForceOutputCodec()){
                    txt << "  _initialCodec = __mfile_encoding__;"  << endl;
                    txt << "  __mfile_encoding__(\"" + mainWnd::getAppConfig()->getOutputCodec() + "\");"  << endl;
                    txt << "  clear (\"functions\");"  << endl;
                }
                if(this->defBasePath())
                    txt << "  global _" << this->name() <<"BasePath = dir;" << endl;
                if(this->defImgPath())
                    txt << "  global _" << this->name() <<"ImgPath = [dir filesep() 'img'];" << endl;

                txt << "  addpath([dir filesep() \"libs\" ]);" << endl;
                txt << "  addpath([dir filesep() \"fcn\" ]);" << endl;
                txt << "  addpath([dir filesep() \"wnd\" ]);" << endl;
                txt << "  addpath([dir filesep() \"guilib\" ]);" << endl;
                txt << "  waitfor(" << (*it)->getName() << "(" << this->runArguments() << ").figure);" << endl;
                if(mainWnd::getAppConfig()->getForceOutputCodec()){
                    txt << "  __mfile_encoding__(_initialCodec);"  << endl;
                }
                txt << "end" << endl;
                f.close();
            }
        }
        paths.append(this->runPath());
        mainWnd::getMainWnd()->runScript(this->runPath() + QDir::separator() + "runApp.m", paths);
    }
    else
        QMessageBox::information(mainWnd::getMainWnd(), QObject::tr("Not defined a main dialog"), QObject::tr("To run a project, you must define a main dialog. This will be the first dialog showed. If there is no dialog in project, add one. If have some dialog, mark as main dialog (left click over them in the project mannager, and select \"set as main dialog\")"));
}

void guiProject::setRunArguments(QString args){
    this->args = args;
}

QString guiProject::runArguments(void){
    return this->args;
}

void guiProject::generatePkg()
{        
    int k;
    QStringList guiLibsFiles;
    QString line;
    QStringList varDef;
    QString strVer = QString::number(this->vVerA) + "." + QString::number(this->vVerB) + "." + QString::number(this->vVerC);
    this->save();
    if((!mainWnd::getMainWnd()->getAppConfig()->gzipPathOk()) ||
            (!mainWnd::getMainWnd()->getAppConfig()->tarPathOk()))
    {
       QMessageBox::information(mainWnd::getMainWnd() ,
                                QObject::tr("Generate octave package"),
                                QObject::tr("To generate a package from this project, you must configure the local path to gzip and tar program from File/Properties menu. This applications can be found in /bin or /usr/bin folder in GNU Linux or must be installed as external program in MS Windows."));
    }
    else
    {
        /* make the package base folder */
        QDir pkgFolder (this->pkgPath());
        if(!pkgFolder.exists(this->name()))
            pkgFolder.mkdir(this->name());
        pkgFolder.cd(this->name());

        /* Check if have COPYING, INDEX and DESCRIPTION files*/
        bool licFile = false;
        bool inxFile = false;
        bool descFile = false;
        bool pkgAddFile = false;
        bool pkgDelFile = false;

        QList<projectItem *>::iterator it;
        for(it = this->items.begin(); it != this->items.end(); it++)
        {
            if((*it)->getName() == "COPYING")
                licFile = true;
            else if((*it)->getName() == "INDEX")
                inxFile = true;
            else if((*it)->getName() == "DESCRIPTION")
                descFile = true;
            else if((*it)->getName() == "PKG_ADD")
                pkgAddFile = true;
            else if((*it)->getName() == "PKG_DEL")
                pkgDelFile = true;
        }
        if(!(licFile && inxFile && descFile && pkgAddFile && pkgDelFile))
        {
            QMessageBox::information(mainWnd::getMainWnd() ,
                                     QObject::tr("Generate octave package"),
                                     QObject::tr("A octave package must have special texts files with license information, details about of package and other data. Not all files are finded in project, basic versions of them are created and added."));

            if(!licFile) /* Add a basic license file */
            {
                QFile::copy(":/template/templates/COPYING", this->otherPath() + QDir::separator() + "COPYING");
                QFile(this->otherPath() + QDir::separator() + "COPYING").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

                childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
                dlg->documentHaveName = true;
                dlg->documentName = this->otherPath() + QDir::separator() + "COPYING";
                dlg->load(dlg->documentName);
                dlg->setWindowTitle(dlg->getName());
                mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
                items.push_back(dlg);
            }

            if(!pkgAddFile)
            {
                QFile::copy(":/template/templates/PKG_ADD", this->otherPath() + QDir::separator() + "PKG_ADD");
                QFile(this->otherPath() + QDir::separator() + "PKG_ADD").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

                childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
                dlg->documentHaveName = true;
                dlg->documentName = this->otherPath() + QDir::separator() + "PKG_ADD";
                dlg->load(dlg->documentName);
                dlg->setWindowTitle(dlg->getName());
                mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
                items.push_back(dlg);
            }

            if(!pkgDelFile)
            {
                QFile::copy(":/template/templates/PKG_DEL", this->otherPath() + QDir::separator() + "PKG_DEL");
                QFile(this->otherPath() + QDir::separator() + "PKG_DEL").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);

                childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
                dlg->documentHaveName = true;
                dlg->documentName = this->otherPath() + QDir::separator() + "PKG_DEL";
                dlg->load(dlg->documentName);
                dlg->setWindowTitle(dlg->getName());
                mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
                items.push_back(dlg);
            }

            if(!inxFile) /* Add a basic index file */
            {
                QFile inxSrc(":/template/templates/INDEX");
                QFile inxDst(this->otherPath() + QDir::separator() + "INDEX");

                inxSrc.open(QIODevice::ReadOnly | QIODevice::Text);
                inxDst.open(QIODevice::WriteOnly | QIODevice::Text);

                QTextStream inxSrcTxt(&inxSrc);
                QTextStream inxDstTxt(&inxDst);
                inxDstTxt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
                while(!inxSrcTxt.atEnd())
                {
                    line = inxSrcTxt.readLine();
                    line.replace("<pkgName>", this->name());
                    inxDstTxt << line << endl;
                }

                inxSrc.close();
                inxDst.close();

                childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
                dlg->documentHaveName = true;
                dlg->documentName = this->otherPath() + QDir::separator() + "INDEX";
                dlg->load(dlg->documentName);
                dlg->setWindowTitle(dlg->getName());
                mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
                items.push_back(dlg);
            }

            if(!descFile) /* Add a basic description file */
            {
                QDateTime now(QDateTime::currentDateTime());
                QFile dscSrc(":/template/templates/DESCRIPTION");
                QFile dscDst(this->otherPath() + QDir::separator() + "DESCRIPTION");

                dscSrc.open(QIODevice::ReadOnly | QIODevice::Text);
                dscDst.open(QIODevice::WriteOnly | QIODevice::Text);

                QTextStream dscSrcTxt(&dscSrc);
                QTextStream dscDstTxt(&dscDst);
                dscDstTxt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
                while(!dscSrcTxt.atEnd())
                {
                    line = dscSrcTxt.readLine();
                    line.replace("<pkgName>", this->name());
                    line.replace("<pkgVer>", strVer);
                    line.replace("<now>", now.toString("dd/MM/yyyy"));
                    dscDstTxt << line << endl;
                }

                dscSrc.close();
                dscDst.close();

                childWndTextFile * dlg = new childWndTextFile(mainWnd::getMainWnd(), 0, this);
                dlg->documentHaveName = true;
                dlg->documentName = this->otherPath() + QDir::separator() + "DESCRIPTION";
                dlg->load(dlg->documentName);
                dlg->setWindowTitle(dlg->getName());
                mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(dlg, Qt::SubWindow);
                items.push_back(dlg);
            }
            mainWnd::getPrjWnd()->refreshList();
        }
        /* The project have all nedded files*/
        QFile::copy(this->otherPath() + QDir::separator() + "COPYING", pkgFolder.path() + QDir::separator() + "COPYING");
        QFile::copy(this->otherPath() + QDir::separator() + "DESCRIPTION", pkgFolder.path() + QDir::separator() + "DESCRIPTION");
        QFile::copy(this->otherPath() + QDir::separator() + "INDEX", pkgFolder.path() + QDir::separator() + "INDEX");
        QFile::copy(this->otherPath() + QDir::separator() + "PKG_ADD", pkgFolder.path() + QDir::separator() + "PKG_ADD");
        QFile::copy(this->otherPath() + QDir::separator() + "PKG_DEL", pkgFolder.path() + QDir::separator() + "PKG_DEL");
        pkgFolder.mkdir("inst");
        pkgFolder.cd("inst");

        pkgFolder.mkdir("fcn");
        pkgFolder.mkdir("img");
        pkgFolder.mkdir("wnd");
        pkgFolder.mkdir("guilib");
        guiLibsFiles << "_loadIcon.m";
        for(k = 0; k < guiLibsFiles.count(); k++){
          QFile::copy(":/guilib/guilib/" + guiLibsFiles[k],
                    pkgFolder.path() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]);
          QFile(pkgFolder.path() + QDir::separator() + "guilib" + QDir::separator() + guiLibsFiles[k]).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
        }
        if(pkgFolder.exists("libs"))
        {
            QDir rmLibs(pkgFolder);
            rmLibs.cd("libs");
            rmLibs.removeRecursively();
        }
        pkgFolder.mkdir("libs");
        copyFolder(this->path() + QDir::separator() + "libs", pkgFolder.path() + QDir::separator() + "libs");

        if(this->defBasePath() || this->defImgPath())
            varDef.append("[_dir, _name, _ext] = fileparts( mfilename('fullpathext') );");
        if(this->defBasePath())
            varDef.append("global _" + this->name() + "BasePath = strtrunc(_dir, length(_dir) - 4);");
        if(this->defImgPath())
            varDef.append("global _" + this->name() + "ImgPath = [ strtrunc(_dir, length(_dir) - 4) filesep() 'img'];");
        for(it = items.begin(); it != items.end(); it++)
        {
            (*it)->setVariableDefinitions(varDef);
            (*it)->exportAsScript(pkgFolder.path());
            if((*it) == this->vMainDlgName)
            {
                QFile f;
                f.setFileName(pkgFolder.path() + QDir::separator() + this->name() + ".m");
                f.open(QIODevice::WriteOnly | QIODevice::Text);
                QTextStream txt(&f);
                txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
                txt << "## -*- texinfo -*-" << endl;
                txt << "## @deftypefn  {} {@var{ret} =} " + this->name() + " (varargin)" << endl;
                txt << "##" << endl;
                txt << "## Create and show the main dialog, return a struct as representation of dialog." << endl;
                txt << "##" << endl;
                txt << "## @end deftypefn" << endl;
                txt << "function ret = " + this->name() + "(varargin)" << endl;
                txt << "  [dir, name, ext] = fileparts( mfilename('fullpathext') );" << endl;
                if(mainWnd::getAppConfig()->getForceOutputCodec()){
                    txt << "  _initialCodec = __mfile_encoding__;"  << endl;
                    txt << "  __mfile_encoding__(\"" + mainWnd::getAppConfig()->getOutputCodec() + "\");"  << endl;
                    txt << "  clear (\"functions\");"  << endl;
                }
                if(this->defBasePath())
                    txt << "  global _" << this->name() << "BasePath = dir;" << endl;
                if(this->defImgPath())
                    txt << "  global _" << this->name() << "ImgPath = [dir filesep() 'img'];" << endl;
                txt << "  addpath( [dir filesep() \"img\" ]);" << endl;
                txt << "  addpath( [dir filesep() \"fcn\" ]);" << endl;
                txt << "  addpath( [dir filesep() \"libs\" ]);" << endl;
                txt << "  addpath( [dir filesep() \"wnd\" ]);" << endl;
                txt << "  addpath( [dir filesep() \"guilib\" ]);" << endl;
                txt << "  " << (*it)->getName() << "(varargin{:});" << endl;
                if(mainWnd::getAppConfig()->getForceOutputCodec()){
                    txt << "  __mfile_encoding__(_initialCodec);"  << endl;
                }
                txt << "end" << endl;
                f.close();
            }
        }
        /* The project structure has made, now we must compress the folder */
        pkgFolder.cdUp();
        pkgFolder.cdUp();

        mainWnd::getPkgGenWnd()->startGen(pkgFolder.path(), this->name());
    }
}

void guiProject::save()
{
    storageManager::saveProject(this);
    mainWnd::getMainWnd()->updateRecentPrj();
    QList<projectItem *>::iterator it;
    for(it = items.begin(); it != items.end(); it++)
        (*it)->save();
}

