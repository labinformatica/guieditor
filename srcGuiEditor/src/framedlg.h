/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef FRAMEDLG_H
#define FRAMEDLG_H

#include <QFrame>
#include <QList>
#include <QPaintEvent>
#include <QWidget>
#include <QMouseEvent>
#include "abstractuicctrl.h"
#include <QXmlStreamWriter>

typedef struct {
  unsigned from;
  unsigned to;
  abstractUICCtrl *ctrl;
} lineCodeMapEntry;

class wndResizePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QString resizeValue;
public:
    wndResizePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual QString generateCode();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};


class wndPositionPropEditor: public abstractPropEditor
{
public:
    wndPositionPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual QString generateCode();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
};

class frameBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QPalette palette;
public:
    frameBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class windowStylePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QString windowStyleValue;
public:
    windowStylePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};


enum hDlgPos {leftHPos, rightHPos, centerHPos, defaultHPos};
class hAlignDlgPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    hDlgPos hPos;
public:
    hAlignDlgPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode() { return "";}
    virtual bool canGenerateCode() { return false;}
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};

enum vDlgPos {topVPos, bottomVPos, centerVPos, defaultVPos};
class vAlignDlgPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    vDlgPos vPos;
public:
    vAlignDlgPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode() { return "";}
    virtual bool canGenerateCode() { return false;}
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};

class frameDlg : public QWidget, public abstractUICCtrl
{
    Q_OBJECT
private:
    static unsigned int formNumber;
    QVector<lineCodeMapEntry> lineCodeMap;
    bool isHidden;
public:
    virtual QString className(){return "frameDlg";}
    abstractUICCtrl * mapCodeLine(unsigned line, int &offset);
    void addWidget(abstractUICCtrl *w);
    int cordToGrid(int);
    explicit frameDlg(QWidget *parent, childWndDlg *dlg);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool isHidden);
    virtual ~frameDlg();
    static unsigned int getFormNumber();

    virtual QStringList generateMFile(QString path);
    virtual QStringList createCallBack(QString path);
    void removeControlFromList(abstractUICCtrl * ctrl, abstractUICCtrl* toDrop);
    void generateControlList(abstractUICCtrl *wdg, QStringList & controlList);
    void generateMFileRecursive(QString path,  QStringList &ret, abstractUICCtrl *wdg, QStringList & controlList);
    void generateCallBackRecursive(QStringList &ret, abstractUICCtrl *wdg, QString dialogName);
    void createCallBackRecursive(QStringList &ret, QString path, abstractUICCtrl *cnt);
    void updateSrcWndCtrls();
signals:

public slots:

protected:
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void paintEvent(QPaintEvent *event);    
    virtual void keyPressEvent(QKeyEvent * event );
    virtual void moveEvent ( QMoveEvent * event );
    virtual void resizeEvent ( QResizeEvent * event );
};

#endif // FRAMEDLG_H
