/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "eduCIAACtrl.h"

#include "editableEduCIAA.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QSerialPortInfo>

edDacValEditor::edDacValEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "dacOutput";
}

QString edDacValEditor::getValue()
{
    editableEduCIAA * eduCiaa;
    QString ret = "";
    eduCiaa = (editableEduCIAA * )this->ctrl->associatedWidget();
    ret = QString::number(eduCiaa->getDacVal());
    return ret;
}

bool edDacValEditor::isValidValue(QString newVal)
{
    bool okNro;
    unsigned int vTest;
    vTest = newVal.toUInt(&okNro);
    return okNro && (vTest < 1024);
}

void edDacValEditor::setValue(QString newVal)
{
    unsigned int dacNewVal;
    editableEduCIAA * eduCiaa;
    eduCiaa = (editableEduCIAA * )this->ctrl->associatedWidget();
    dacNewVal = newVal.toUInt();
    eduCiaa->setDacVal(dacNewVal % 1024);
}

QString edDacValEditor::generateCode()
{
    QString ret = "'" + this->propName + "', '" + this->getValue() + "'";
    return ret;
}

eduCiaaLedValueEditor::eduCiaaLedValueEditor(abstractUICCtrl *ctrl, unsigned int pinNumber)
    :abstractPropEditor(ctrl)
{
    this->pinNumber = pinNumber;
    if((pinNumber >= 1) && ((pinNumber <= 3)))
      this->propName = "ledNro_"+QString::number(pinNumber);
    else if (pinNumber == 4)
        this->propName = "ledRGBRed";
    else if (pinNumber == 5)
        this->propName = "ledRGBGreen";
    else if (pinNumber == 6)
        this->propName = "ledRGBBlue";

}

QString eduCiaaLedValueEditor::getValue()
{
    editableEduCIAA * edCiaa;
    QString ret = "None";
    edCiaa = (editableEduCIAA * )this->ctrl->associatedWidget();
    if(edCiaa->getPinValue(this->pinNumber) == pinOn)
       ret = "On";
    else if(edCiaa->getPinValue(this->pinNumber) == pinOff)
       ret = "Off";
    return ret;
}

bool eduCiaaLedValueEditor::isValidValue(QString newVal)
{
    return (newVal == "On") || (newVal == "Off") || (newVal == "None");
}

void eduCiaaLedValueEditor::setValue(QString newVal)
{
    editableEduCIAA * edCiaa;
    edCiaa = (editableEduCIAA * )this->ctrl->associatedWidget();
    if (newVal == "On")
      edCiaa->setPinValue(this->pinNumber, pinOn);
    else if(newVal == "Off")
      edCiaa->setPinValue(this->pinNumber, pinOff);
    else if(newVal == "None")
      edCiaa->setPinValue(this->pinNumber, pinNone);
}

QString eduCiaaLedValueEditor::generateCode()
{
    QString ret = "";
    editableEduCIAA * edCiaa;
    edCiaa = (editableEduCIAA * )this->ctrl->associatedWidget();
    if(edCiaa->getPinValue(this->pinNumber) !=  pinNone)
    {
        if(edCiaa->getPinValue(this->pinNumber) == pinOn)
          ret = "'" + this->propName + "', 'On'";
        else
          ret = "'" + this->propName + "', 'Off'";
    }
    return ret;
}

QWidget * eduCiaaLedValueEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("None");
    cb->addItem("Off");
    cb->addItem("On");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(eduCiaaPinValueChanged(QString)));
    return cb;
}
void eduCiaaLedValueEditor::eduCiaaPinValueChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(eduCiaaPinValueChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(eduCiaaPinValueChanged(QString)));
    }
}


cbCIAASerialPortEditor::cbCIAASerialPortEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "serialPort";
}

QString cbCIAASerialPortEditor::getValue()
{
    editableEduCIAA * cb;
    QString ret = "";
    cb = (editableEduCIAA * )this->ctrl->associatedWidget();
    ret = cb->getSerialPortName();
    return ret;
}

bool cbCIAASerialPortEditor::isValidValue(QString newVal __attribute__((unused)))
{
    return true;
}

void cbCIAASerialPortEditor::setValue(QString newVal)
{
    editableEduCIAA * cb;
    cb = (editableEduCIAA * )this->ctrl->associatedWidget();
    cb->setSerialPortName(newVal);
}

QString cbCIAASerialPortEditor::generateCode()
{
    QString ret = "'" + this->propName + "', '" + this->getValue() + "'";
    return ret;
}

QWidget * cbCIAASerialPortEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();

    foreach (QSerialPortInfo info, ports)
    {
        if(info.portName().length() > 0)
          cb->addItem(info.portName());
    }
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
    return cb;
}

void cbCIAASerialPortEditor::cbSerialPortChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbSerialPortChanged(QString)));
    }
}


unsigned int eduCIAACtrl::uicNameCounter = 0;

unsigned int eduCIAACtrl::getNameCounter()
{
    return uicNameCounter;
}

eduCIAACtrl::eduCIAACtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableEduCIAA * img = new editableEduCIAA(parent, this);
    uicNameCounter++;
    this->setCtrlName("eduCIAA_"+QString::number(uicNameCounter));
    img->resize(52, 52);

    this->setAssociatedWidget(img);
    this->adjTL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjML->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjMR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTM->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBM->asociateCtrl(this->getAssociatedWidget(), false);
    this->properties.registerProp(new cbCIAASerialPortEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 1));
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 2));
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 3));
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 4)); // RGB Red
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 5)); // RGB Green
    this->properties.registerProp(new eduCiaaLedValueEditor(this, 6)); // RGB Blue
    this->properties.registerProp(new edDacValEditor(this)); // RGB Blue

}

void eduCIAACtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableEduCIAA * ciaa = dynamic_cast<editableEduCIAA *>(this->associatedWidget());
    ciaa->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList eduCIAACtrl::generateMFile(QString path)
{
    QStringList ret;
    abstractPropEditor * prop;
    QString lineMCode;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = eduCIAA( ...") ;
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = prop->generateCode();
          if(lineMCode.length() > 0)
            ret.append("\t" + lineMCode );
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;    
}

QStringList eduCIAACtrl::createCallBack(QString path __attribute__((unused)))
{
  QStringList ret;
  return ret;
}

abstractUICCtrl * eduCIAACtrl::clone(QWidget *parent,
                                     abstractUICCtrl *octaveParent,
                                     childWndDlg * parentWnd){
    eduCIAACtrl * ret = new eduCIAACtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void eduCIAACtrl::setHidden(bool isHidden){
    ((editableEduCIAA *)this->associatedWidget())->setHidden(isHidden);
}

eduCIAACtrl::~eduCIAACtrl()
{
    delete this->getAssociatedWidget();
}

eduCIAACtrlGen::eduCIAACtrlGen():controlGenerator("eduCIAACtrl")
{
}

abstractUICCtrl * eduCIAACtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    eduCIAACtrl * aNano = new eduCIAACtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = aNano->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = aNano->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      aNano->setSrcCallBack(src.split('\n'));

    aNano->deselect();
    return aNano;
}

