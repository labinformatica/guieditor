/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "callbackctrl.h"
#include "editablecallback.h"
#include "wdgargeditor.h"
unsigned int callBackCtrl::uicNameCounter = 0;

unsigned int callBackCtrl::getNameCounter()
{
    return uicNameCounter;
}

callBackCtrl::callBackCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)
{    
    editableCallBack * img = new editableCallBack(parent, this);
    uicNameCounter++;
    this->setHaveCallBack(true);
    this->setCtrlName("callBack_"+QString::number(uicNameCounter));
    img->resize(52, 52);

    this->setAssociatedWidget(img);
    this->adjTL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjML->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjMR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTM->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBM->asociateCtrl(this->getAssociatedWidget(), false);

    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new fNamePropEditor(this));
    this->properties.registerProp(new argListPropEditor(this));

    this->srcCallBack.append(QObject::tr("% This code can define a callback."));
    this->srcCallBack.append(QObject::tr("% You must associate them to figure or control onload like event window"));
}

void callBackCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableCallBack * callBack = dynamic_cast<editableCallBack *>(this->associatedWidget());
    callBack->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList callBackCtrl::generateMFile(QString path __attribute__((unused)))
{
    QStringList ret;
    ret.append("  " + this->properties.getValueByName("Name") + " = @" +  this->properties.getValueByName("functionName") + ";");
    return ret;
}

QStringList callBackCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    QStringList arg = this->properties.getValueByName("Arguments").split("|", QString::SkipEmptyParts);
    toAdd.append(this->preCallback(this->properties.getValueByName("functionName"), arg));
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * callBackCtrl::clone(QWidget *parent,
                                      abstractUICCtrl *octaveParent,
                                      childWndDlg * parentWnd){
    callBackCtrl * ret = new callBackCtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}
void callBackCtrl::setHidden(bool isHidden){
    ((editableCallBack *)this->associatedWidget())->setHidden(isHidden);
}

callBackCtrl::~callBackCtrl()
{
    delete this->getAssociatedWidget();
}

callBackCtrlCtrlGen::callBackCtrlCtrlGen():controlGenerator("callBackCtrl")
{
}

abstractUICCtrl * callBackCtrlCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    callBackCtrl * callBack = new callBackCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = callBack->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = callBack->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      callBack->setSrcCallBack(src.split('\n'));

    callBack->deselect();
    return callBack;
}

//// Implementación de los métodos de los editores de propiedades


argListPropEditor::argListPropEditor(abstractUICCtrl *ctrl):
    abstractPropEditor(ctrl)
{
    this->propName = "Arguments";
    arg.clear();
}

QString argListPropEditor::getValue()
{
    QString lst;
    int i;
    for(i = 0; i < arg.count(); i++)
    {
        if(i < arg.count() - 1)
          lst += arg[i] + "|";
        else
          lst += arg[i];
    }
    return lst;
}

bool argListPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegExp reg ("^[a-zA-Z_][a-zA-Z0-9_]*$");
    bool ok = true;
    int i;
    QStringList l = newVal.split("|", QString::SkipEmptyParts);
    for(i = 0; i < l.count(); i++)
        ok = ok && l[i].contains(reg);
    return ok;
}

void argListPropEditor::setValue(QString newVal)
{
  arg = newVal.split("|", QString::SkipEmptyParts);
}

QString argListPropEditor::generateCode()
{
    QString ret;

    return ret;
}

bool argListPropEditor::canGenerateCode()
{
    return false;
}

QWidget * argListPropEditor::getEditWidget()
{
    wdgArgEditor * wdgId = new wdgArgEditor(this);
    wdgId->setItems(this->getValue());
    connect(wdgId->getLEItems(), SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    return wdgId;
}

void argListPropEditor::valueItemChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {
        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueItemChanged()));
}
