/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "nodeMCUctrl.h"

#include "editableNodeMCU.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QSerialPortInfo>
#include <QStringList>


showIpDialog::showIpDialog(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "showIpDlg";
    this->showIpDlg = true;
}

QString showIpDialog::getValue()
{
    QString ret;
    ret = "false";
    if(this->showIpDlg)
        ret = "true";
    return ret;
}
bool showIpDialog::isValidValue(QString newVal)
{
    return (newVal.toLower() == "true") || (newVal.toLower() == "false");
}
void showIpDialog::setValue(QString newVal)
{
    this->showIpDlg=(newVal.toLower() == "true");
}

QString showIpDialog::generateCode()
{
    QString ret = "'showIpDlg', '" + this->getValue() + "'";
    return ret;
}

bool showIpDialog::canGenerateCode()
{
    return true;
}

QWidget * showIpDialog::getEditWidget()
{
    checkBoxUpdPropWdg * cb = new checkBoxUpdPropWdg(this);
    cb->setChecked(this->showIpDlg);
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    return cb;
}

void showIpDialog::callBackChanged(int state)
{
    checkBoxUpdPropWdg * cb = (checkBoxUpdPropWdg *)QObject::sender();
    if(state == Qt::Unchecked)
    {
        this->setValue("false");
    }
    else if(state == Qt::Checked)
    {
        this->setValue("true");
    }
    else
    {
        disconnect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
        if(this->getValue().toLower() == "true")
            cb->setChecked(true);
        else
            cb->setChecked(false);
        connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    }
}

expNodeClass::expNodeClass(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "exportClass";
    this->expClass = true;
}

QString expNodeClass::getValue()
{
    QString ret;
    ret = "false";
    if(this->expClass)
        ret = "true";
    return ret;
}
bool expNodeClass::isValidValue(QString newVal)
{
    return (newVal.toLower() == "true") || (newVal.toLower() == "false");
}
void expNodeClass::setValue(QString newVal)
{
    this->expClass=(newVal.toLower() == "true");
}

QString expNodeClass::generateCode()
{
    return "";
}

bool expNodeClass::canGenerateCode()
{
    return false;
}

QWidget * expNodeClass::getEditWidget()
{
    checkBoxUpdPropWdg * cb = new checkBoxUpdPropWdg(this);
    cb->setChecked(this->expClass);
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    return cb;
}

void expNodeClass::callBackChanged(int state)
{
    checkBoxUpdPropWdg * cb = (checkBoxUpdPropWdg *)QObject::sender();
    if(state == Qt::Unchecked)
    {
        this->setValue("false");
    }
    else if(state == Qt::Checked)
    {
        this->setValue("true");
    }
    else
    {
        disconnect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
        if(this->getValue().toLower() == "true")
            cb->setChecked(true);
        else
            cb->setChecked(false);
        connect(cb, SIGNAL(stateChanged(int)), this, SLOT(callBackChanged(int)));
    }
}


edIpEditor::edIpEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Ip";
}

QString edIpEditor::getValue()
{
    editableNodeMCU * node;
    QString ret = "";
    node = (editableNodeMCU * )this->ctrl->associatedWidget();
    ret = node->getIpAddress();
    return ret;
}

bool edIpEditor::isValidValue(QString newVal)
{
    QStringList partes = newVal.split(".");
    int v;
    int i;
    bool okNro;
    bool okAll;
    okAll = partes.length() == 4;
    for(i = 0; (i < partes.length()) && okAll; i++)
    {
        v = partes[i].toInt(&okNro);
        okAll = okAll && (v>=0) && (v < 256) && okNro;
    }
    return okAll;
}

void edIpEditor::setValue(QString newVal)
{
    editableNodeMCU * node;
    node = (editableNodeMCU * )this->ctrl->associatedWidget();
    node->setIpAddress(newVal);
}

QString edIpEditor::generateCode()
{
    QString ret = "'ip', '" + this->getValue() + "'";
    return ret;
}


cbPinNodeTypeEditor::cbPinNodeTypeEditor(abstractUICCtrl *ctrl, int pinNumber, bool isReadOnly):
    abstractPropEditor(ctrl)
{
    this->pinIsReadOnly = isReadOnly;
    this->pinNumber = pinNumber;
    this->propName = "pinType" + QString::number(pinNumber);
}

QString cbPinNodeTypeEditor::getValue()
{
    editableNodeMCU * node;
    QString ret = "none";
    node = (editableNodeMCU * )this->ctrl->associatedWidget();
    if(node->getPinType(this->pinNumber) == input)
        ret = "input";
    else if(node->getPinType(this->pinNumber) == inputPullUp)
        ret = "inputPullUp";
    else if(node->getPinType(this->pinNumber) == output)
        ret = "output";
    else if(node->getPinType(this->pinNumber) == outputOD)
        ret = "outputOD";
    else if(node->getPinType(this->pinNumber) == pwm)
        ret = "pwm";

    return ret;
}

bool cbPinNodeTypeEditor::isValidValue(QString newVal)
{
    if (this->pinIsReadOnly)
      return (newVal == "input") || (newVal == "none");
    else
      return (newVal == "none") ||
             (newVal == "input") ||
             (newVal == "inputPullUp") ||
              (newVal == "output") ||
              (newVal == "outputOD") ||
              (newVal == "pwm");
}

void cbPinNodeTypeEditor::setValue(QString newVal)
{
    editableNodeMCU * node;
    node = (editableNodeMCU * )this->ctrl->associatedWidget();
    if(newVal == "input")
        node->setPinType(this->pinNumber, input);
    else if(newVal == "inputPullUp")
        node->setPinType(this->pinNumber, inputPullUp);
    else if(newVal == "output")
        node->setPinType(this->pinNumber, output);
    else if(newVal == "outputOD")
        node->setPinType(this->pinNumber, outputOD);
    else if(newVal == "pwm")
        node->setPinType(this->pinNumber, pwm);
    else
        node->setPinType(this->pinNumber, none);
}

QString cbPinNodeTypeEditor::generateCode()
{
    QString ret;
    editableNodeMCU * nano;
    nano = (editableNodeMCU * )this->ctrl->associatedWidget();
    if (nano->getPinType(this->pinNumber) != none)
    {
      if(nano->getPinType(this->pinNumber) == pwm)
      {
          ret = "'typePinPWM" + QString::number(this->pinNumber) + "', '" + this->getValue() + "', ";
          ret += "'frec', '1000', 'duty', '0'";
      }
      else if((nano->getPinType(this->pinNumber) == input) ||
              (nano->getPinType(this->pinNumber) == inputPullUp) ||
              (nano->getPinType(this->pinNumber) == output) ||
              (nano->getPinType(this->pinNumber) == outputOD))
      {
          ret = "'typePinDigital" + QString::number(this->pinNumber) + "', '" + this->getValue() + "'";
      }


    }
    return ret;
}

QWidget * cbPinNodeTypeEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("none");
    cb->addItem("input");
    cb->addItem("inputPullUp");
    if (!this->pinIsReadOnly)
    {
      cb->addItem("output");   
      cb->addItem("outputOD");
      cb->addItem("pwm");
    }
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
    return cb;
}

void cbPinNodeTypeEditor::cbPinDirChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbPinDirChanged(QString)));
    }
}


unsigned int nodeMCUCtrl::uicNameCounter = 0;

unsigned int nodeMCUCtrl::getNameCounter()
{
    return uicNameCounter;
}

nodeMCUCtrl::nodeMCUCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    int i;
    editableNodeMCU * img = new editableNodeMCU(parent, this);
    uicNameCounter++;
    this->setCtrlName("nodeMCU_"+QString::number(uicNameCounter));
    img->resize(52, 52);

    this->setAssociatedWidget(img);
    this->adjTL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjML->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBL->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjMR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBR->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjTM->asociateCtrl(this->getAssociatedWidget(), false);
    this->adjBM->asociateCtrl(this->getAssociatedWidget(), false);
    this->properties.registerProp(new edIpEditor(this));
    this->properties.registerProp(new expNodeClass(this));
    this->properties.registerProp(new showIpDialog(this));
    this->properties.registerProp(new positionPropEditor(this));
    for(i=0; i <= 16; i++)
    {
      this->properties.registerProp(new cbPinNodeTypeEditor(this, i));
    }
}

void nodeMCUCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableNodeMCU * node = dynamic_cast<editableNodeMCU *>(this->associatedWidget());
    node->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList nodeMCUCtrl::generateMFile(QString path)
{
    QStringList ret;
    abstractPropEditor * prop;
    QString lineMCode;
    int i;
    QStringList files;
    files << "nodeAdcReadInput.m";
    files << "nodeAdcReadVcc.m";
    files << "nodeClose.m";
    files << "nodeGetTemp.m";
    files << "nodeGetValueDigital.m";
    files << "nodeMCU.m";
    files << "nodeReset.m";    
    files << "nodeRmConfig.m";
    files << "nodeSetPinType.m";
    files << "nodeSetValueDigital.m";
    files << "nodeSetValuePwm.m";
    files << "nodeStartPwm.m";
    files << "nodeStopPwm.m";

    if(this->properties.getPropByName("exportClass")->getValue() == "true")
    {
        QDir libDir(path);
        libDir.cdUp();
        libDir.cd("libs");
        QFile::copy(":/ctrlAduino/octaveCtrls/selIpAddr.m",
                    libDir.path() + QDir::separator() + "selIpAddr.m");
        QFile(libDir.path() + QDir::separator() + "selIpAddr.m").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
        QFile::copy(":/ctrlAduino/octaveCtrls/selIpAddr_def.m",
                    libDir.path() + QDir::separator() + "selIpAddr_def.m");
        QFile(libDir.path() + QDir::separator() + "selIpAddr_def.m").setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
        libDir.mkdir("@nodeMCU");
        libDir.cd("@nodeMCU");

        for(int i=0; i < files.count(); i++){
            QFile::copy(":/ctrlAduino/octaveCtrls/@nodeMCU/" + files[i],
                        libDir.path() + QDir::separator() + files[i]);
            QFile(libDir.path() + QDir::separator() + files[i]).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ReadGroup);
        }
    }
    ret.append("  " + this->properties.getValueByName("Name") + " = nodeMCU( ...") ;
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = prop->generateCode();
          if(lineMCode.length() > 0)
            ret.append("\t" + lineMCode );
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList nodeMCUCtrl::createCallBack(QString path __attribute__((unused)))
{
  QStringList ret;
  return ret;
}

abstractUICCtrl * nodeMCUCtrl::clone(QWidget *parent,
                                     abstractUICCtrl *octaveParent,
                                     childWndDlg * parentWnd){
    nodeMCUCtrl * ret = new nodeMCUCtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void nodeMCUCtrl::setHidden(bool isHidden){
    ((editableNodeMCU *)this->associatedWidget())->setHidden(isHidden);
}

nodeMCUCtrl::~nodeMCUCtrl()
{
    delete this->getAssociatedWidget();
}

nodeMCUCtrlGen::nodeMCUCtrlGen():controlGenerator("nodeMCUCtrl")
{
}

abstractUICCtrl * nodeMCUCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    nodeMCUCtrl * node = new nodeMCUCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = node->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = node->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      node->setSrcCallBack(src.split('\n'));

    node->deselect();
    return node;
}

