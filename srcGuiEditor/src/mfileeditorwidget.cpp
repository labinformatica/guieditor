/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "mfileeditorwidget.h"
#include "ui_mfileeditorwidget.h"
#include <QMenu>
#include <QMenuBar>
#include "mainwnd.h"

mfileEditorWidget::mfileEditorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mfileEditorWidget)
{
    ui->setupUi(this);
    //QFontMetrics fm(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    //ui->srcEditor->setTabStopWidth(mainWnd::getMainWnd()->getAppConfig()->getEditorTabSize() * fm.width(" "));
    lex = new QsciLexerMatlab(ui->srcEditor);
    lex->setFont(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    ui->srcEditor->setLexer(lex);
    ui->srcEditor->setFont(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    ui->srcEditor->setMarginType(0, QsciScintilla::NumberMargin);
    ui->srcEditor->setMarginWidth(0, "00000");
    ui->srcEditor->setCaretLineVisible(true);
}

mfileEditorWidget::~mfileEditorWidget()
{
    delete ui;
}
