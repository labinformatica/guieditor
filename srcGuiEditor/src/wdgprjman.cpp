﻿/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgprjman.h"
#include "ui_wdgprjman.h"
#include <QMessageBox>

wdgPrjMan::wdgPrjMan(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wdgPrjMan)
{
    ui->setupUi(this);
    this->vActiveProject = NULL;
}

wdgPrjMan::~wdgPrjMan()
{
    delete ui;
}

void wdgPrjMan::addProject(guiProject *prj)
{
    this->openProjects.push_back(prj);
    this->setActiveProject(prj);
    refreshList();
}

bool wdgPrjMan::isOpen(QString fileName){
    bool ret = false;
    QList<guiProject *>::iterator prj;
    for(prj = openProjects.begin(); prj!= openProjects.end(); prj++)
    {
        if (fileName == (*prj)->fullPath() +  ".prj")
            ret = true;
    }
    return ret;
}

void wdgPrjMan::refreshList()
{
    QList<guiProject *>::iterator prj;
    QList<projectItem *>::iterator prjItem;
    treeItemProject *prjTreeItem;
    this->ui->projectTree->clear();
    for(prj = openProjects.begin(); prj!= openProjects.end(); prj++)
    {
        prjTreeItem = new treeItemProject(*prj);
        this->ui->projectTree->addTopLevelItem(prjTreeItem);
        if(openProjects.count() == 1)
            prjTreeItem->setSelected(true);
        for(prjItem = (*prj)->items.begin(); prjItem != (*prj)->items.end(); prjItem++)
        {
            (*prjItem)->treeItemWidget(prjTreeItem);
        }
        prjTreeItem->setExpanded(true);
    }


}

void wdgPrjMan::setActiveProject(guiProject *p)
{
    this->vActiveProject = p;
}

guiProject * wdgPrjMan::activeProject()
{
    return this->vActiveProject;
}

void wdgPrjMan::on_projectTree_itemDoubleClicked(QTreeWidgetItem *item, int column __attribute__((unused)))
{
    dynamic_cast<abstractPrjTreeItem *>(item)->activate();

}

void wdgPrjMan::on_projectTree_itemActivated(QTreeWidgetItem *item, int column __attribute__((unused)))
{
    dynamic_cast<abstractPrjTreeItem *>(item)->selectProject();
}

void wdgPrjMan::on_projectTree_itemClicked(QTreeWidgetItem *item, int column __attribute__((unused)))
{
    dynamic_cast<abstractPrjTreeItem *>(item)->selectProject();
}

void wdgPrjMan::closeActiveProject()
{
    QList<projectItem *>::iterator it;
    QMessageBox::StandardButton reply;
    bool haveUnSave = false;
    if(this->activeProject())
    {
        for(it = this->activeProject()->items.begin();
            it != this->activeProject()->items.end();
            it ++)
            haveUnSave = haveUnSave || (*it)->isModified();

        if(haveUnSave)
        {
            reply = QMessageBox::question(this,
                                          tr("Se perderá información"),
                                          tr("Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?"),
                                          QMessageBox::No | QMessageBox::Yes);
            if(reply == QMessageBox::Yes)
            {
                for(it = this->activeProject()->items.begin();
                    it != this->activeProject()->items.end();
                    it ++)
                    (*it)->save();
            }
            this->activeProject()->save();
        }

        for(it = this->activeProject()->items.begin();
            it != this->activeProject()->items.end();
            it ++)
        {
            (*it)->closingProject();
            delete (*it);
        }

        this->openProjects.removeOne(this->activeProject());
        delete this->vActiveProject;
        this->setActiveProject(NULL);
        this->refreshList();
    }
}
