/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDITCTRL_H
#define EDITCTRL_H
#include "abstractuicctrl.h"
#include <QWidget>
#include <QLineEdit>
#include "controlgenerator.h"

class editHAlignmentPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    editHAlignmentPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void editHAlignmentChanged(QString text);
};

class textEditPropEditor: public abstractPropEditor
{
public:
    textEditPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QLineEdit *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal){
        ((QLineEdit *)this->ctrl->associatedWidget())->setText(newVal);
    }

    virtual bool canAgroup(){ return true;}
    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};

class editBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QPalette palette;
public:
    editBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class editFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    editFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class editCtrl : public abstractUICCtrl
{
  private:
    static unsigned int uicNameCounter;

  protected:
  public:
    static unsigned int getNameCounter();
    QStringList createCallBack(QString path);
    editCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);

    virtual QString className() { return "editCtrl";}
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~editCtrl();   
};

class editCtrlGen: public controlGenerator
{
public:
  editCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // EDITCTRL_H
