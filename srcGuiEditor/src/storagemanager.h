/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * \class storageManager
 *
 * \brief Gestor de almacenamiento
 *  Esta clase es responsable de persistir todos los atributos asociados a un
 *  diálogo particular así como de restaurarlo desde un archivo.
 *  Incorpora además de los métodos básicos para almacenar y restaurar un diálogo
 *  métodos para gestionar una lista de "generadores de controles". Estas son clases
 *  que son responsables de, para un control particular, generar un objeto con las
 *  características encontradas en el archivo asociado.
 *
 * \note Por cada nuevo tipo de control, la clase generadora de ese control debe
 * registrarse en la lista de generadores. De no hacer esto, si bien los controles
 * podrán almacenarse no se los podrá restaurar.
 *
 *
 */

#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H
#include <QString>
#include "framedlg.h"
#include "controlgenerator.h"

class storageManager
{
private:
    /** Debido a que esta clase ha sido implementada como una clase estática
      * se gestiona una lista de generadores como una lista (QList) también
      * estática. Para resolver el problema del almacenamiento, este método
      * crea la lista como estática retornándola como una referencia. De este
      * modo se resuelve el problema de creación y acceso a la lista.
      */
    static QList<controlGenerator *>& getGeneratorList();
public:
    /// Retorna la cantidad de generadores registrados en la lista
    static int registeredGeneratosCount();
    /// Registra (carga) los generadores necesarios en la lista.
    static void registerGenerators();
    /// Elimina todos los generadores de la lista (libera la memoria de los generadores)
    static void unregisterGenerators();
    /** Carga una ventana desde un archivo. Los argumentos son el frame donde
      *  se representará la ventana y el nombre del archivo que se desea leer.
      * El archivo debe existir y ser accesible. En lo que respecta al objeto frame
      * debe ser creado previo a la invocación del método.
      */

    static void loadDlgFromFile(childWndDlg * dlg, /**< [out] Frame donde se asociarán los controles. */
                             QString name);   /**< [in] Nombre del archivo a procesar (full path). */

    static void loadDlgFromFileRecursive(childWndDlg * dlg,
                               abstractUICCtrl * octaveParent,
                               QXmlStreamReader &xml);
    /** Alamena una ventana en un archivo. El nombre del archivo no es un parámetro
      * debido a que entre los atributos del frame está el nombre del documento asociado.
      */
    static void saveDlgToFile(childWndDlg * dlg, QString fileName);
    static void saveDlgToFileRecursive(abstractUICCtrl *ctrl, QXmlStreamWriter &xml);

    static void saveProject(guiProject * prj);
    static guiProject * loadProject(QString fileName);
};


#endif // STORAGEMANAGER_H
