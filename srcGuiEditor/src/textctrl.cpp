/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "textctrl.h"
#include "editabletext.h"
#include "storagemanager.h"

#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"

textHAlignmentPropEditor::textHAlignmentPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "HorizontalAlignment";
}

QString textHAlignmentPropEditor::getValue()
{
    editableText * lab;
    QString ret;
    lab = (editableText * )this->ctrl->associatedWidget();
    if(lab->alignment() == Qt::AlignLeft)
        ret = "left";
    else if(lab->alignment() == Qt::AlignRight)
        ret = "right";
    else
        ret = "center";
    return ret;
}

bool textHAlignmentPropEditor::isValidValue(QString newVal)
{
    return (newVal == "left")||(newVal == "center")||(newVal == "right");
}

void textHAlignmentPropEditor::setValue(QString newVal)
{
    editableText * lab;
    lab = (editableText * )this->ctrl->associatedWidget();
    if(newVal == "left")
        lab->setAlignment(Qt::AlignLeft);
    else if(newVal == "right")
        lab->setAlignment(Qt::AlignRight);
    else
        lab->setAlignment(Qt::AlignHCenter);
}

QString textHAlignmentPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * textHAlignmentPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("left");
    cb->addItem("center");
    cb->addItem("right");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(textHAlignmentChanged(QString)));
    return cb;
}

void textHAlignmentPropEditor::textHAlignmentChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(textHAlignmentChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(textHAlignmentChanged(QString)));
    }
}

textBgClrPropEditor::textBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString textBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Window).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).blueF(), 'f', 3) + "]";
    return ret;
}

bool textBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void textBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Window, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString textBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * textBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void textBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

autoSizeTextPropEditor::autoSizeTextPropEditor(abstractUICCtrl *ctrl)
    :abstractPropEditor(ctrl)
{
    this->defValue = true;
    this->propName = "isAutoSize";
}

QString autoSizeTextPropEditor::getValue()
{
    QString ret;
    ret = "false";
    if(this->defValue)
        ret = "true";
    return ret;
}

bool autoSizeTextPropEditor::isValidValue(QString newVal)
{
    return (newVal.toLower() == "true") || (newVal.toLower() == "false");
}

void autoSizeTextPropEditor::setValue(QString newVal)
{
    if(isValidValue(newVal))
        this->defValue = (newVal == "true");
}

bool autoSizeTextPropEditor::canGenerateCode()
{
    return false;
}

QWidget * autoSizeTextPropEditor::getEditWidget()
{
    checkBoxUpdPropWdg * cb = new checkBoxUpdPropWdg(this);
    cb->setChecked(this->defValue);
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(autoSizeChanged(int)));
    return cb;
}

void autoSizeTextPropEditor::autoSizeChanged(int state)
{
    checkBoxUpdPropWdg * cb = (checkBoxUpdPropWdg *)QObject::sender();
    if(state == Qt::Unchecked)
    {
        this->setValue("false");
    }
    else if(state == Qt::Checked)
    {
        this->setValue("true");
    }
    else
    {
        disconnect(cb, SIGNAL(stateChanged(int)), this, SLOT(autoSizeChanged(int)));
        if(this->getValue().toLower() == "true")
            cb->setChecked(true);
        else
            cb->setChecked(false);
        connect(cb, SIGNAL(stateChanged(int)), this, SLOT(autoSizeChanged(int)));
    }
}


textFgClrPropEditor::textFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString textFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::WindowText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).blueF(), 'f', 3) + "]";
    return ret;
}

bool textFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void textFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::WindowText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString textFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * textFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void textFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int textCtrl::uicNameCounter = 0;

unsigned int textCtrl::getNameCounter()
{
    return uicNameCounter;
}

textCtrl::textCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWndDlg):abstractUICCtrl(parent, octaveParent, parentWndDlg)
{
    editableText * text = new editableText(parent, this);
    uicNameCounter++;
    this->setCtrlName("Label_"+QString::number(uicNameCounter));       

    this->setAssociatedWidget(text);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new textTextPropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new textBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new textHAlignmentPropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new autoSizeTextPropEditor(this));    
    this->properties.registerProp(new textFgClrPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));
    this->properties.getPropByName("String")->setValue(this->getCtrlName());

    text->setAlignment(Qt::AlignLeft);
}
void textCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableText * text = dynamic_cast<editableText *>(this->associatedWidget());
    text->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList textCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','text'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList textCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

bool textCtrl::isAutoSize()
{
    return true;
}

abstractUICCtrl * textCtrl::clone(QWidget *parent,
                                  abstractUICCtrl *octaveParent,
                                  childWndDlg * parentWnd){
    textCtrl * ret = new textCtrl(parent,
                                  octaveParent,
                                  parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void textCtrl::setHidden(bool isHidden){
    ((editableText *)this->associatedWidget())->setHidden(isHidden);
}
textCtrl::~textCtrl()
{
  delete this->getAssociatedWidget();
}

textCtrlGen::textCtrlGen():controlGenerator("textCtrl")
{
}

abstractUICCtrl * textCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    textCtrl * txt = new textCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = txt->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = txt->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      txt->setSrcCallBack(src.split('\n'));

    txt->deselect();
    return txt;

}


