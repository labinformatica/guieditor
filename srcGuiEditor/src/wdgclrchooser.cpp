/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgclrchooser.h"
#include "ui_wdgclrchooser.h"
#include <QColorDialog>


wdgClrChooser::wdgClrChooser(abstractPropEditor * prop) :
    QWidget(0),updPropWdg(prop),
    ui(new Ui::wdgClrChooser)
{
    ui->setupUi(this);
}

wdgClrChooser::~wdgClrChooser()
{
    delete ui;
}

void wdgClrChooser::setColor(QString v)
{
    ui->leColor->setText(v);
}

QString wdgClrChooser::getColor()
{
    return ui->leColor->text();
}

QLineEdit *wdgClrChooser::getLEColor()
{
    return ui->leColor;
}

void wdgClrChooser::updPropValue()
{
    ui->leColor->setText(this->getPropEditor()->getValue());
}

void wdgClrChooser::on_btnColorChooser_clicked()
{
    QColor clr;
    QColor ret;
    QString strColor = ui->leColor->text();
    strColor.remove("[");
    strColor.remove("]");
    QStringList v = strColor.split(" ");
    if(v.count() == 3)
        clr = QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat());
    else
        clr = Qt::black;
    ret = QColorDialog::getColor(clr, this, "Select color...");
    if(ret.isValid())
    {
        strColor = "[";
        strColor += QString::number(ret.redF(), 'f', 3) + " ";
        strColor += QString::number(ret.greenF(), 'f', 3) + " ";
        strColor += QString::number(ret.blueF(), 'f', 3) + "]";
        ui->leColor->setText(strColor);
    }
    this->getPropEditor()->setValue(strColor);
}
