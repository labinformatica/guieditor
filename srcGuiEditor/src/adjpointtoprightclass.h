/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ADJPOINTTOPRIGHTCLASS_H
#define ADJPOINTTOPRIGHTCLASS_H

#include <QWidget>

//! Representa el punto de ajuste en un control cuando es seleccionado
/*!
  Cuando se selecciona un control, sea cual sea su tipo, aparecen un conjunto de puntos
  al rededor. Estos puntos permiten cambiar el tamaño del control al pasar al mover el punto
  de ajuste haciendo click sobre el mismo y soltando el mouse.
  Esta clase representa el punto de control que se ubica en la esquina superior derecha.

  Debido a que es necesario dibujar de modo personalido el control, esta clase
  deriva de QWidget. De esta manera se logra una representación particular del control
  en función de su lugar. Su posición no es determinada de modo externo, sino que
  se calcula en función del control al que se asocia el punto de control. El control asociado
  es referenciado a través del atributo privado asociatedControl. El control recibido como
  argumento en el constructor es utilizado para acceder al control que contiene el control
  que ha sido seleccionado. En un caso típico, si se tiene un butón sobre una ventana,
  el puntero al botón estaría almacenado en el atributo "asociatedControl" mientras que
  el puntero a la ventana que contiene el control sería un argumento del constructor.

  En lo que respecta a la lógica con la que se logra el redimensionamiento del control
  asociado, esta es simple. Se considera que se tiene una estructura de 3 estados:
  - Cuando se presiona en el en el punto de ajuste, una bandera (resizing) activada (=true).
  - Si se dispara el evento "mouse move", y por ende, se ejecuta el método público virtual
  mouseMoveEvent, se verifica el estado de la vandera (resizing). Si está activa (true) se
  redimensiona el control asociado.
  - Cuando el botón se suelta (mouseReleaseEvent) se actualiza la bandera (resizing)

  \note Todos los puntos de ajuste son cuadrados de 5 pixeles por 5 pixeles. Esto es un valor
  establecido sin posibilidad de configuración en esta versión. Se prevee que en las próximas versiones
  el tamaño del punto de control sea configurable.

*/
class adjPointTopRightClass : public QWidget
{
    Q_OBJECT
private:
    int moveXInit;
    int moveYInit;
    bool resizing;
    bool canResize;
    QWidget * asociatedControl;
public:
    //! Constructor de la clase, toma como argumento el control (QWidget) de nivel superior, dentro del cuál se dibuja el widget
    explicit adjPointTopRightClass(QWidget *parent);
    //! Setter para el control asociado al punto de control.
    /*!
      Este método debe ser invocado de modo establecer la correspondencia entre el punto de control
      y las acciones que sobre él se realicen con el control en cuestión.
      Además, establece las coordenadas donde el control debe dibujarse.
    */
    void asociateCtrl(QWidget * ctrl, bool canResize = true);
    //! Re posiciona el punto de ajuste en función del tamaño y posición del control.
    void resetPosition();
signals:
    
public slots:
protected:
    //! Representa el control en el widget que lo contiene
    /*! Este método es invocado de modo automático, en todos los widgets, para que se represente en la pantalla.
      Según sea necesario actualizar la representación del control, se invocará a este método.
      */
    void paintEvent(QPaintEvent * event);
    //! Este método es utilizado para registrar que se ha soltado el mouse y actualizar la bandera respectiva.
    virtual void mouseReleaseEvent(QMouseEvent *);
    //! Registra cuando se presiona el mouse sobre el control y actualiza la bandera de estado.
    virtual void mousePressEvent(QMouseEvent * event);
    //! Registra el movimiento del mouse y actualiza el tamaño del widget asociado.
    /*!
      Para calcular el tamaño al que debe actualizarce el widget asociado se registran, al precionar
      el botón izquierdo del mouse las coordenadas donde se hizo click. Estos valores se almacenan en los
      atributos moveXInit y moveYInit (enteros, privados). Luego, según el mouse se desaplaza, se
      calcula la diferencia entre el valor de las coordenadas actuales y las coordenadas iniciales.
    */
    virtual void mouseMoveEvent(QMouseEvent * event);
};

#endif // ADJPOINTTOPRIGHTCLASS_H
