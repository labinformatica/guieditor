/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "imgctrl.h"
#include "editableimg.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"

unsigned int imgCtrl::uicNameCounter = 0;

unsigned int imgCtrl::getNameCounter()
{
    return uicNameCounter;
}

imgCtrl::imgCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd)
    :abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableImage * img = new editableImage(parent, this);
    uicNameCounter++;
    this->setCtrlName("Image_"+QString::number(uicNameCounter));
    img->resize(80, 80);

    this->setAssociatedWidget(img);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));
}

void imgCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableImage * img = dynamic_cast<editableImage *>(this->associatedWidget());
    img->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList imgCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = axes( ...") ;
    ret.append("\t'Units', 'pixels'");
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList imgCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * imgCtrl::clone(QWidget *parent,
                                 abstractUICCtrl *octaveParent,
                                 childWndDlg * parentWnd){
    imgCtrl * ret = new imgCtrl(parent,
                                octaveParent,
                                parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void imgCtrl::setHidden(bool isHidden){
    ((editableImage *)this->associatedWidget())->setHidden(isHidden);
}

imgCtrl::~imgCtrl()
{
  delete this->getAssociatedWidget();
}

imgCtrlGen::imgCtrlGen():controlGenerator("imgCtrl")
{
}

abstractUICCtrl * imgCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    imgCtrl * img = new imgCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = img->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = img->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      img->setSrcCallBack(src.split('\n'));

    img->deselect();
    return img;

}


