/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "adjpointmidleleftclass.h"

#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QDebug>
#include <QPaintEvent>

/*
 +
 +         *------------*------------*
 +         |                         |
 +      >  *                         *
 +         |                         |
 +         *------------*------------*
 +
 */

adjPointMidleLeftClass::adjPointMidleLeftClass(QWidget *parent) :
    QWidget(parent)
{
    this->asociatedControl = NULL;
    this->canResize = true;
    this->resizing = false;
}

void adjPointMidleLeftClass::asociateCtrl(QWidget * ctrl, bool canResize)
{
    this->canResize = canResize;
    this->asociatedControl = ctrl;
    if (this->canResize)
        this->setCursor(Qt::SizeHorCursor);
    this->setGeometry(this->asociatedControl->x()-3,
                      this->asociatedControl->y() + (this->asociatedControl->height()/2) - 3,
                      5,
                      5);
}

void adjPointMidleLeftClass::resetPosition()
{
    this->setGeometry(this->asociatedControl->x()-3,
                      this->asociatedControl->y() + (this->asociatedControl->height()/2) - 3,
                      5,
                      5);
}

void adjPointMidleLeftClass::paintEvent(QPaintEvent * event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    QBrush brush(QColor::fromRgb(120,120,120));
    QPen penBlack(Qt::black);
    QPen penLight(QColor::fromRgb(200,200,200));
    penBlack.setWidth(1);
    penBlack.setStyle(Qt::SolidLine);
    penLight.setWidth(1);
    penLight.setStyle(Qt::SolidLine);
    brush.setStyle(Qt::SolidPattern);
    painter.setPen(penLight);
    painter.setBrush(brush);
    this->setGeometry(this->asociatedControl->x()-2,
                      this->asociatedControl->y() + (this->asociatedControl->height()/2) - 2,
                      5,
                      5);

    painter.fillRect(0, 0, (int)this->width()-1, (int)this->height()-1,QColor::fromRgb(120,120,120));

    painter.drawLine(0, 0, (int)this->width()-1, 0);
    painter.drawLine(0, 0, 0, (int)this->height()-1);

    painter.setPen(penBlack);
    painter.drawLine((int)this->width()-1, (int)this->height()-1, 0, (int)this->height()-1);
    painter.drawLine((int)this->width()-1, (int)this->height()-1, (int)this->width()-1,0);
}

void adjPointMidleLeftClass::mouseReleaseEvent(QMouseEvent * )
{
    if(this->canResize)
      this->resizing = false;
}

void adjPointMidleLeftClass::mousePressEvent(QMouseEvent * event)
{
    if(this->canResize)
    {
      this->moveXInit = event->x();
      this->moveYInit = event->y();
      this->resizing = true;
    }
}

void adjPointMidleLeftClass::mouseMoveEvent(QMouseEvent * event)
{
    if(this->resizing && this->canResize)
    {
        int dx = event->x()-this->moveXInit;
        QRect g = this->asociatedControl->geometry();
        int origWidth = g.width();
        g.setLeft(g.left() + dx);
        g.setWidth(origWidth - dx);
        this->asociatedControl->setGeometry(g);
    }
}
