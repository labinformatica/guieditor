/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wnditemedit.h"
#include "ui_wnditemedit.h"

wndItemEdit::wndItemEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::wndItemEdit)
{
    ui->setupUi(this);
}

wndItemEdit::~wndItemEdit()
{
    delete ui;
}

void wndItemEdit::on_btnAdd_clicked()
{
    ui->listItem->addItem(ui->leNewVal->text());
}

void wndItemEdit::on_btnDel_clicked()
{
    delete ui->listItem->takeItem(ui->listItem->currentRow());

}

void wndItemEdit::on_btnUp_clicked()
{
    QListWidgetItem * currentItem;
    int currentRow = ui->listItem->currentRow();
    if (currentRow > 0)
    {
      currentItem = ui->listItem->takeItem(currentRow);
      ui->listItem->insertItem(currentRow - 1, currentItem);
      ui->listItem->setCurrentRow(currentRow - 1);
    }
}

void wndItemEdit::on_btnDown_clicked()
{
    QListWidgetItem * currentItem;
    int currentRow = ui->listItem->currentRow();
    if (currentRow < ui->listItem->count()-1)
    {
      currentItem = ui->listItem->takeItem(currentRow);
      ui->listItem->insertItem(currentRow + 1, currentItem);
      ui->listItem->setCurrentRow(currentRow + 1);
    }
}

void wndItemEdit::on_btnAceptar_clicked()
{

}
