/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef TABLECTRL_H
#define TABLECTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QTableWidget>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"

class tableBgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    tableBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class tableFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    tableFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class tableRearrangePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    bool value;
public:
    tableRearrangePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};

class tableRowStripingPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    bool value;
public:
    tableRowStripingPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged(QString text);
};

class tableCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

public:
    static unsigned int getNameCounter();
    tableCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual QString className() { return "tableCtrl";}
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~tableCtrl();
};

class tableCtrlGen: public controlGenerator
{
public:
  tableCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // TABLECTRL_H


