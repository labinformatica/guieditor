/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "childwnddlg.h"
#include <framedlg.h>
#include <QFocusEvent>
#include <QCloseEvent>
#include <qmessagebox.h>
#include <QFileDialog>
#include <QTextStream>

#include "storagemanager.h"
#include <QMenu>

#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QToolBar>
#include <QHeaderView>
#include "newfilewnd.h"
#include "ui_newfilewnd.h"

treeItemDialog::treeItemDialog(guiProject * p, childWndDlg * dlg, QTreeWidgetItem *parent):
    abstractPrjTreeItem(parent)
{
    this->associatedPrj = p;
    this->setText(0, dlg->getFrame()->getCtrlName());
    this->setToolTip(0, dlg->documentName);
    this->setIcon(0, QPixmap(":/img/dlgItem"));
    this->dlg = dlg;
    if(this->dlg == p->getMainDlg())
    {
        this->setText(0, dlg->getFrame()->getCtrlName() + "[*]");
    }
}

void treeItemDialog::activate()
{
    dlg->show();    
    mainWnd::getMainWnd()->ui->mdiArea->setActiveSubWindow(dlg);
    mainWnd::getPrjWnd()->setActiveProject(this->associatedPrj);
}

void treeItemDialog::populateMenu(QMenu &m)
{
    m.addAction(tr("Set as main dialog"), this, SLOT(actSetAsMainDlg(bool)));
    m.addAction(tr("Duplicate"), this, SLOT(actDuplicateDlg(bool)));
    m.addAction(tr("Rename"), this, SLOT(actRenameDlg(bool)));
    m.addAction(tr("Delete ") + dlg->getFrame()->getCtrlName(), this, SLOT(actDelete(bool)));
}

void treeItemDialog::actDuplicateDlg(bool checked __attribute__((unused))){
    newFileWnd wnd(this->associatedPrj);
    wnd.setWindowTitle(tr("Name for dialog copy"));
    if(wnd.exec() == QDialog::Accepted)
    {
      childWndDlg * newDlg = this->dlg->clone(
                  dlg->parentWidget(),
                  dlg->windowFlags(),
                  dlg->getSrcWnd(),
                  dlg->getProPanel(),
                  dlg->getGUIPrj());

      newDlg->getFrame()->setCtrlName(wnd.ui->leFileName->text());
      newDlg->save(this->associatedPrj->guiPath() + QDir::separator() + wnd.ui->leFileName->text() + ".xml");

      mainWnd::getMainWnd()->ui->mdiArea->addSubWindow(newDlg, Qt::SubWindow);
      newDlg->show();
      newDlg->setWindowModified(false);
      newDlg->getFrame()->updateSrcWndCtrls();
      newDlg->getFrame()->setCanChangeName(false);
      abstractPrjTreeItem * dlgItem = newDlg->treeItemWidget(this->associatedPrj->getTreeItem());
      this->associatedPrj->getTreeItem()->addChild(dlgItem);
      this->associatedPrj->getTreeItem()->setExpanded(true);
      this->associatedPrj->items.push_back(newDlg);
    }
}


void treeItemDialog::actRenameDlg(bool checked __attribute__((unused))){
    newFileWnd wnd(this->associatedPrj);
    wnd.setWindowTitle(tr("New name for dialog"));
    wnd.ui->btnAdd->setText("Rename");
    if(wnd.exec() == QDialog::Accepted)
    {
        QString previusName = dlg->documentName;
        dlg->getFrame()->setCanChangeName(true);
        dlg->getFrame()->setCtrlName(wnd.ui->leFileName->text());
        dlg->save(this->associatedPrj->guiPath() + QDir::separator() + wnd.ui->leFileName->text() + ".xml");
        dlg->getFrame()->setCanChangeName(false);
        QFile::remove(previusName);
        this->setText(0, dlg->getFrame()->getCtrlName());
    }
}

void treeItemDialog::actDelete(bool checked __attribute__((unused)))
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(mainWnd::getMainWnd(),
                                  QObject::tr("Warning!!! This operation cannot be undone"),
                                  QObject::tr("You are trying to delete the file %1, this operation can't be undone. Are you sure to delete this file?").arg(this->dlg->documentName),
                                  QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        QFile toDelete(this->dlg->documentName);
        toDelete.remove();
        if(this->dlg == this->associatedPrj->getMainDlg())
            this->associatedPrj->setMainDlg(NULL);
        this->dlg->close();
        this->associatedPrj->items.removeAll(this->dlg);
        mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this->dlg);
        this->dlg->deleteLater();
        mainWnd::getPrjWnd()->refreshList();
    }
}

void treeItemDialog::selectProject()
{
    mainWnd::getPrjWnd()->setActiveProject(this->associatedPrj);
}

void treeItemDialog::actSetAsMainDlg(bool checked __attribute__((unused)))
{
   this->associatedPrj->setMainDlg(this->dlg);
    mainWnd::getPrjWnd()->refreshList();
}

void childWndDlg::closeEvent(QCloseEvent * closeEvent)
{
    QMessageBox::StandardButton reply;    
    if (this->isWindowModified() && (this->testAttribute(Qt::WA_DeleteOnClose)))
    {
        reply = QMessageBox::question(this,
                                      tr("Se perderá información"),
                                      tr("Las modificaciones realizadas no se han guardado. Desea salir de todos modos?"),
                                      QMessageBox::Yes | QMessageBox::No);
        if(reply == QMessageBox::Yes)
        {
            closeEvent->accept();
            if(this->mdiArea()->subWindowList().count() == 1)
               this->getSrcWnd()->closeDlg();
        }
        else
            closeEvent->ignore();
    }
    else
       this->getSrcWnd()->closeChildWnd();
}


void generateContainerList(QList<abstractUICCtrl *> &lst, QList<abstractUICCtrl *> & process)
{
  QList<abstractUICCtrl *>::iterator it;
  for(it = process.begin(); it != process.end(); it++)
      if((*it)->className() == "frameDlg")
      {
          lst.push_back((*it));
      }
      else if((*it)->isContainer())
      {
          lst.push_back((*it));
          generateContainerList(lst, (*it)->addedWidgets);
      }
}

childWndDlg::childWndDlg(QWidget *parent, Qt::WindowFlags flags, wdgSrcEditor *srcEdPanel, wdgProp *propPanel, guiProject * prj):
    abstractChildWnd(parent, flags), projectItem("dialog", prj)
{
    this->documentHaveName = false;
    this->documentName = "";
    this->propPanel = propPanel;
    this->srcEdPanel = srcEdPanel;
    this->setSelectionState(selectionNone);


//    QWidget *mainWdg = new QWidget(this);
//    this->frm = new frameDlg(mainWdg, this);
//    QVBoxLayout* vbox = new QVBoxLayout;
//    vbox->addWidget(topBar);
//    vbox->addWidget(this->frm);
//    vbox->setContentsMargins(0,0,0,0);
//    vbox->setSpacing(0);
//    mainWdg->setLayout(vbox);
//    this->setWidget(mainWdg);

    this->frm = new frameDlg(this, this);
    this->setWidget(this->frm);

    this->xInit = 0;
    this->yInit = 0;
    this->selWidth = 0;
    this->selHeight = 0;

    ////

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(ShowContextMenu(const QPoint &)));
}

void childWndDlg::ShowContextMenu(const QPoint &pos)
{
   QMenu contextMenu(this);   
   QList<abstractUICCtrl *> containersList;
   QList<abstractUICCtrl *>::iterator it;

   contextMenu.addAction(QPixmap (":/img/img/leftArrow.png"),tr("Align left"),this,SLOT(alignToLeft()));
   contextMenu.addAction(QPixmap (":/img/img/hCenterArrow.png"),tr("H center"),this,SLOT(alignToHCenter()));
   contextMenu.addAction(QPixmap (":/img/img/rightArrow.png"),tr("Aling right"),this,SLOT(alignToRight()));

   contextMenu.addSeparator();

   contextMenu.addAction(QPixmap (":/img/img/upArrow.png"),tr("Align top"),this,SLOT(alignToTop()));
   contextMenu.addAction(QPixmap (":/img/img/vCenterArrow.png"),tr("V center"),this,SLOT(alignToVCenter()));
   contextMenu.addAction(QPixmap (":/img/img/downArrow.png"),tr("Align bottom"),this,SLOT(alignToBottom()));

   contextMenu.addSeparator();

   contextMenu.addAction(QPixmap (":/img/img/alignHDlg.png"),tr("Center in dialog (H)"),this,SLOT(alignHDlg()));
   contextMenu.addAction(QPixmap (":/img/img/alignVDlg.png"),tr("Center in dialog (V)"),this,SLOT(alignVDlg()));

   contextMenu.addSeparator();

   contextMenu.addAction(QPixmap (":/img/img/heightToLarge.png"),tr("Resize to highest"),this,SLOT(heightToLarge()));
   contextMenu.addAction(QPixmap (":/img/img/heightToShort.png"),tr("Resize to small"),this,SLOT(heightToSmall()));

   contextMenu.addSeparator();

   contextMenu.addAction(QPixmap (":/img/img/widthToLarge.png"),tr("Resize to large"),this,SLOT(widthToLarge()));
   contextMenu.addAction(QPixmap (":/img/img/wdithToShort.png"),tr("Resize to small"),this,SLOT(widthToSmall()));

   contextMenu.addSeparator();

   generateContainerList(containersList, frm->addedWidgets);

   QMenu * subMenu = contextMenu.addMenu(tr("Move to:"));
   for(it = containersList.begin(); it != containersList.end(); it++)
   {
       QAction *act = subMenu->addAction((*it)->getCtrlName(), this, SLOT(changeParent()));
       act->setData(QVariant::fromValue(*it));
   }


   contextMenu.exec(mapToGlobal(pos));
}


bool childWndDlg::isModified()
{
    return  isWindowModified();
}

QString childWndDlg::getName()
{
    return this->getFrame()->getCtrlName();
}

void childWndDlg::focusOutEvent(QFocusEvent * focusOutEvent)
{
    if(focusOutEvent->lostFocus())
    {
      if(this->getSrcWnd())
          this->getSrcWnd()->saveSourceCode();
    }
    QMdiSubWindow::focusOutEvent(focusOutEvent);
}

void childWndDlg::focusInEvent(QFocusEvent * focusInEvent)
{    
    if(focusInEvent->gotFocus() && this->getSrcWnd())
    {
      this->selectionChanged();
    }
    QMdiSubWindow::focusInEvent(focusInEvent);
}

childWndDlg::~childWndDlg()
{

}

void childWndDlg::save(QString fileName)
{
   this->documentHaveName = true;
   this->documentName = fileName;
   storageManager::saveDlgToFile(this, fileName);
   this->setWindowModified(false);
   QFileInfo fileInfo(fileName);
   this->setWindowTitle(fileInfo.fileName());
}

void childWndDlg::save()
{
     if(!this->documentHaveName)
     {
       QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("XML Dialogs (*.dlg);;All files (*.*);;"), 0);
       if(!fileName.isNull())
       {
           this->documentHaveName = true;
           this->documentName = fileName;           
           storageManager::saveDlgToFile(this, fileName);
           this->setWindowModified(false);
           QFileInfo fileInfo(fileName);
           this->setWindowTitle(fileInfo.fileName());
       }
     }
     else
     {
         mainWnd::getSrcWnd()->saveSourceCode();
         storageManager::saveDlgToFile(this, this->documentName);
         this->setWindowModified(false);
     }
}

void childWndDlg::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("XML Dialogs (*.xml);;All files (*);;"), 0);
    if(!fileName.isNull())
    {
        this->documentHaveName = true;
        this->documentName = fileName;
        storageManager::saveDlgToFile(this, this->documentName);
        this->setWindowModified(false);
        QFileInfo fileInfo(fileName);
        this->setWindowTitle(fileInfo.fileName());
     }

}

void childWndDlg::load(QString fn)
{
      QFileInfo fileInfo(fn);
      storageManager::loadDlgFromFile(this, fn);
      this->documentHaveName = true;
      this->documentName = fn;
      this->setWindowTitle(fileInfo.fileName());
      this->setWindowModified(false);
 }

void childWndDlg::run(QString path)
{
    QFile f;
    this->exportAsScript(path);
    f.setFileName(path + "/runApp.m");
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream txt(&f);
    txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
    txt << "function runApp()" << endl;
    txt << "  [dir, name, ext] = fileparts( mfilename('fullpathext') );" << endl;
    txt << "  addpath( [dir filesep() \"wnd\" ]);" << endl;
    txt << " waitfor(" + this->getFrame()->getCtrlName() + ".figure);" << endl;

    txt << "end" << endl;
    f.close();
}

void childWndDlg::exportAsScript(QString path)
{        
    QFile f;
    QFile fDlg;
    QDir outPath (path);
    QString dlgName;
    QStringList MFile;
    if(!outPath.exists(path + "/wnd"))
        outPath.mkdir(path + "/wnd");
    outPath.cd(path + "/wnd");
    frameDlg * frm = getFrame();
    dlgName = frm->properties.getValueByName("Name");

    f.setFileName(outPath.path() + "/" + dlgName + "_def.m");
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream txt(&f);
    txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
    MFile = frm->generateMFile(outPath.path());
    for (QStringList::Iterator it = MFile.begin(); it != MFile.end(); ++it )
       txt << *it << endl;
    f.close();

    fDlg.setFileName(outPath.path() + "/" + dlgName + ".m");
    fDlg.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream txtDlg(&fDlg);
    txtDlg.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
    txtDlg << "## -*- texinfo -*-" << endl;
    txtDlg << "## @deftypefn  {} {@var{wnd} =} " + dlgName +" ()" << endl;
    txtDlg << "##" << endl;
    txtDlg << "## Create and show the dialog, return a struct as representation of dialog." << endl;
    txtDlg << "##" << endl;
    txtDlg << "## @end deftypefn" << endl;

    txtDlg << "function wnd = " + dlgName + "(varargin)" << endl;
    for(int i = 0; i < this->getVarDefinitions().count(); i++)
        txtDlg << this->getVarDefinitions()[i] << endl;
    txtDlg << "  " + dlgName + "_def;" << endl;
    txtDlg << "  wnd = show_" + dlgName + "(varargin{:});" << endl;
    txtDlg << "end" << endl;
    fDlg.close();
}

void childWndDlg::activate(int line , int col)
{
    abstractUICCtrl * ctrl = nullptr;
    int offset;
    this->show();
    mainWnd::getMainWnd()->ui->mdiArea->setActiveSubWindow(this);
    mainWnd::getPrjWnd()->setActiveProject(this->getGUIPrj());
    if((line != -1) && (col != -1)){
        ctrl = this->frm->mapCodeLine(line, offset);
        if(ctrl){
            this->deselectAllUIC();
            this->addToSelection(ctrl);
            this->srcEdPanel->setCursorLine(offset);           
        }
    }
}

void childWndDlg::closingProject()
{
    close();
    mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this);
}

abstractPrjTreeItem * childWndDlg::treeItemWidget(abstractPrjTreeItem *treeItem)
{
    return new treeItemDialog(dynamic_cast<treeItemProject*>(treeItem)->getPrj(), this, treeItem);
}

childWndDlg * childWndDlg::clone(QWidget *parent, Qt::WindowFlags flags,
                                 wdgSrcEditor * srcEdPanel, wdgProp *propPanel, guiProject *prj){
    childWndDlg * ret = new childWndDlg(
                parent,
                flags,
                srcEdPanel,
                propPanel,
                prj);
    ret->setWidget(nullptr);
    delete ret->frm;
    ret->frm = (frameDlg *)this->frm->clone(ret, ret->frm, ret);
    ret->setWidget(ret->frm);
    ret->deselectAllUIC();
    ret->update();
    return ret;
}

void childWndDlg::setSelectionState(eSelectionState newState)
{
    this->vSelectionState = newState;
}

eSelectionState childWndDlg::selectionState()
{
    return this->vSelectionState;
}

void childWndDlg::addToSelection(abstractUICCtrl *ctrToAdd)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        if(this->selectedWidgets.count(ctrToAdd) == 0)
        {
            this->selectedWidgets.push_back(ctrToAdd);
            this->selectionChanged();
        }
        else
        {
            this->removeFromSelection(ctrToAdd);
            this->selectionChanged();
        }
    }
    else
    {
        this->deselectAllUIC();
        this->selectedWidgets.push_back(ctrToAdd);
        this->selectionChanged();
    }
    for(ctrl  = selectedWidgets.begin(); ctrl != selectedWidgets.end(); ++ctrl)
        (*ctrl)->select();
}

bool childWndDlg::isSelected(abstractUICCtrl *ctrl)
{
    return this->selectedWidgets.contains(ctrl);
}

void childWndDlg::removeFromSelection(abstractUICCtrl *ctrl)
{
    this->selectedWidgets.removeOne(ctrl);
    ctrl->deselect();
    this->selectionChanged();
}

void childWndDlg::deselectAllUIC()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = selectedWidgets.begin(); ctrl != selectedWidgets.end(); ++ctrl)
        (*ctrl)->deselect();
    this->selectedWidgets.clear();
    this->selectionChanged();
}

void childWndDlg::selectionChanged()
{
    abstractUICCtrl * ctrl;
    QList<abstractUICCtrl *>::Iterator itCtrl;
    QList<abstractUICCtrl *>::Iterator subCtrl;
    abstractPropEditor * prop;
    int i = 0;

    /***
     * Un problema que se da durante la edición visual de los controles en una ventana de diálogo
     * se produce cuando se selecciona al mismo tiempo un control "no contenedor" y un control
     * que contiene otros controles. El problema es que al mover o actuar sobre la posición de uno
     * y otro el espacio de desplazamiento es incremental ya que no tienen el mismo punto inicial de
     * referencia.
     ***/
    if(selectedWidgets.count() > 1)
    {
        for(itCtrl  = selectedWidgets.begin(); itCtrl != selectedWidgets.end(); ++itCtrl)
            if((*itCtrl)->isContainer())
                for(subCtrl  = (*itCtrl)->addedWidgets.begin(); subCtrl != (*itCtrl)->addedWidgets.end(); ++subCtrl)
                {
                    (*subCtrl)->deselect();
                    this->selectedWidgets.removeOne(*subCtrl);
                }
    }

    if(selectedWidgets.count() == 0)
    {
        mainWnd::getPropPan()->getTblProp()->setRowCount(this->getFrame()->properties.getCount());
        mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
        prop = this->getFrame()->properties.getFirst();
        while(prop)
        {
            mainWnd::getPropPan()->getTblProp()->setCellWidget(i,0,prop->getTitleWidget());
            mainWnd::getPropPan()->getTblProp()->setCellWidget(i,1,prop->getEditWidget());
            mainWnd::getPropPan()->getTblProp()->setRowHeight(i, 22);
            /* Fix backup problem on checkbox controls (haveCallBack options) and is set to
               window propertys to have the same aspect */
            mainWnd::getPropPan()->getTblProp()->cellWidget(i, 1)->setAutoFillBackground(true);
            mainWnd::getPropPan()->getTblProp()->cellWidget(i, 1)->setBackgroundRole(QPalette::Base);

            prop = this->getFrame()->properties.getNext();
            i++;
        }
        //this->srcWnd->saveSourceCode();  // Con esto se resuelve guardar el código al conmutar ventanas.
        this->srcEdPanel->updateWidgetList(this->getFrame()->addedWidgets);
        this->srcEdPanel->actualWidget(this->getFrame());
    }
    else if(selectedWidgets.count() == 1)
    {
        ctrl = this->selectedWidgets.first();
        mainWnd::getPropPan()->getTblProp()->setRowCount(ctrl->properties.getCount());
        mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
        prop = ctrl->properties.getFirst();
        while(prop)
        {
            mainWnd::getPropPan()->getTblProp()->setCellWidget(i,0,prop->getTitleWidget());
            mainWnd::getPropPan()->getTblProp()->setCellWidget(i,1,prop->getEditWidget());
            mainWnd::getPropPan()->getTblProp()->setRowHeight(i, 22);
            /* Fix backup problem on checkbox controls (haveCallBack options)*/
            mainWnd::getPropPan()->getTblProp()->cellWidget(i, 1)->setAutoFillBackground(true);
            mainWnd::getPropPan()->getTblProp()->cellWidget(i, 1)->setBackgroundRole(QPalette::Base);

            prop = ctrl->properties.getNext();
            i++;
        }
        //this->srcWnd->saveSourceCode(); // Con esto se resuelve guardar el código al conmutar ventanas.
        this->srcEdPanel->updateWidgetList(this->getFrame()->addedWidgets);
        this->srcEdPanel->actualWidget(ctrl);
    }
    else
    {
        mainWnd::getPropPan()->getTblProp()->setRowCount(0);
        mainWnd::getPropPan()->getTblProp()->verticalHeader()->hide();
    }
}

void childWndDlg::moveSelection(int dx, int dy)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = this->selectedWidgets.begin();ctrl != this->selectedWidgets.end();++ctrl)
        (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x() + dx,
                                          (*ctrl)->associatedWidget()->y() + dy);
}

void childWndDlg::deleteSelection()
{
    abstractUICCtrl * ctrl;
    QList<abstractUICCtrl *>::Iterator itera;
    QList<abstractUICCtrl *>toDelete;

    for(itera  = this->selectedWidgets.begin();
        itera != this->selectedWidgets.end();
        ++itera)
    {
        ctrl = *itera;
        toDelete.push_back(ctrl);
    }

    this->deselectAllUIC();
    for(itera  = toDelete.begin();
        itera != toDelete.end();
        ++itera)
    {
        ctrl = *itera;
        this->getFrame()->addedWidgets.removeOne(ctrl);
        delete ctrl;
    }
    toDelete.clear();
}

void childWndDlg::resizeSelection(int dx, int dy)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
        (*ctrl)->associatedWidget()->resize((*ctrl)->associatedWidget()->width() + dx,
                                            (*ctrl)->associatedWidget()->height() + dy);

}

void childWndDlg::alignHDlg()
{
    int xLeft;
    int xRight;
    int minXLeft = 0;
    int maxXRight = 0;
    int newX;
    int dx;
    bool first = true;
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = selectedWidgets.begin(); ctrl != selectedWidgets.end(); ctrl++)
    {
        if(first)
        {
            xLeft = (*ctrl)->associatedWidget()->x();
            xRight = (*ctrl)->associatedWidget()->x() + (*ctrl)->associatedWidget()->width();
            minXLeft = xLeft;
            maxXRight = xRight;
            first = false;
        }
        else
        {
            xLeft = (*ctrl)->associatedWidget()->x();
            xRight = (*ctrl)->associatedWidget()->x() + (*ctrl)->associatedWidget()->width();
            if(minXLeft > xLeft)
                minXLeft = xLeft;
            if(maxXRight < xRight)
                maxXRight = xRight;
        }
    }
    newX = (this->getFrame()->width()/2) - ((maxXRight - minXLeft) / 2);
    dx = newX - minXLeft;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x() + dx,
                                          (*ctrl)->associatedWidget()->y());
    }
}

void childWndDlg::alignVDlg()
{
    int yTop;
    int yBottom;
    int minYTop = 0;
    int maxYBottom = 0;
    int newY;
    int dy;
    bool first = true;
    QList<abstractUICCtrl *>::Iterator ctrl;
    for(ctrl  = selectedWidgets.begin(); ctrl != selectedWidgets.end(); ctrl++)
    {
        if(first)
        {
            yTop = (*ctrl)->associatedWidget()->y();
            yBottom = (*ctrl)->associatedWidget()->y() + (*ctrl)->associatedWidget()->height();
            minYTop = yTop;
            maxYBottom = yBottom;
            first = false;
        }
        else
        {
            yTop = (*ctrl)->associatedWidget()->y();
            yBottom = (*ctrl)->associatedWidget()->y() + (*ctrl)->associatedWidget()->height();
            if(minYTop > yTop)
                minYTop = yTop;
            if(maxYBottom < yBottom)
                maxYBottom = yBottom;
        }
    }
    newY = (this->getFrame()->height()/2) - ((maxYBottom - minYTop) / 2);
    dy = newY - minYTop;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x(),
                                          (*ctrl)->associatedWidget()->y() + dy);
    }
}

void childWndDlg::alignToLeft()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int newX, newY;
    for(ctrl  = this->selectedWidgets.begin();ctrl != this->selectedWidgets.end();++ctrl)
    {
        if(!firstCtrl)
            firstCtrl = *ctrl;
        else
        {
            newX = firstCtrl->associatedWidget()->x();
            newY = (*ctrl)->associatedWidget()->y();
            (*ctrl)->associatedWidget()->move(newX, newY);
        }
    }
}

void childWndDlg::alignToRight()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int xTarget;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            xTarget = firstCtrl->associatedWidget()->x() + firstCtrl->associatedWidget()->width();
        }
        else
            (*ctrl)->associatedWidget()->move(xTarget - (*ctrl)->associatedWidget()->width(),
                                              (*ctrl)->associatedWidget()->y());

    }
}

void childWndDlg::alignToHCenter()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int xTarget;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            xTarget = firstCtrl->associatedWidget()->x() + firstCtrl->associatedWidget()->width()/2;
        }
        else
            (*ctrl)->associatedWidget()->move(xTarget - (*ctrl)->associatedWidget()->width()/2,
                                              (*ctrl)->associatedWidget()->y());
    }
}

void childWndDlg::alignToTop()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
            firstCtrl = *ctrl;
        else
        {
            (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x(),
                                              firstCtrl->associatedWidget()->y());
        }
    }
}

void childWndDlg::alignToVCenter()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int yTarget;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            yTarget = firstCtrl->associatedWidget()->y() + firstCtrl->associatedWidget()->height()/2;
        }
        else
        {
            (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x(),
                                              yTarget - (*ctrl)->associatedWidget()->height()/2);
        }
    }
}

void childWndDlg::alignToBottom()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int yTarget;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            yTarget = firstCtrl->associatedWidget()->y() + firstCtrl->associatedWidget()->height();
        }
        else
        {
            (*ctrl)->associatedWidget()->move((*ctrl)->associatedWidget()->x(),
                                              yTarget - (*ctrl)->associatedWidget()->height());
        }
    }
}

void childWndDlg::widthToSmall()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int minWidth = 0;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            minWidth = firstCtrl->associatedWidget()->width();
        }
        else
        {
            if((*ctrl)->associatedWidget()->width() < minWidth)
                minWidth = (*ctrl)->associatedWidget()->width();
        }
    }
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
        (*ctrl)->associatedWidget()->resize(minWidth, (*ctrl)->associatedWidget()->height());
}

void childWndDlg::widthToLarge()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int maxWidth = 0;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            maxWidth = firstCtrl->associatedWidget()->width();
        }
        else
        {
            if((*ctrl)->associatedWidget()->width() > maxWidth)
                maxWidth = (*ctrl)->associatedWidget()->width();
        }
    }
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
        (*ctrl)->associatedWidget()->resize(maxWidth, (*ctrl)->associatedWidget()->height());
}

void childWndDlg::heightToSmall()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;
    int minHeight = 0;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            minHeight = firstCtrl->associatedWidget()->height();
        }
        else
        {
            if((*ctrl)->associatedWidget()->height() < minHeight)
                minHeight = (*ctrl)->associatedWidget()->height();
        }
    }
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
        (*ctrl)->associatedWidget()->resize((*ctrl)->associatedWidget()->width(),minHeight);

}

void childWndDlg::heightToLarge()
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    abstractUICCtrl * firstCtrl = NULL;

    int maxHeight = 0;
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
    {
        if(!firstCtrl)
        {
            firstCtrl = *ctrl;
            maxHeight = firstCtrl->associatedWidget()->height();
        }
        else
        {
            if((*ctrl)->associatedWidget()->height() > maxHeight)
                maxHeight = (*ctrl)->associatedWidget()->height();
        }
    }
    for(ctrl  = this->selectedWidgets.begin();
        ctrl != this->selectedWidgets.end();
        ++ctrl)
        (*ctrl)->associatedWidget()->resize((*ctrl)->associatedWidget()->width(), maxHeight);

}

void childWndDlg::changeParent()
{
    int xMin, yMin;
    int aux;
    QRect pos;
    QAction *act = dynamic_cast<QAction *>(QObject::sender());
    abstractUICCtrl * ctrlParent = act->data().value<abstractUICCtrl *>();
    QList<abstractUICCtrl *>::Iterator ctrl;
    bool isRecursive = false;

    for(ctrl = this->selectedWidgets.begin(); ctrl != this->selectedWidgets.end(); ctrl++)
    {
        isRecursive = isRecursive || ((*ctrl) == ctrlParent);
    }
    if(isRecursive)
        QMessageBox::information(this,tr("Warning!"),tr("You cannot move a control inside itself!"));
    if((selectedWidgets.count() > 0) && (!isRecursive))
    {
        xMin = selectedWidgets.first()->associatedWidget()->geometry().left();
        yMin = selectedWidgets.first()->associatedWidget()->geometry().top();
        for(ctrl = this->selectedWidgets.begin(); ctrl != this->selectedWidgets.end(); ctrl++)
        {
            aux = selectedWidgets.first()->associatedWidget()->geometry().left();
            if(aux < xMin)
                xMin = aux;
            aux = selectedWidgets.first()->associatedWidget()->geometry().top();
            if(aux < yMin)
                yMin = aux;
        }

        for(ctrl = this->selectedWidgets.begin(); ctrl != this->selectedWidgets.end(); ctrl++)
        {
            (*ctrl)->getOctaveParent()->addedWidgets.removeOne(*ctrl);
            (*ctrl)->setParents(ctrlParent->associatedWidget(), ctrlParent, this);
            (*ctrl)->setContainnerWidget(ctrlParent->associatedWidget());
            (*ctrl)->setOctaveParent(ctrlParent);
            ctrlParent->addWidget(*ctrl);
            pos = (*ctrl)->associatedWidget()->geometry();
            pos.setLeft(pos.left() - xMin);
            pos.setTop(pos.top() - yMin);
            (*ctrl)->associatedWidget()->move(pos.x()+20, pos.y()+20);
            (*ctrl)->associatedWidget()->show();
            (*ctrl)->select();
        }
    }
}
