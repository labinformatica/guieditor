/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "editablebuttongroup.h"
#include <QPainter>
#include <QRadioButton>
#include "framedlg.h"
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QPushButton>
#include "painthidden.h"

editableButtonGroup::editableButtonGroup(QWidget *parent, abstractUICCtrl *uiCtrl):
    QGroupBox(parent)
{    
    underMovement = false;
    this->setFlat(false);
    this->parentUICtrl = uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
    this->isHidden = false;
    this->setAutoFillBackground(true);
}

void editableButtonGroup::setParents(QWidget *parent, abstractUICCtrl *uiCtrl)
{
    setParent(parent);
    this->parentUICtrl = uiCtrl;
}

int editableButtonGroup::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

void editableButtonGroup::setHiden(bool isHidden){
    this->isHidden = isHidden;
}

editableButtonGroup::~editableButtonGroup()
{

}

void editableButtonGroup::mouseReleaseEvent(QMouseEvent *event)
{
    QList<abstractUICCtrl *>::Iterator ctrl;
    QRect selectionGeometry;
    abstractUICCtrl * newWidget;
    abstractUICCtrl *parent;
    QString baseName;
    QString tempName;
    int cnt;
    if(mainWnd::getMainWnd()->uicState == uicStateNone)
    {
        underMovement = false;
        this->parentUICtrl->updateAdjPoints();
    }
    else if(mainWnd::getMainWnd()->uicState == uicStateAddWidget)
    {
        this->parentUICtrl->getParentWnd()->setSelWidth(cordToGrid(event->x())  - this->parentUICtrl->getParentWnd()->getSelXInit());
        this->parentUICtrl->getParentWnd()->setSelHeight(cordToGrid(event->y()) - this->parentUICtrl->getParentWnd()->getSelYInit());
        if(this->parentUICtrl->getParentWnd()->getSelWidth() < 0)
        {
            this->parentUICtrl->getParentWnd()->setSelXInit(this->parentUICtrl->getParentWnd()->getSelXInit() + this->parentUICtrl->getParentWnd()->getSelWidth());
            this->parentUICtrl->getParentWnd()->setSelWidth(this->parentUICtrl->getParentWnd()->getSelWidth() * -1);
        }
        if(this->parentUICtrl->getParentWnd()->getSelHeight() < 0)
        {
            this->parentUICtrl->getParentWnd()->setSelYInit(
                        this->parentUICtrl->getParentWnd()->getSelYInit() +
                        this->parentUICtrl->getParentWnd()->getSelHeight());
            this->parentUICtrl->getParentWnd()->setSelHeight(
                        this->parentUICtrl->getParentWnd()->getSelHeight() * -1);
        }
        if((this->parentUICtrl->getParentWnd()->getSelHeight() <= 5) || (this->parentUICtrl->getParentWnd()->getSelWidth() <=5))
        {
            this->parentUICtrl->getParentWnd()->setSelWidth(defaultUICWidth);
            this->parentUICtrl->getParentWnd()->setSelHeight(defaultUICHeight);
        }
        mainWnd::getMainWnd()->uicState = uicStateNone;
        /* Name validation */
        // Get main framgeDlg parent
        parent = parentUICtrl;
        while(parent->getOctaveParent())
            parent = parent->getOctaveParent();

        newWidget = mainWnd::getMainWnd()->toAdd;

        baseName = newWidget->getCtrlName();
        tempName = newWidget->getCtrlName();
        cnt = 1;
        while(parent->haveCtrlWithName(tempName, parent)){
            tempName = baseName + QString::number(cnt);
            cnt++;
        }
        newWidget->setCtrlName(tempName);
        newWidget->setParents(this, this->parentUICtrl, this->parentUICtrl->getParentWnd());
        newWidget->setContainnerWidget(this);
        newWidget->setOctaveParent(this->parentUICtrl);

        /* Resolve exclusive radio button and push button implementation*/
        if (newWidget->className() == "radioButtonCtrl")
            ((QRadioButton *)newWidget->associatedWidget())->setAutoExclusive(true);
        else if(newWidget->className() == "toggleCtrl")
            ((QPushButton *)newWidget->associatedWidget())->setAutoExclusive(true);


        this->parentUICtrl->addedWidgets.push_back(newWidget);

        this->parentUICtrl->getParentWnd()->getSrcWnd()->updateWidgetList(this->parentUICtrl->getParentWnd()->getFrame()->addedWidgets, true);
        if(!newWidget->isAutoSize())
        {
            newWidget->associatedWidget()->setGeometry(
                        this->parentUICtrl->getParentWnd()->getSelXInit(),
                        this->parentUICtrl->getParentWnd()->getSelYInit(),
                        this->parentUICtrl->getParentWnd()->getSelWidth(),
                        this->parentUICtrl->getParentWnd()->getSelHeight());
        }
        else
        {
            newWidget->associatedWidget()->move(
                        this->parentUICtrl->getParentWnd()->getSelXInit(),
                        this->parentUICtrl->getParentWnd()->getSelYInit());
        }        
        newWidget->associatedWidget()->show();
        mainWnd::getMainWnd()->toAdd = NULL;
        this->parentUICtrl->getParentWnd()->setWindowModified(true);

        this->parentUICtrl->getParentWnd()->setSelectionState(selectionEnd);
        QGroupBox::update();
    }
    if(this->parentUICtrl->getParentWnd()->selectionState() == selectionStarted)
    {
        this->parentUICtrl->getParentWnd()->setSelectionState(selectionEnd);
        QGroupBox::update();

        selectionGeometry.setX(this->parentUICtrl->getParentWnd()->getSelXInit());
        selectionGeometry.setY(this->parentUICtrl->getParentWnd()->getSelYInit());
        selectionGeometry.setWidth(this->parentUICtrl->getParentWnd()->getSelWidth());
        selectionGeometry.setHeight(this->parentUICtrl->getParentWnd()->getSelHeight());
        if(!(QApplication::keyboardModifiers() & Qt::ShiftModifier))
            this->parentUICtrl->getParentWnd()->deselectAllUIC();
        for(ctrl  = this->parentUICtrl->addedWidgets.begin();
            ctrl != this->parentUICtrl->addedWidgets.end();
            ++ctrl)
            if((*ctrl)->position().intersects(selectionGeometry))
            {
                (*ctrl)->select();
                this->parentUICtrl->getParentWnd()->selectedWidgets.push_back(*ctrl);
            }
        this->parentUICtrl->getParentWnd()->selectionChanged();
    }
}

void editableButtonGroup::mousePressEvent(QMouseEvent * event)
{
    /* Si no estamos agregando un nuevo widget */
    if(mainWnd::getMainWnd()->uicState == uicStateNone)
    {
        underMovement = true;
        moveXInit = this->cordToGrid(event->x());
        moveYInit = this->cordToGrid(event->y());
        if(!this->parentUICtrl->getParentWnd()->isSelected(this->parentUICtrl))
            this->parentUICtrl->getParentWnd()->addToSelection(this->parentUICtrl);
    }
    else if(mainWnd::getMainWnd()->uicState == uicStateAddWidget)
    {
        this->parentUICtrl->getParentWnd()->setSelWidth(0);
        this->parentUICtrl->getParentWnd()->setSelHeight(0);
        this->parentUICtrl->getParentWnd()->setSelXInit(event->x());
        this->parentUICtrl->getParentWnd()->setSelYInit(event->y());
        this->parentUICtrl->getParentWnd()->setSelectionState(selectionStarted);
    }

}

void editableButtonGroup::mouseMoveEvent(QMouseEvent * event)
{
    int dx;
    int dy;
    if(this->parentUICtrl->getParentWnd()->selectionState() == selectionStarted)
    {
        this->parentUICtrl->getParentWnd()->setSelWidth(cordToGrid(event->x()) - this->parentUICtrl->getParentWnd()->getSelXInit());
        this->parentUICtrl->getParentWnd()->setSelHeight(cordToGrid(event->y()) - this->parentUICtrl->getParentWnd()->getSelYInit());
        QGroupBox::update();
    }
    else if(underMovement)
    {
        dx = this->cordToGrid(event->x()) - moveXInit;
        dy = this->cordToGrid(event->y()) - moveYInit;
        this->parentUICtrl->getParentWnd()->moveSelection(dx, dy);
    }
}


void editableButtonGroup::paintEvent(QPaintEvent *event)
{
    QGroupBox::paintEvent(event);
    QPen lapiz(Qt::black);
    QBrush relleno(Qt::white);
    QPainter painter(this);
    lapiz.setWidth(1);
    lapiz.setStyle(Qt::DashLine);
    relleno.setStyle(Qt::NoBrush);
    painter.setPen(lapiz);
    painter.setBrush(relleno);

    int xIni = cordToGrid(event->rect().x());
    int yIni = cordToGrid(event->rect().y());
    for(int x = xIni; x < event->rect().width();x += 5)
        for(int y = yIni; y < event->rect().height(); y += 5)
            painter.drawPoint(x, y);
    if(mainWnd::getMainWnd()->uicState == uicStateAddWidget){
        if(this->parentUICtrl->getParentWnd()->selectionState() == selectionStarted)
            painter.drawRect(this->parentUICtrl->getParentWnd()->getSelXInit(),
                             this->parentUICtrl->getParentWnd()->getSelYInit(),
                             this->parentUICtrl->getParentWnd()->getSelWidth(),
                             this->parentUICtrl->getParentWnd()->getSelHeight());
    }
    painter.end();
    if(this->isHidden)
        paintHidden ph(this);
}




void editableButtonGroup::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (this->parent(), newEvent, 0);
    }
}

void editableButtonGroup::moveEvent ( QMoveEvent * event )
{
    QGroupBox::moveEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableButtonGroup::resizeEvent ( QResizeEvent * event )
{
    QGroupBox::resizeEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}
