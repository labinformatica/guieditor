/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "buttonctrl.h"
#include "editablebutton.h"
#include "commonproperties.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"
#include <QDebug>
#include "wdgiconchooser.h"


btnBgClrPropEditor::btnBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString btnBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Button).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).blueF(), 'f', 3) + "]";
    return ret;
}

bool btnBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void btnBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        float r, g, b;
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        r = v[0].toFloat();
        g = v[1].toFloat();
        b = v[2].toFloat();
        palette = ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Button, QColor::fromRgbF(r, g, b));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString btnBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'" + this->getPropName() + "', " + this->getValue();
    return ret;
}


QWidget * btnBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void btnBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

btnFgClrPropEditor::btnFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString btnFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::ButtonText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::ButtonText).blueF(), 'f', 3) + "]";
    return ret;
}

bool btnFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{    
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void btnFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::ButtonText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString btnFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * btnFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void btnFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text())){
        this->setValue(ed->text());
    }
    else{
        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int buttonCtrl::uicNameCounter = 0;

unsigned int buttonCtrl::getNameCounter()
{
    return uicNameCounter;
}

buttonCtrl::buttonCtrl(QWidget *parent,
                       abstractUICCtrl *octaveParent,
                       childWndDlg * parentWnd):
    abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableButton * btn;
    uicNameCounter++;
    this->setCtrlName("Button_"+QString::number(uicNameCounter));
    btn = new editableButton(parent, this);
    btn->setText(this->getCtrlName());
    btn->resize(60, 25);
    this->setAssociatedWidget(btn);

    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());

    this->properties.registerProp(new textBtnPropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new btnBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new btnFgClrPropEditor(this));
    this->properties.registerProp(new iconBtnPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));


    this->srcCallBack.append(QObject::tr("% This code will be executed when user click the button control."));
    this->srcCallBack.append(QObject::tr("% As default, all events are deactivated, to activate must set the"));
    this->srcCallBack.append(QObject::tr("% property 'generateCallback' from the properties editor"));
}

void buttonCtrl::setParents(QWidget * parent,
                            abstractUICCtrl * octaveParent,
                            childWndDlg * parentWnd)
{
    editableButton * btn = dynamic_cast<editableButton *>(this->associatedWidget());
    btn->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList buttonCtrl::generateMFile(QString path __attribute__((unused)))
{
    QStringList ret;
    abstractPropEditor * prop;
    QString lineMCode;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','pushbutton'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    return ret;
}

QStringList buttonCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * buttonCtrl::clone(QWidget *parent,
                                    abstractUICCtrl *octaveParent,
                                    childWndDlg * parentWnd){
    buttonCtrl * ret = new buttonCtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    ret->m_iconFile = this->m_iconFile;
    return ret;
}

void buttonCtrl::setHidden(bool isHidden){
    ((editableButton *)this->associatedWidget())->setHidden(isHidden);
}

buttonCtrl::~buttonCtrl()
{
  delete this->getAssociatedWidget();
}



buttonCtrlGen::buttonCtrlGen():controlGenerator("buttonCtrl")
{
}

abstractUICCtrl * buttonCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    buttonCtrl * btn = new buttonCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = btn->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = btn->properties.getNext();
    }    
    src = xml.readElementText();
    if(src.length() > 0)
      btn->setSrcCallBack(src.split('\n'));

    btn->deselect();

    return btn;
}
