/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "newprojectwnd.h"
#include "ui_newprojectwnd.h"
#include <QStandardPaths>
#include <QFileDialog>
#include <mainwnd.h>

newProjectWnd::newProjectWnd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newProjectWnd)
{
    ui->setupUi(this);
    ui->leProjectPath->setText(mainWnd::getAppConfig()->getWorkingDir());
    ui->labDirExists->setVisible(false);
}

newProjectWnd::~newProjectWnd()
{
    delete ui;
}

void newProjectWnd::on_btnSelFolder_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Project folder"),ui->leProjectPath->text());
    if(!path.isNull())
        ui->leProjectPath->setText(path);
}


void newProjectWnd::on_leProjectName_textChanged(const QString &arg1 __attribute__((unused)))
{
    ui->leProjectName->setText(ui->leProjectName->text().toLower());
    if(ui->leProjectName->text().length() != 0)
    {
      QDir dir(ui->leProjectPath->text() + "/" + ui->leProjectName->text());
      if(!dir.exists())
      {
          ui->labDirExists->setVisible(false);
          ui->btnCreate->setEnabled(true);
      }
      else{
          ui->labDirExists->setVisible(true);
          ui->btnCreate->setEnabled(false);
      }

    }
    else
    {
        ui->btnCreate->setEnabled(false);
    }

}




