/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "checkboxctrl.h"
#include "editablecheckbox.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"


cbCheckPropEditor::cbCheckPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "Value";
}

QString cbCheckPropEditor::getValue()
{
    editableCheckBox * cb;
    QString ret = "deselect";
    cb = (editableCheckBox * )this->ctrl->associatedWidget();
    if(cb->isChecked())
        ret = "select";
    return ret;
}

bool cbCheckPropEditor::isValidValue(QString newVal)
{
    return (newVal == "select")||(newVal == "deselect");
}

void cbCheckPropEditor::setValue(QString newVal)
{
    editableCheckBox * cb;
    cb = (editableCheckBox * )this->ctrl->associatedWidget();
    if(newVal == "select")
        cb->setChecked(true);
    else
        cb->setChecked(false);
}

QString cbCheckPropEditor::generateCode()
{
    QString ret = "'Min', 0, 'Max', 1, ";
    if(this->getValue() == "select")
        ret += "'" + this->propName + "', 1";
    else
        ret += "'" + this->propName + "', 0";
    return ret;
}

QWidget * cbCheckPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("select");
    cb->addItem("deselect");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbCheckedChanged(QString)));
    return cb;
}

void cbCheckPropEditor::cbCheckedChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbCheckedChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(cbCheckedChanged(QString)));
    }
}


cbBgClrPropEditor::cbBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString cbBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Button).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Button).blueF(), 'f', 3) + "]";
    return ret;
}

bool cbBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void cbBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Button, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString cbBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * cbBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void cbBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

cbFgClrPropEditor::cbFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString cbFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::WindowText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).blueF(), 'f', 3) + "]";
    return ret;
}

bool cbFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void cbFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::WindowText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString cbFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * cbFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void cbFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int checkBoxCtrl::uicNameCounter = 0;

unsigned int checkBoxCtrl::getNameCounter()
{
    return uicNameCounter;
}

checkBoxCtrl::checkBoxCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg *parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableCheckBox * cb = new editableCheckBox(parent, this);
    QFontMetrics fm(cb->font());
    uicNameCounter++;
    this->setCtrlName("CheckBox_"+QString::number(uicNameCounter));
    cb->setText(this->getCtrlName());
    //cb->resize(fm.width(this->getCtrlName()) + 24, fm.height());

    this->setAssociatedWidget(cb);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new cbTextPropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new cbBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));    
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new cbCheckPropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new cbFgClrPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));
}

void checkBoxCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableCheckBox * cb = dynamic_cast<editableCheckBox *>(this->associatedWidget());
    cb->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList checkBoxCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','checkbox'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList checkBoxCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

bool checkBoxCtrl::isAutoSize()
{
    return true;
}

abstractUICCtrl * checkBoxCtrl::clone(QWidget *parent,
                                      abstractUICCtrl *octaveParent,
                                      childWndDlg * parentWnd){
    checkBoxCtrl * ret = new checkBoxCtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void checkBoxCtrl::setHidden(bool isHidden){
    ((editableCheckBox *)this->associatedWidget())->setHidden(isHidden);
}

checkBoxCtrl::~checkBoxCtrl()
{
  delete this->getAssociatedWidget();
}

cbCtrlGen::cbCtrlGen():controlGenerator("checkBoxCtrl")
{
}

abstractUICCtrl * cbCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    checkBoxCtrl * cb = new checkBoxCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = cb->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = cb->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      cb->setSrcCallBack(src.split('\n'));

    cb->deselect();
    return cb;

}


