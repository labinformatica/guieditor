/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "ardunoprgwnd.h"
#include "ui_ardunoprgwnd.h"
 #include <QSerialPortInfo>
#include <QMessageBox>
#include <QDebug>

void ardUnoPrgWnd::procUpdateAct(int doIt, int count)
{
  ui->progBar->setVisible(count != doIt);
  ui->progBar->setMaximum(count);
  ui->progBar->setValue(doIt);
  if(doIt == count)
      QMessageBox::information(this, tr("Successful programming"), tr("The programming process has been successfully completed"));
}

void ardUnoPrgWnd::procUpdateError(QString msg)
{
  ui->progBar->setVisible(false);
  QMessageBox::critical(this, tr("Programming error"), tr("Something failed during the programming process. The action associated with the fault has returned the value:") + msg);
}

void ardUnoPrgWnd::updateStateCmd(QString str)
{
    qDebug() << str;
}

ardUnoPrgWnd::ardUnoPrgWnd(editableANano *edNano, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ardUnoPrgWnd)
{
    ui->setupUi(this);
    this->edNano = edNano;
    this->on_btnRefresh_clicked();
    this->ui->progBar->setVisible(false);
    ardProgrammer = new comManager(this);
    connect(ardProgrammer, SIGNAL(updateStateAct(int,int)), this, SLOT(procUpdateAct(int,int)));
    connect(ardProgrammer, SIGNAL(programError(QString)), this, SLOT(procUpdateError(QString)));
    connect(ardProgrammer, SIGNAL(updateStateCmd(QString)), this, SLOT(updateStateCmd(QString)));
}

ardUnoPrgWnd::~ardUnoPrgWnd()
{
    if(ardProgrammer->isConnected())
        ardProgrammer->closeDevice();
    disconnect(ardProgrammer, SIGNAL(updateStateAct(int,int)), this, SLOT(procUpdateAct(int,int)));
    disconnect(ardProgrammer, SIGNAL(programError(QString)), this, SLOT(procUpdateError(QString)));
    disconnect(ardProgrammer, SIGNAL(updateStateCmd(QString)), this, SLOT(updateStateCmd(QString)));
    delete ardProgrammer;
    delete ui;
}

void ardUnoPrgWnd::on_btnClose_clicked()
{
    close();
}

void ardUnoPrgWnd::on_btnProgram_clicked()
{
    QSerialPort::BaudRate baud = QSerialPort::Baud115200;
    if(ui->cmbBps->currentIndex() == 1)
        baud = QSerialPort::Baud57600;
    if(ardProgrammer->openDevice(ui->cmbSerialPort->currentText(), baud)){
        //                    :/ctrlAduino/octaveCtrls/arduino_cfw.hex
        ardProgrammer->start(":/ctrlAduino/octaveCtrls/arduino_cfw.hex");
    }
    else{
        QMessageBox::warning(this, tr("Serial port error"), tr("The selected serial port") + " " + ui->cmbSerialPort->currentText() + " " + tr("could not open."));
    }
}

void ardUnoPrgWnd::on_btnRefresh_clicked()
{
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    ui->cmbSerialPort->clear();            
    foreach(QSerialPortInfo info, ports)
    {
        if(info.portName() != "")
          ui->cmbSerialPort->addItem(info.portName());
    }
    ui->cmbSerialPort->setCurrentIndex(0);
}
