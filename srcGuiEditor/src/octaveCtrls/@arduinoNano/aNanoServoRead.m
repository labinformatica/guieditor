% aNanoServoRead(obj, nroServo)
% obj: the arduino controll used for communication
% nroServo: servo identification. Valid values are in the range [0,3].
% Return: position of indicated servo
% El paquete de datos, para pines digitales, se construye como:
% 0xB0, CMD = 8, nroServo 0, 0, 0, 0, suma, 0x0b
% Return 1 byte with the servo position

function ret = aNanoServoRead(obj, nroServo)
  v = [176, 8, nroServo, 0, 0, 0, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
  ret =  srl_read(port, 1);
endfunction
