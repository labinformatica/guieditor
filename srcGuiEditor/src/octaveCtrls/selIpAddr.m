## -*- texinfo -*-
## @deftypefn  {} {@var{wnd} =} selIpAddr ()
##
## Create and show the dialog, return a struct as representation of dialog.
##
## @end deftypefn
function wnd = selIpAddr()
  selIpAddr_def;
  wnd = show_selIpAddr();
end
