## -*- texinfo -*-
## @deftypefn {} {} nodeMCU (@var{parameterList})
## Create a object for represent a nodeMCU board
##
## @example
## retObj = nodeMCU()
## @end example
##
## @noindent
## @end deftypefn

function retObj = nodeMCU(varargin)  
  %% Si esta instalado el paquete de instrumentacion lo cargamos.
  %% en caso contrario mostramos un mensaje indicando que debe instalarse
  %% el paquete.
  havePkg = pkg('list', 'instrument-control');
  if(isempty(havePkg)) 
    disp "This package require a instrument-control package for run. ";
    disp "Please install instrument-control package from the forge repo to continue.";
    return;
  else 
    pkg('load', 'instrument-control')
  endif

  if (length (varargin) < 2 || rem (length (varargin), 2) != 0)
    error ("set: expecting property/value pairs");
  endif

  mustSelIp = false;
  ipVal = "";
  for i = 1:(length (varargin)-1)
    if ((strcmp(varargin{i}, 'showIpDlg') == 1) && (strcmp(varargin{i + 1}, 'true') == 1))
        mustSelIp = true;
    endif
    if (strcmp(varargin{i}, 'ip') == 1)
        ipVal = varargin{i + 1};
    endif
  endfor    
  retObj.ip = "";
  
  if mustSelIp
    parentWnd = gcf();
    [ip1, ipVal] = strtok (ipVal, ".");
    [ip2, ipVal] = strtok (ipVal, ".");
    [ip3, ipVal] = strtok (ipVal, ".");
    [ip4, ipVal] = strtok (ipVal, ".");
    wnd = selIpAddr();
    set(wnd.edIp1, 'String', ip1);
    set(wnd.edIp2, 'String', ip2);
    set(wnd.edIp3, 'String', ip3);
    set(wnd.edIp4, 'String', ip4);
    setappdata(wnd.figure, "parentWnd", parentWnd);
    waitfor(wnd.figure);
    if isappdata(parentWnd, "ipAddr")
      retObj.ip = getappdata(parentWnd, "ipAddr");
    else
      error ("Ip selection canceled");
    endif
  endif

  retObj = class(retObj, "nodeMCU");    
  waitHandle = waitbar (0, "Espere por favor.\n Inicializando placa...");
  cntWait = 0;
  totalWait = length (varargin);
  
  while (length (varargin) > 1)
    prop = varargin{1};
    val = varargin{2};
    varargin(1:2) = [];
    if (strcmp(prop, "ip"))      
      if ! mustSelIp
        retObj.ip = val;      
      endif
      retObj.socket = tcp(retObj.ip, 1221);
    endif
    %% Primero establecemos el tipo de pin
    for ind=0:12    
      strTest = ["typePinDigital" num2str(ind)];
      if(strcmp(strTest, prop))
         if ((ind == 1) || (ind == 2))      
           disp(["Se ha omitido la conf del pint " num2str(ind) " debido a que se utiliza con otra funcion"]);
         else
           nodeSetPinType(retObj, ind, "digital", val);
         endif      
      endif
    endfor

     for ind=1:12    
      strTest = ["typePinPWM" num2str(ind)];
      if(strcmp(strTest, prop))
         if ((ind == 1) || (ind == 2))      
           disp(["Se ha omitido la conf del pint " num2str(ind) " debido a que se utiliza con otra funcion"]);
         else
           nodeSetPinType(retObj, ind, "pwm", varargin{1}, varargin{2}, varargin{3}, varargin{4});
         endif      
      endif
    endfor
    if(strcmp("typePinADC", prop))
       nodeSetPinType(retObj, 0, "adc", "source", val);
    endif

    waitbar (cntWait/totalWait, waitHandle);
    cntWait = cntWait + 2;
  endwhile
  delete (waitHandle);
endfunction
