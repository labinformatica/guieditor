function ret = nodeAdcReadVcc(obj)
  v = [176, 10, 0, 0, 0, 0, 0, 0, 11];
  if (strcmp(class(obj), "nodeMCU") != 1)  
    error("Esta funcion ha sido definida para utilizar objetos de la clase nodeMCU");
  endif
 
 for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  for i=1:9
    tcp_write(obj.socket, uint8(v(i)));
  endfor
  v1 = tcp_read(obj.socket, 1); 
  v2 = tcp_read(obj.socket, 1);
  disp(v1);
  disp(v2);
  ret = (uint16(v2)*256) + uint16(v1);  
endfunction
