function setLed(obj, led, val)

  ledState = 0;
  if (strcmp(val,"On"))
    ledState = 1;
  endif
 
  v = [176, 2, 1, led, ledState, 0, 11];
  crcPos =  length(v) - 1;
  v(crcPos) = 0;
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      

  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
  
endfunction
