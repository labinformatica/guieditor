function ret = startFilter(obj)
  v = [176, 0, 8, 0, 11];
  
  crcPos =  length(v)-1;
  v(crcPos) = 0;
  
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
  ret = "";
  letra = char(srl_read(port, 1));  
  while ((letra != char(10)))  
    ret = [ret letra];
    letra = char(srl_read(port, 1));
  endwhile

endfunction
