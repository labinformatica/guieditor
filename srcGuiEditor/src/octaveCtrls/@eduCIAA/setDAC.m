function ret = setDAC(obj, val)
  v = [176, 2, 3, 0, 0, 0, 11];
  if (val < 0)
    val = 0;
  endif
  if ( val > 1023)
    val = 1023;
  endif
  v(4) = mod(val, 256);
  val = idivide(val, 256);
  v(5) = mod(val, 256);
  crcPos =  length(v)-1;
  v(crcPos) = 0;
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
endfunction
