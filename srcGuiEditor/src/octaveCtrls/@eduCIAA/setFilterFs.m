function ret = setFilterFs(obj, fs)
  v = [176, 4, 7, 0, 0, 0, 0, 0, 11];
  
  dataBits = bitunpack(uint32(fs));
  bitInit = 1;
  bitEnd = 8;
  for i=1:4 
    bytePart = uint8(bitpack(dataBits(bitInit:bitEnd), "uint8"));
    v(3 + i) = bytePart;
    bitInit = bitInit + 8;
    bitEnd = bitEnd + 8;
  endfor
  
  crcPos =  length(v)-1;
  v(crcPos) = 0;
  
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
  ret = "";
  letra = char(srl_read(port, 1));  
  while ((letra != char(10)))  
    ret = [ret letra];
    letra = char(srl_read(port, 1));
  endwhile  
endfunction
