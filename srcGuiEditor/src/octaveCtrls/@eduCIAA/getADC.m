function ret = getADC(obj, ch)
  v = [176, 1, 4, ch, 0, 11];
  crcPos =  length(v)-1;
  v(crcPos) = 0;
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
    port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor  

  b01 = uint16(srl_read(port, 1));  
  b02 = uint16(srl_read(port, 1));  
  ret = uint16(0);
  ret = bitor(b01, bitshift(b02, 8));  

endfunction
