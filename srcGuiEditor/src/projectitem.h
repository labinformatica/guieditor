/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef PROJECTITEM_H
#define PROJECTITEM_H
#include <QPixmap>
#include "prjtreewidget.h"

class guiProject;

class projectItem
{
protected:
    QString vGroupName;
    QString vFileName;
    bool vHaveFileName;
    QStringList varDef;
    guiProject * prj;
public:
    projectItem(QString groupName, guiProject * prj);
    guiProject * getGUIPrj(void){return this->prj;}
    virtual void activate(int line = -1, int col = -1) = 0;
    virtual void save() = 0;
    virtual void saveAs() = 0;
    virtual void load(QString) = 0;
    virtual void exportAsScript(QString) = 0;
    virtual void closingProject() = 0;
    virtual bool isModified() = 0;
    virtual QString getName() = 0;    
    virtual abstractPrjTreeItem * treeItemWidget(abstractPrjTreeItem *) = 0;
    void setVariableDefinitions(QStringList vars);
    QStringList getVarDefinitions();
    QString groupName();
    virtual ~projectItem();
};

#endif // PROJECTITEM_H
