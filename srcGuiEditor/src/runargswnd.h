#ifndef RUNARGSWND_H
#define RUNARGSWND_H

#include <QDialog>

namespace Ui {
class runArgsWnd;
}

class runArgsWnd : public QDialog
{
    Q_OBJECT

public:
    explicit runArgsWnd(QWidget *parent = nullptr);
    QString getArguments(void);
    void setArguments(QString v);
    ~runArgsWnd();

private:
    Ui::runArgsWnd *ui;
};

#endif // RUNARGSWND_H
