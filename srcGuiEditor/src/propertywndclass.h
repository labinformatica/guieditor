/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef PROPERTYWNDCLASS_H
#define PROPERTYWNDCLASS_H

#include <QDialog>

namespace Ui {
class propertyWndClass;
}

class propertyWndClass : public QDialog
{
    Q_OBJECT

public:
    explicit propertyWndClass(QWidget *parent = 0);
    ~propertyWndClass();
    Ui::propertyWndClass *ui;
    QFont ctrls;
    QFont editor;
private slots:
    void on_btnChangeOctavePath_clicked();
    void on_btnChangeTmpPath_clicked();
    void on_btnAddLib_clicked();
    void on_btnRemLib_clicked();
    void on_btnQtPlugginPath_clicked();
    void on_btnDeletePlugPath_clicked();
    void on_btnGZipPath_clicked();
    void on_btnTarPath_clicked();
    void on_btnChangeFontCtrl_clicked();
    void on_btnChangeFontEditor_clicked();
    void on_btnChgWorkingDir_clicked();
};

#endif // PROPERTYWNDCLASS_H
