/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgargeditor.h"
#include "ui_wdgargeditor.h"
#include "wnditemedit.h"
#include "ui_wnditemedit.h"
#include <QRegExpValidator>
#include <QRegExp>

wdgArgEditor::wdgArgEditor(abstractPropEditor * prop) :
    QWidget(0),updPropWdg(prop),
    ui(new Ui::wdgArgEditor)
{
    ui->setupUi(this);
}

wdgArgEditor::~wdgArgEditor()
{
    delete ui;
}

void wdgArgEditor::setItems(QString v)
{
    ui->leItems->setText(v);
}

QString wdgArgEditor::getItems()
{
    return ui->leItems->text();
}

QLineEdit *wdgArgEditor::getLEItems()
{
    return ui->leItems;
}

void wdgArgEditor::updPropValue()
{
    ui->leItems->setText(this->getPropEditor()->getValue());
}


void wdgArgEditor::on_btnEdit_clicked()
{
    int i;
    QString strItems;
    QStringList items = ui->leItems->text().split("|", QString::SkipEmptyParts);
    wndItemEdit wnd(this);
    QRegExp reg ("^[a-zA-Z_][a-zA-Z0-9_]*$");
    wnd.ui->leNewVal->setValidator(new QRegExpValidator(reg, wnd.ui->leNewVal));
    wnd.setWindowTitle(tr("Callback argument editor"));
    wnd.ui->listItem->clear();
    for(i=0; i < items.count(); i++)
            wnd.ui->listItem->addItem(items[i]);
    if(wnd.exec())
    {        
        for(i=0;i<wnd.ui->listItem->count();i++)
        {
            if(i < wnd.ui->listItem->count() - 1)
              strItems += wnd.ui->listItem->item(i)->text() + "|";
            else
              strItems += wnd.ui->listItem->item(i)->text();
        }
        ui->leItems->setText(strItems);
        this->getPropEditor()->setValue(strItems);
    }
}
