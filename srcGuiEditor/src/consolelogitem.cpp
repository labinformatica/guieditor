#include "consolelogitem.h"

ConsoleLogItem::ConsoleLogItem(guiProject * prj, QListWidget *parent):QListWidgetItem(parent, QListWidgetItem::UserType){
  this->prj = prj;
}

ConsoleLogItem::ConsoleLogItem(guiProject * prj, const QString &text, QListWidget *parent):QListWidgetItem(text, parent, QListWidgetItem::UserType){
  this->prj = prj;
}

ConsoleLogItem::ConsoleLogItem(guiProject * prj, const QIcon &icon, const QString &text, QListWidget *parent):QListWidgetItem(icon, text, parent, QListWidgetItem::UserType){
  this->prj = prj;
}
