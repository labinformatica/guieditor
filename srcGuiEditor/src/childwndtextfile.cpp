/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "childwndtextfile.h"
#include <QFileDialog>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QMessageBox>
#include <QTextStream>

treeItemTextFile::treeItemTextFile(guiProject * p, childWndTextFile *dlg,  QTreeWidgetItem * parent)
    :abstractPrjTreeItem(parent)
{
    this->setText(0, dlg->windowTitle());
    this->setToolTip(0, dlg->documentName);
    this->setIcon(0, QPixmap(":/img/txtFile"));
    this->dlg = dlg;
    this->associatedPrj = p;
}

void treeItemTextFile::activate()
{
    dlg->show();
    dlg->widget()->show();
}

void treeItemTextFile::populateMenu(QMenu &m)
{
    m.addAction(tr("Delete") + " " + dlg->windowTitle(), this, SLOT(actDelete(bool)));
}

void treeItemTextFile::selectProject()
{
    mainWnd::getPrjWnd()->setActiveProject(this->associatedPrj);
}

void treeItemTextFile::clicked()
{
    this->activate();
}

void treeItemTextFile::actDelete(bool checked __attribute__((unused)))
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(mainWnd::getMainWnd(),
                                  QObject::tr("Warning!!! This operation cannot be undone"),
                                  QObject::tr("You are trying to delete the file %1, this operation can't be undone. Are you sure to delete this file?").arg(this->dlg->documentName),
                                  QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        QFile toDelete(this->dlg->documentName);
        toDelete.remove();
        QFileInfo fi(this->dlg->documentName);

        QFile pkgCpy(this->associatedPrj->pkgPath() + QDir::separator() + this->associatedPrj->name() + QDir::separator() + fi.fileName());
        if(pkgCpy.exists())
            pkgCpy.remove();

        this->dlg->close();
        this->associatedPrj->items.removeAll(this->dlg);
        mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this->dlg);
        this->dlg->deleteLater();
        mainWnd::getPrjWnd()->refreshList();
    }
}


childWndTextFile::childWndTextFile(QWidget *parent, Qt::WindowFlags flags, guiProject *prj)
    :abstractChildWnd(parent, flags),projectItem("Text", prj)
{
    this->editor = new QsciScintilla;
    this->editor->setFont(mainWnd::getMainWnd()->getAppConfig()->getEditorFont());
    this->setGeometry(0, 0, 320, 240);
    this->setWidget(editor);
    this->setWindowIcon(QPixmap(":/img/img/icono.svg"));
}

void childWndTextFile::save()
{
    if(!this->documentHaveName)
    {
      QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("Text files (*.txt);;All files (*.*);;"), 0);
      if(!fileName.isNull())
      {
          this->documentHaveName = true;
          this->documentName = fileName;
          QFile f(fileName);
          f.open(QIODevice::WriteOnly | QIODevice::Text);
          QTextStream sTxt(&f);
          sTxt << this->editor->text();
          f.close();
          this->setWindowModified(false);
          QFileInfo fileInfo(fileName);
          this->setWindowTitle(fileInfo.fileName());
      }
    }
    else
    {
        QFile f(this->documentName);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream sTxt(&f);
        sTxt << this->editor->text();
        f.close();
        this->setWindowModified(false);
    }
}

void childWndTextFile::save(QString fn)
{
    QFile f(fn);
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream sTxt(&f);
    sTxt << editor->text();
    f.close();
}

void childWndTextFile::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("Text files (*.txt);;All files (*);;"), 0);
    if(!fileName.isNull())
    {
        this->documentHaveName = true;
        this->documentName = fileName;
        QFileInfo fileInfo(fileName);
        this->setWindowTitle(fileInfo.fileName());

        QFile f(fileName);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream sTxt(&f);
        sTxt << this->editor->text();
        f.close();
     }
}

void childWndTextFile::load(QString fn)
{
    QFile f(fn);
    f.open(QIODevice::ReadOnly | QIODevice::Text);    
    editor->setText(f.readAll());
    f.close();
}

void childWndTextFile::run(QString path __attribute__ ((unused)))
{
    /*
     * Text files, as default, cant run in octave
    */
}

void childWndTextFile::exportAsScript(QString)
{
    /*
     * Text files, as default, cant be exported
    */
}

void childWndTextFile::activate(int line __attribute__((unused)), int col __attribute__((unused)))
{
    show();
    this->widget()->show();
}

void childWndTextFile::closingProject()
{
}

QString childWndTextFile::getName()
{
    QFileInfo fileInfo(documentName);
    return fileInfo.fileName();
}

bool childWndTextFile::isModified()
{
    return this->editor->isModified();
}

abstractPrjTreeItem * childWndTextFile::treeItemWidget(abstractPrjTreeItem *treeItem)
{
    return new treeItemTextFile(dynamic_cast<treeItemProject*>(treeItem)->getPrj(), this, treeItem);
}
