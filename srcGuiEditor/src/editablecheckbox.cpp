/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "editablecheckbox.h"
#include <QApplication>
#include "framedlg.h"
#include <QFontMetrics>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include "painthidden.h"

editableCheckBox::editableCheckBox(QWidget *parent, abstractUICCtrl *uiCtrl) :
    QCheckBox(parent)
{    
    underMovement = false;
    this->parentUICtrl = uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
    this->isHidden = false;
}


editableCheckBox::editableCheckBox(const QString & text, QWidget * parent, abstractUICCtrl *uiCtrl):
    QCheckBox(text, parent)
{
    QFontMetrics fm(this->font());
    this->resize(fm.boundingRect(this->text()).width() + 40, fm.boundingRect(this->text()).height());
    underMovement = false;    
    this->parentUICtrl=uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
    this->isHidden = false;
}

void editableCheckBox::setParents(QWidget *parent, abstractUICCtrl *uiCtrl)
{
    setParent(parent);
    this->parentUICtrl = uiCtrl;
}

void editableCheckBox::setHidden(bool isHidden){
    this->isHidden = isHidden;
}

editableCheckBox::~editableCheckBox()
{

}

int editableCheckBox::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

void editableCheckBox::mouseReleaseEvent(QMouseEvent *)
{
    underMovement = false;
    this->parentUICtrl->updateAdjPoints();
}

void editableCheckBox::mousePressEvent(QMouseEvent * event)
{
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();

    underMovement = true;
    moveXInit = this->cordToGrid(event->x());
    moveYInit = this->cordToGrid(event->y());

    if(!wnd->isSelected(this->parentUICtrl))
      wnd->addToSelection(this->parentUICtrl);
}

void editableCheckBox::mouseMoveEvent(QMouseEvent * event)
{
    int dx;
    int dy;
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();

    if(underMovement)
    {
        dx = this->cordToGrid(event->x()) - moveXInit;
        dy = this->cordToGrid(event->y()) - moveYInit;
        wnd->moveSelection(dx, dy);
    }
}


void editableCheckBox::moveEvent ( QMoveEvent * event )
{
    QCheckBox::moveEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableCheckBox::resizeEvent ( QResizeEvent * event )
{
    QCheckBox::resizeEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableCheckBox::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (this->parent(), newEvent, 0);
    }
}

void editableCheckBox::paintEvent(QPaintEvent *e){
    QCheckBox::paintEvent(e);
    if(isHidden)
        paintHidden ph(this);
}

