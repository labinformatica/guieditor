/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CONTROLGENERATOR_H
#define CONTROLGENERATOR_H
#include <QString>
#include "abstractuicctrl.h"
#include <QXmlStreamReader>
#include "framedlg.h"

class controlGenerator
{
private:
    QString className;
public:
    controlGenerator(QString className);
    QString getClassName();
    virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg *dlg) = 0;
    virtual ~controlGenerator();
};

#endif // CONTROLGENERATOR_H
