/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgitemeditor.h"
#include "ui_wdgitemeditor.h"
#include "wnditemedit.h"
#include "ui_wnditemedit.h"

wdgItemEditor::wdgItemEditor(abstractPropEditor * prop) :
    QWidget(0),updPropWdg(prop),
    ui(new Ui::wdgItemEditor)
{
    ui->setupUi(this);
}

wdgItemEditor::~wdgItemEditor()
{
    delete ui;
}

void wdgItemEditor::setItems(QString v)
{
    ui->leItems->setText(v);
}

QString wdgItemEditor::getItems()
{
    return ui->leItems->text();
}

QLineEdit *wdgItemEditor::getLEItems()
{
    return ui->leItems;
}

void wdgItemEditor::updPropValue()
{
    ui->leItems->setText(this->getPropEditor()->getValue());
}


void wdgItemEditor::on_btnEdit_clicked()
{
    int i;
    QString strItems;
    QStringList items = ui->leItems->text().split("|");
    wndItemEdit wnd(this);
    wnd.ui->listItem->clear();
    for(i=0; i < items.count(); i++)
            wnd.ui->listItem->addItem(items[i]);
    if(wnd.exec())
    {        
        for(i=0;i<wnd.ui->listItem->count();i++)
        {
            if(i < wnd.ui->listItem->count() - 1)
              strItems += wnd.ui->listItem->item(i)->text() + "|";
            else
              strItems += wnd.ui->listItem->item(i)->text();
        }
        ui->leItems->setText(strItems);
        this->getPropEditor()->setValue(strItems);
    }
}
