/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "sccheckmem.h"
#include <QDebug>

scCheckMem::scCheckMem(prgAction *act, memPage * page, bool isLast):serialCmd(act, isLast)
{
    this->buf.clear();
    this->aPage = page;
    this->vCheckOk = false;
}

QString scCheckMem::cmdInfo()
{
    return "Checking page address: " + QString("0x%1").arg(aPage->getAddr(), 4, 16, QLatin1Char('0'));
}

QByteArray scCheckMem::dataToSend(void)
{
    QByteArray toSend;
    toSend.push_back(Cmnd_STK_READ_PAGE);
    toSend.push_back((uint8_t)((aPage->getSize()>> 8) & 0xFF));
    toSend.push_back((uint8_t)(aPage->getSize() & 0xFF));
    toSend.push_back((uint8_t)'F');
    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

rcvType scCheckMem::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    uint32_t i;
    buf.push_back(data);
    QString debugStr;
    if ((buf.length() == 130) && (buf[0] == Resp_STK_INSYNC) && (buf[129] == Resp_STK_OK))
    {
        ret = rcvOk;
        this->vCheckOk = true;
        for(i = 0; (i < this->aPage->getSize()) && this->vCheckOk; i++)
            this->vCheckOk = ((uint8_t)buf[i+1]) == aPage->getData(i);
    }
    else if((buf.length() == 1) && (buf[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

bool scCheckMem::checkOk()
{
    return this->vCheckOk;
}

scCheckMem::~scCheckMem(void)
{

}
