/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "hexfile.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

memPage::memPage(uint32_t size, uint32_t addr)
{
    uint32_t i;
    this->data = new uint8_t[size];
    for(i = 0; i < size; i++)
        this->data[i] = 0xFF;
    this->size = size;
    this->addr = addr;
}

uint32_t  memPage::getAddr()
{
    return this->addr;
}

uint8_t   memPage::getData(uint32_t off)
{
    return this->data[off];
}

uint8_t*  memPage::getData()
{
    return this->data;
}

void       memPage::setData(uint8_t val, uint32_t off)
{
    if(off < this->size)
        this->data[off] = val;
}

uint32_t  memPage::getSize()
{
    return this->size;
}

memPage::~memPage()
{
    delete []this->data;
}

hexFile::hexFile()
{
}

void hexFile::paginate(uint32_t pageSize)
{
    memPage * aPage;
    int32_t inx, segOff;
    int pCnt;
    uint32_t pageAddr;
    uint32_t address;
    uint32_t offSet;
    bool pFound = false;
    for(inx = 0; inx < pages.length(); inx ++)
    {
        aPage = pages[inx];
        delete aPage;
    }
    pages.clear();
    /* Recorro los segmentos */
    for(inx = 0; inx < data.length(); inx++)
    {
        for(segOff = 0; segOff < data[inx]->getLen(); segOff++)
        {
          address  = data[inx]->getAddr() + segOff;
          pageAddr = address & (~(pageSize - 1));
          offSet   = address &   (pageSize - 1);
          pFound = false;
          pCnt = 0;
          while((pCnt < pages.length()) && (!pFound))
             if(pages[pCnt]->getAddr() == pageAddr)
                 pFound = true;
              else
                 pCnt++;
          if(pFound)
              pages[pCnt]->setData(data[inx]->getData(segOff), offSet);
          else
          {
              aPage = new memPage(pageSize, pageAddr);
              aPage->setData(data[inx]->getData(segOff), offSet);
              pages.push_back(aPage);
          }
        }
    }
}

bool hexFile::process(QString fileName)
{        
    bool ok = true;
    QFile f;
    QString line;
    QString strLen; bool lenOk;
    QString strAddr;bool addrOk;
    QString strType;bool typeOk;
    QString strData;bool dataOk, d;
    QString strCrc; bool crcOk;
    uint8_t crc;
    hexRecord *r;

    int i;

    for(i = 0; i < data.length(); i++)
        delete data[i];
    this->data.clear();

    f.setFileName(fileName);
    f.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream hex(&f);
    while((!hex.atEnd()) && ok)
    {
        line = hex.readLine();
        if(line != "")
        {
          ok = ok && (line[0] == ':');
          if(ok)
          {
            line.remove(0, 1); // Quitamos ':'

            strLen = line.left(2);
            line.remove(0, 2); // Quitamos el tamaño

            strAddr = line.left(4);
            line.remove(0, 4); // Quitamos la dirección

            strType = line.left(2);
            line.remove(0, 2); // Quitamos el tipo

            strCrc = line.right(2);
            line.remove(line.length()-2, 2);

            strData = line;
            r = new hexRecord(strLen.toInt(&lenOk, 16));
            r->setAddr(strAddr.toInt(&addrOk, 16));
            r->setType(strType.toInt(&typeOk, 16));
            crc = strCrc.toInt(&crcOk, 16);
            dataOk = true;

            for(i = 0; (i < r->getLen() && dataOk); i++)
            {
                r->setData(i, strData.mid((2*i), 2).toInt(&d, 16));
                dataOk = dataOk && d;
            }
            if(dataOk && lenOk && addrOk && typeOk && crcOk &&
                    (r->getCrc() == crc) && (r->getLen() > 0) && (r->getType() == 0))
                data.push_back(r);
            else
                delete r;
          }
        }
    }
    f.close();
    return ok;
}

uint32_t hexFile::pageCount()
{
    return this->pages.length();
}

memPage * hexFile::getPage(uint32_t p)
{
    return pages[p];
}

QVector<hexRecord *> hexFile::hexData(void)
{
    return this->data;
}

hexFile::~hexFile()
{
    int i;
    for(i=0; i < data.length(); i++)
        delete data[i];
    for(i=0; i < pages.length(); i++)
        delete pages[i];
}
