/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "scsetdevparam.h"

scSetDevParam::scSetDevParam(prgAction *act) :
    serialCmd(act, true)
{
    buffer.clear();
}

QString scSetDevParam::cmdInfo()
{
  return "Set device parametters";
}

QByteArray scSetDevParam::dataToSend(void)
{
    QByteArray toSend;
    toSend.push_back(Cmnd_STK_SET_DEVICE);
    toSend.push_back(0x86); // ATMega328p
    toSend.push_back((char)0x00); // Device revision
    toSend.push_back((char)0x00); // Programming mode: parallel and serial
    toSend.push_back(0x01); // Parallel mode: full parallel mode
    toSend.push_back(0x01); // Polling mode: can be used polling mode
    toSend.push_back(0x01); // Self timed
    toSend.push_back(0x01); // Lock bytes (0x01 but is not used in stk500 v1)
    toSend.push_back(0x03); // Fuse byte (0x03 but is not used in stk500 v1);

    toSend.push_back(0xFF); // Flash poll val 1
    toSend.push_back(0xFF); // Flash poll val 2
    toSend.push_back(0xFF); // EEPROM poll val 1
    toSend.push_back(0xFF); // EEPROM poll val 2

    toSend.push_back((char)0x00); // High part of page size (page size = 128)
    toSend.push_back((uint8_t)128);  // Low part of page size (page size = 128)
    toSend.push_back((1024 >> 8) & 0xFF); // EEPROM size (high part)
    toSend.push_back((char)(1024 & 0xFF)); // EEPROM size (low part)

    toSend.push_back((char)((0x8000 >> 24) & 0xFF)); // Flash size 1/4 high
    toSend.push_back((char)((0x8000 >> 16) & 0xFF)); // Flash size 2/4 hig low
    toSend.push_back((char)((0x8000 >>  8) & 0xFF)); // Flash size 3/4 low high
    toSend.push_back((char)((0x8000)       & 0xFF)); // Flash size 4/4 low low

    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

rcvType scSetDevParam::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    buffer.push_back(data);
    if ((buffer.length() == 2) && (buffer[0] == Resp_STK_INSYNC) && (buffer[1] == Resp_STK_OK))
        ret = rcvOk;
    else if((buffer.length() == 1) && (buffer[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

scSetDevParam::~scSetDevParam(void)
{

}
