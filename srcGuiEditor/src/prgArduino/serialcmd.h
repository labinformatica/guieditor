/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef SERIALCMD_H
#define SERIALCMD_H

#include <QByteArray>
#include <QString>
#include <QByteArray>
#include <QObject>
#include "Devices.h"
#include "command.h"

class prgAction;

typedef enum {rcvWait,  /* Must wait more data                   */
              rcvOk,    /* All data was recive and its ok        */
              rcvFail   /* All possible data was recive and fail */
             } rcvType;

class serialCmd
{
protected:
    QString sendCmd;
    int retryCount;
    prgAction *associatedAction;
    bool lastCmd;
    uint32_t retVal;
public:    
    static int maxRetry;
    serialCmd(prgAction *act, bool isLastCmd);
    prgAction * getAction(void);
    bool isLastCmd(void);
    uint32_t getRcvData();
    virtual QString cmdInfo() = 0;
    virtual QByteArray dataToSend(void) = 0;
    virtual rcvType processResponse(QByteArray data) = 0;
    virtual bool mustSkip(void);
    void addRetry();
    int getRetry();
    virtual ~serialCmd(void);
};

#endif // SERIALCMD_H
