/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDITABLEIMG_H
#define EDITABLEIMG_H

#include <QLabel>
#include <QMouseEvent>
#include "abstractuicctrl.h"

class editableImage : public QLabel
{
    Q_OBJECT
private:
    bool underMovement;
    int moveXInit;
    int moveYInit;
    abstractUICCtrl *parentUICtrl;
    QPixmap pixImg;
    bool isHidden;
public:
    explicit editableImage(QWidget *parent, abstractUICCtrl *uiCtrl);
//    explicit editableImage(const QString & text, QWidget * parent, abstractUICCtrl *uiCtrl);
    void setParents(QWidget *parent, abstractUICCtrl *uiCtrl);
    int cordToGrid(int);
    void setHidden(bool isHidden);
    virtual ~editableImage();
protected:
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void moveEvent(QMoveEvent * event);
    virtual void resizeEvent(QResizeEvent  * event);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void paintEvent(QPaintEvent *e);
signals:    
public slots:  
};

#endif // EDITABLEIMG_H
