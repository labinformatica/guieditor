/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WDGDEBUG_H
#define WDGDEBUG_H

#include <QWidget>
#include <QProcess>
#include <QListWidgetItem>

namespace Ui {
class wdgDebug;
}

class wdgDebug : public QWidget
{
    Q_OBJECT

public:
    explicit wdgDebug(QProcess * octProc, QWidget *parent);
    ~wdgDebug();
    Ui::wdgDebug *ui;
public slots:
    void availData();
    void showStdErr();
    void showStdOut();

private slots:
    void on_btnExec_clicked();

    void on_btnCopyOutput_clicked();

    void on_btnClearConsole_clicked();

    void on_lstConsoleOut_itemDoubleClicked(QListWidgetItem *item);

private:

    QProcess * octProc;
    QString lineOutput;
    QString lineStd;
    QString lineErr;
};

#endif // WDGDEBUG_H
