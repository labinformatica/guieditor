/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDITABLEBUTTONGROUP_H
#define EDITABLEBUTTONGROUP_H

#include <QGroupBox>
#include <QMouseEvent>
#include "abstractuicctrl.h"

class editableButtonGroup: public QGroupBox
{
    Q_OBJECT
private:
    bool underMovement;
    int moveXInit;
    int moveYInit;
    abstractUICCtrl *parentUICtrl;
    bool isHidden;
public:    
    explicit  editableButtonGroup(QWidget *parent, abstractUICCtrl *uiCtrl);
    void setParents(QWidget *parent, abstractUICCtrl *uiCtrl);
    int cordToGrid(int);
    void setHiden(bool);
    virtual ~editableButtonGroup();
protected:
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void paintEvent(QPaintEvent *event);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void moveEvent(QMoveEvent * event);
    virtual void resizeEvent(QResizeEvent  * event);
};

#endif // EDITABLEBUTTONGROUP_H
