/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CONFIGSETTINGS_H
#define CONFIGSETTINGS_H
#include <QSettings>
#include "propertywndclass.h"

class configSettings
{
private:
    QString octavePath;
    QString workingDir;
    QString tempDir;
    QStringList libPaths;
    QString language;
    QString qt5PlugginPath;
    QString gzipPath;
    QString tarPath;
    QFont ctrlFont;
    QFont editorFont;
    int editorTabSize;
    QStringList recentPrj;
    bool forceCodec;
    QString outputCodec;
public:
    configSettings();
    void loadCfg();
    void saveCfg();
    bool octavePathOk();
    bool tmpDirOk();

    bool gzipPathOk();
    bool tarPathOk();

    void paramToWnd(propertyWndClass *wnd);
    void wndToParam(propertyWndClass *wnd);

    int getEditorTabSize();

    QString getQt5PlugginPath();
    QString getOctavePath();
    QString getTempDir();
    QString getLanguage();

    QString getWorkingDir(void);
    void setWorkingDir(QString wd);

    QString getGzipPath();
    QString getTarPath();

    QFont getCtrlFont();
    QFont getEditorFont();

    QStringList getLibPaths();

    void registerRecentPrj(QString prjPath);
    QString getRecentPrj(int prjNumber);
    int getRecentPrjCount(void);

    bool getForceOutputCodec(void);
    QString getOutputCodec(void);
    ~configSettings();
};

#endif // CONFIGSETTINGS_H
