/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef EDITABLEANANO_H
#define EDITABLEANANO_H

#include <QLabel>
#include <QMouseEvent>
#include "abstractuicctrl.h"

typedef enum {none, input, inputPullUp, output} aNanoPinType;

class editableANano;

class dblClickEventFilter: public QObject
{
public:
    dblClickEventFilter(QObject *parent);
    bool eventFilter(QObject* object,QEvent* event);
};

class editableANano : public QLabel
{
    Q_OBJECT
private:
    bool underMovement;
    int moveXInit;
    int moveYInit;
    abstractUICCtrl *parentUICtrl;
    QPixmap pixImg;
    aNanoPinType pinType[32];
    QString serialPortName;
    unsigned int autoReadInterval;
    bool isHidden;
public:
    void setSerialPortName(QString v)       { this->serialPortName = v;      }
    QString getSerialPortName(void)         { return this->serialPortName;   }
    void setPinType(int pinNro, aNanoPinType t)
    {
       if((pinNro >= 0) && (pinNro <32))
           pinType[pinNro] = t;
    }
    aNanoPinType getPinType(int pinNro)
    {
        aNanoPinType ret = none;
        if((pinNro >=0) && (pinNro <32))
            ret = pinType[pinNro];
        return ret;
    }

    explicit editableANano(QWidget *parent, abstractUICCtrl *uiCtrl);    
    void setParents(QWidget *parent, abstractUICCtrl *uiCtrl);
    int cordToGrid(int);
    virtual ~editableANano();

    bool getPinOutput(int pin);
    bool getPinValue(int pin);
    bool getPinAutoRead(int pin);

    void setPinOutput(int pin, bool v);
    void setPinValue(int pin, bool v);
    void setPinAutoRead(int pin, bool v);
    void setHidden(bool isHidden);
protected:
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void moveEvent(QMoveEvent * event);
    virtual void resizeEvent(QResizeEvent  * event);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void paintEvent(QPaintEvent *e);
signals:
    void doubleClick(void);
public slots:
    void onDoubleClick(void);


};

#endif // EDITABLEANANO_H
