/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CHILDWNDMFILE_H
#define CHILDWNDMFILE_H
#include "abstractchildwnd.h"
#include "guiproject.h"
#include "mfileeditorwidget.h"

class childWndMFile;

class treeItemMFile :  public abstractPrjTreeItem
{
    Q_OBJECT
private:
    childWndMFile * dlg;
    guiProject * associatedPrj;
public:
    explicit treeItemMFile(guiProject * p, childWndMFile *dlg,  QTreeWidgetItem * parent = 0);
    void activate();
    void populateMenu(QMenu &);
    void selectProject();
private slots:
    void clicked();
    void actDelete(bool checked = false);
};

class childWndMFile : public abstractChildWnd, public projectItem
{
    Q_OBJECT
protected:
    mfileEditorWidget * srcEditorWidget;
public:
    childWndMFile(QWidget *parent, Qt::WindowFlags flags, guiProject *prj);

    mfileEditorWidget * getSrcEditor();
    void save();
    void save(QString);
    void saveAs();
    void load(QString fn);
    void run(QString path);
    void exportAsScript(QString);
    void activate(int line = -1, int col = -1);
    void closingProject();
    QString getName();
    virtual bool isModified();
    abstractPrjTreeItem * treeItemWidget(abstractPrjTreeItem *);
};

#endif // CHILDWNDMFILE_H
