/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CALLBACKCTRL_H
#define CALLBACKCTRL_H
#include <QString>
#include <QStringList>
#include "abstractuicctrl.h"

#include "commonproperties.h"
#include "controlgenerator.h"

class fNamePropEditor: public abstractPropEditor
{
private:
    QString fName;
public:
    fNamePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "functionName";
        this->fName = ctrl->getCtrlName() + "_fcn";
    }
    QString getValue(){
        return fName;
    }

    bool isValidValue(QString newVal){
        QRegExp r("^[a-zA-Z_][a-zA-Z0-9_]*$");
        return (newVal.contains(r) == 1);
    }
    virtual void setValue(QString newVal){
        fName = newVal;
    }

    virtual QString generateCode()
    {
        return "@" + this->fName;
    }
};



class callBackCtrl: public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

protected:

public:
  static unsigned int getNameCounter();
  QStringList createCallBack(QString path);
  callBackCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
  void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
  QStringList generateMFile(QString path);

  virtual QString className() { return "callBackCtrl";}
  abstractUICCtrl * clone(QWidget *parent,
                          abstractUICCtrl *octaveParent,
                          childWndDlg * parentWnd);
  void setHidden(bool);
  virtual ~callBackCtrl();
  virtual bool mustDeclareCallBack(void) {return false;}
  virtual bool isAutoSize() { return true;}
};

//// Comienzan las definiciones de las propiedades específicas del combBox
///

class argListPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QStringList arg;
 public:
    argListPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueItemChanged();
};

class callBackCtrlCtrlGen: public controlGenerator
{
public:
  callBackCtrlCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // CALLBACKCTRL_H
