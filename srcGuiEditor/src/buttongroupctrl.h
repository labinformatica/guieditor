/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef BUTTONGROUPCTRL_H
#define BUTTONGROUPCTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QGroupBox>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"

enum bgBsBorderStyle{ bgBevelIn, bgBevelOut, bgEtchedIn, bgEtchedOut, bgLine, bgNone};

enum bgTpTitlePos{bgCenterBottom=0, bgCenterTop=1, bgLeftBottom=2, bgLeftTop=3, bgRightBottom=4, bgRightTop=5};
const QString bgTitlePos[] ={"centerbottom" , "centertop" , "leftbottom" , "lefttop" , "rightbottom" , "righttop"};
enum bgFwFontWeight {bgWNormal=0, bgWBold=1, bgWDemi = 2, bgWLight = 3};
const QString bgFontWeight[] = {"normal", "bold", "demi", "light"};

enum bgFaFontAngle {bgItalic=0, bgNormal=1, bgOblique=2};
const QString bgFontAngle[] = {"italic", "normal", "oblique"};

class bgStyleGenerator
{
private:
    QWidget *widget;
    int vBorderWidth;
    QColor vBackgroundColor;
    QColor vForegroundColor;
    bgBsBorderStyle vBorderStyle;
    bgTpTitlePos vTitlePos;
    QString vFontName;
    bgFwFontWeight vFontWeight;
    bgFaFontAngle vFontAngle;
    int vFontSize;
public:

    bgStyleGenerator(abstractUICCtrl *w);

    QString getStyleSheet();

    int borderWidth();
    void setBorderWidth(int w);

    QColor backgroundColor();
    void setBackgroundColor(QColor c);

    QColor foregroundColor();
    void setForegroundColor(QColor c);

    bgBsBorderStyle borderStyle();
    void setBorderStyle(bgBsBorderStyle bs);

    bgTpTitlePos titlePos();
    void setTitlePos(bgTpTitlePos p);

    QString fontName();
    void setFontName(QString f);

    bgFwFontWeight fontWeight();
    void setFontWight(bgFwFontWeight);

    bgFaFontAngle fontAngle();
    void setFontAngle(bgFaFontAngle);

    int fontSize();
    void setFontSize(int s);
};


class bgBgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class bgFgClrPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class bgTitlePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgTitlePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};

class bgTitlePosPropEditor: public abstractPropEditor
{
    Q_OBJECT
private:

public:
    bgTitlePosPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void titlePosChange(const QString &txt);
};

class bgFontNamePropEditor: public abstractPropEditor
{
    Q_OBJECT
private:
    QFontDatabase fontDB;
public:
    bgFontNamePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontChanged(const QString &txt);
};

class bgFontSizePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgFontSizePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontSizeChanged(const QString & text);
};

class bgFontWeightPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgFontWeightPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontWeightChanged(const QString & text);
};

class bgFontAnglePropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgFontAnglePropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void fontAngleChanged(const QString & text);
};

class bgBorderWidtPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    bgBorderWidtPropEditor(abstractUICCtrl *ctrl);
    QString getValue();

    bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void borderSizeChanged(const QString & text);
};



class buttonGroupCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

protected:

public:
    static unsigned int getNameCounter();
    void addWidget(abstractUICCtrl *w);
    bgStyleGenerator *styleGenerator;
    buttonGroupCtrl(QWidget * parent,
                   abstractUICCtrl *octaveParent,
                   childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
     virtual QStringList generateMFile(QString path);

    virtual QString className() { return "buttonGroup";}
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg *parentWnd);
    void setHidden(bool);
    virtual ~buttonGroupCtrl();
};

class buttonGroupCtrlGen: public controlGenerator
{
public:
  buttonGroupCtrlGen();
virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);

};

#endif // GROUPPANELCTRL_H
