/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "editctrl.h"
#include "editablelineedit.h"
#include "commonproperties.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"

editHAlignmentPropEditor::editHAlignmentPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "HorizontalAlignment";
}

QString editHAlignmentPropEditor::getValue()
{
    editableLineEdit * ed;
    QString ret;
    ed = (editableLineEdit * )this->ctrl->associatedWidget();
    if(ed->alignment() == Qt::AlignLeft)
        ret = "left";
    else if(ed->alignment() == Qt::AlignRight)
        ret = "right";
    else
        ret = "center";
    return ret;
}

bool editHAlignmentPropEditor::isValidValue(QString newVal)
{
    return (newVal == "left")||(newVal == "center")||(newVal == "right");
}

void editHAlignmentPropEditor::setValue(QString newVal)
{
    editableLineEdit * ed;
    ed = (editableLineEdit * )this->ctrl->associatedWidget();
    if(newVal == "left")
        ed->setAlignment(Qt::AlignLeft);
    else if(newVal == "right")
        ed->setAlignment(Qt::AlignRight);
    else
        ed->setAlignment(Qt::AlignHCenter);
}

QString editHAlignmentPropEditor::generateCode()
{
    return "'" + this->propName + "', '" + this->getValue() + "'";
}

QWidget * editHAlignmentPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("left");
    cb->addItem("center");
    cb->addItem("right");
    //cb->setCurrentText(this->getValue());
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(editHAlignmentChanged(QString)));
    return cb;
}

void editHAlignmentPropEditor::editHAlignmentChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(editHAlignmentChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(editHAlignmentChanged(QString)));
    }
}

editBgClrPropEditor::editBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString editBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Base).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Base).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Base).blueF(), 'f', 3) + "]";
    return ret;
}

bool editBgClrPropEditor::isValidValue(QString newVal)
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void editBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Base, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString editBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}


QWidget * editBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void editBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}


editFgClrPropEditor::editFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString editFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Text).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Text).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Text).blueF(), 'f', 3) + "]";
    return ret;
}

bool editFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void editFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::Text, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString editFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * editFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void editFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int editCtrl::uicNameCounter = 0;

unsigned int editCtrl::getNameCounter()
{
    return uicNameCounter;
}

editCtrl::editCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    uicNameCounter++;
    editableLineEdit* edit = new editableLineEdit(parent, this);
    this->setAssociatedWidget(edit);
    edit->setAlignment(Qt::AlignLeft);
    this->setCtrlName("Edit_"+QString::number(uicNameCounter));
    edit->setText(this->getCtrlName());
    edit->resize(60, 25);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new textEditPropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new editBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new editHAlignmentPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new editFgClrPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));

    this->srcCallBack.append(QObject::tr("% This code will be executed when the control lost focus and text has "));
    this->srcCallBack.append(QObject::tr("% been changed or when press the \"enter\" key."));
    this->srcCallBack.append(QObject::tr("% As default, all events are deactivated, to activate must set the"));
    this->srcCallBack.append(QObject::tr("% property 'have callback' from the properties editor"));
}

void editCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableLineEdit * ed = dynamic_cast<editableLineEdit *>(this->associatedWidget());
    ed->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList editCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','edit'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList editCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * editCtrl::clone(QWidget *parent,
                                  abstractUICCtrl *octaveParent,
                                  childWndDlg * parentWnd){
    editCtrl * ret = new editCtrl(parent,
                                  octaveParent,
                                  parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void editCtrl::setHidden(bool isHidden){
    ((editableLineEdit*)this->associatedWidget())->setHidden(isHidden);
}

editCtrl::~editCtrl()
{
    delete this->getAssociatedWidget();
}

editCtrlGen::editCtrlGen():controlGenerator("editCtrl")
{
}

abstractUICCtrl * editCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    editCtrl * edit = new editCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = edit->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = edit->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      edit->setSrcCallBack(src.split('\n'));
    edit->deselect();
    return edit;
}


