/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "framectrl.h"
#include "editableframe.h"
#include "storagemanager.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"


frameCtrlBgClrPropEditor::frameCtrlBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString frameCtrlBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Window).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).blueF(), 'f', 3) + "]";
    return ret;
}

bool frameCtrlBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void frameCtrlBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Window, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString frameCtrlBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * frameCtrlBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void frameCtrlBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

unsigned int frameCtrl::uicNameCounter = 0;

unsigned int frameCtrl::getNameCounter()
{
    return uicNameCounter;
}

frameCtrl::frameCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd):abstractUICCtrl(parent, octaveParent, parentWnd)
{
    editableFrame * frame = new editableFrame(parent, this);
    uicNameCounter++;
    this->setCtrlName("Frame_"+QString::number(uicNameCounter));

    this->setAssociatedWidget(frame);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new frameCtrlBgClrPropEditor(this));
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new callBackPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));
}

void frameCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableFrame * eFrm = dynamic_cast<editableFrame *>(this->associatedWidget());
    eFrm->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList frameCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uicontrol( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Style','frame'");
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList frameCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * frameCtrl::clone(QWidget *parent,
                                   abstractUICCtrl *octaveParent,
                                   childWndDlg * parentWnd){
    frameCtrl * ret = new frameCtrl(parent,
                                      octaveParent,
                                      parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}

void frameCtrl::setHidden(bool isHidden){
    ((editableFrame *)this->associatedWidget())->setHidden(isHidden);
}

frameCtrl::~frameCtrl()
{
  delete this->getAssociatedWidget();
}

frameCtrlGen::frameCtrlGen():controlGenerator("frameCtrl")
{
}

abstractUICCtrl * frameCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    frameCtrl * frm = new frameCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = frm->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = frm->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      frm->setSrcCallBack(src.split('\n'));

    frm->deselect();
    return frm;

}


