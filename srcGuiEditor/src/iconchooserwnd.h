#ifndef ICONCHOOSERWND_H
#define ICONCHOOSERWND_H

#include <QDialog>

namespace Ui {
class iconChooserWnd;
}

class iconChooserWnd : public QDialog
{
    Q_OBJECT

public:
    explicit iconChooserWnd(QWidget *parent,
                            QStringList imgList,
                            QString imgPath,
                            QString actualIcon);
    ~iconChooserWnd();
    QString getSelectedIcon(void);
private slots:
    void on_lstFileNames_itemSelectionChanged();

private:
    Ui::iconChooserWnd *ui;
    QString imgPath;
    QStringList imgList;
    QString selIcon;
};

#endif // ICONCHOOSERWND_H
