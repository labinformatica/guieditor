/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "childwndmfile.h"
#include "mfileeditorwidget.h"
#include "ui_mfileeditorwidget.h"
#include <QMenu>
#include <QMenuBar>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QFileDialog>

#include "mainwnd.h"
#include "ui_mainwnd.h"

#include <QMessageBox>

treeItemMFile::treeItemMFile(guiProject * p, childWndMFile * dlg, QTreeWidgetItem *parent):
    abstractPrjTreeItem(parent)
{
    this->setText(0, dlg->windowTitle());
    this->setToolTip(0, dlg->documentName);
    this->setIcon(0, QPixmap(":/img/mFileItem"));
    this->dlg = dlg;
    this->associatedPrj = p;
}

void treeItemMFile::activate()
{
    dlg->show();
    dlg->widget()->show();
}

void treeItemMFile::clicked()
{
    this->activate();
}

void treeItemMFile::populateMenu(QMenu &m)
{
    m.addAction(tr("Delete") + " " + dlg->windowTitle(), this, SLOT(actDelete(bool)));
}

void treeItemMFile::actDelete(bool checked __attribute__((unused)))
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(mainWnd::getMainWnd(),
                                  QObject::tr("Warning!!! This operation cannot be undone"),
                                  QObject::tr("You are trying to delete the file %1, this operation can't be undone. Are you sure to delete this file?").arg(this->dlg->documentName),
                                  QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        QFile toDelete(this->dlg->documentName);
        toDelete.remove();
        QFileInfo fi(this->dlg->documentName);

        QFile runCopy(this->associatedPrj->runPath() + QDir::separator() + "fcn" + QDir::separator() + fi.fileName());
        if(runCopy.exists())
            runCopy.remove();

        QFile exportCopy(this->associatedPrj->exportPath() + QDir::separator() + "fcn" + QDir::separator() + fi.fileName());
        if(exportCopy.exists())
            exportCopy.remove();

        this->dlg->close();
        this->associatedPrj->items.removeAll(this->dlg);
        mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this->dlg);
        this->dlg->deleteLater();
        mainWnd::getPrjWnd()->refreshList();
    }
}

void treeItemMFile::selectProject()
{
    mainWnd::getPrjWnd()->setActiveProject(this->associatedPrj);
}


childWndMFile::childWndMFile(QWidget *parent, Qt::WindowFlags flags, guiProject * prj)
    :abstractChildWnd(parent, flags),projectItem("MFile", prj)
{
    srcEditorWidget = new mfileEditorWidget(this);

    this->setWidget(srcEditorWidget);
    this->setGeometry(1, 1, 480, 320);
    this->setWindowIcon(QPixmap(":/img/img/icono.svg"));
}

mfileEditorWidget * childWndMFile::getSrcEditor()
{
    return this->srcEditorWidget;
}

void childWndMFile::save()
{    
    if(!this->documentHaveName)
    {
      QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("Octave script (*.m);;All files (*.*);;"), 0);
      if(!fileName.isNull())
      {
          this->documentHaveName = true;
          this->documentName = fileName;
          QFile f(fileName);
          QTextStream sTxt(&f);
          f.open(QIODevice::WriteOnly | QIODevice::Text);          
          sTxt << this->srcEditorWidget->ui->srcEditor->text();
          f.close();
          this->setWindowModified(false);
          QFileInfo fileInfo(fileName);
          this->setWindowTitle(fileInfo.fileName());
      }
    }
    else
    {
        QFile f(this->documentName);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream sTxt(&f);
        sTxt << this->srcEditorWidget->ui->srcEditor->text();
        f.close();
        this->setWindowModified(false);
    }
}

void childWndMFile::save(QString fileName)
{
    QFile f(fileName);
    this->documentHaveName = true;
    this->documentName = fileName;
    this->setWindowModified(false);
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream sTxt(&f);

    sTxt << this->srcEditorWidget->ui->srcEditor->text();
    f.close();
    QFileInfo fileInfo(fileName);
    this->setWindowTitle(fileInfo.fileName());
}

void childWndMFile::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Select destination file name"), QApplication::applicationDirPath(), tr("Octave script (*.m);;All files (*);;"), 0);
    if(!fileName.isNull())
    {
        this->documentHaveName = true;
        this->documentName = fileName;
        QFile f(fileName);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream sTxt(&f);
        sTxt << this->srcEditorWidget->ui->srcEditor->text();
        f.close();
        this->setWindowModified(false);
        QFileInfo fileInfo(fileName);
        this->setWindowTitle(fileInfo.fileName());
     }
}

void childWndMFile::load(QString fn)
{
    QFile f(fn);
    f.open(QIODevice::ReadOnly | QIODevice::Text);
    this->srcEditorWidget->ui->srcEditor->clear();
    this->srcEditorWidget->ui->srcEditor->setText(f.readAll());
    f.close();
}

void childWndMFile::run(QString path __attribute__((unused)))
{

}

void childWndMFile::exportAsScript(QString aPath)
{
    QDir fcnPath(aPath);
    if(!fcnPath.exists("fcn"))
        fcnPath.mkdir("fcn");
    QFile f(aPath + "/fcn/" + getName());
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream sTxt(&f);
    sTxt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
    sTxt << this->srcEditorWidget->ui->srcEditor->text();
    f.close();
}

void childWndMFile::activate(int line, int col)
{
    this->show();
    if(this->widget()){
        this->widget()->show();
        if((line != -1) && (col != -1)){
            this->srcEditorWidget->ui->srcEditor->setCursorPosition(line -1, col - 1);
            this->srcEditorWidget->ui->srcEditor->setFocus();
        }
    }
}

void childWndMFile::closingProject()
{
    this->close();
    mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this);
}

QString childWndMFile::getName()
{
    QFileInfo fileInfo(documentName);
    return fileInfo.fileName();
}

bool childWndMFile::isModified()
{
    return  isWindowModified() | srcEditorWidget->ui->srcEditor->isModified();
}


abstractPrjTreeItem * childWndMFile::treeItemWidget(abstractPrjTreeItem *treeItem)
{
    return new treeItemMFile(dynamic_cast<treeItemProject*>(treeItem)->getPrj(), this, treeItem);
}
