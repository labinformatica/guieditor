/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CHILDWNDDLG_H
#define CHILDWNDDLG_H

#include <QMdiSubWindow>
#include <QCloseEvent>

#include "abstractuicctrl.h"

#define defaultUICWidth 90
#define defaultUICHeight 22

#include "projectitem.h"
#include "prjtreewidget.h"
#include "abstractchildwnd.h"
#include "guiproject.h"
#include <QToolBar>
#include "wdgprop.h"
#include "wdgsrceditor.h"

class childWndDlg;
class guiProject;
class wdgSrcEditor;

class treeItemDialog :  public abstractPrjTreeItem
{
    Q_OBJECT
private:
    childWndDlg * dlg;
    guiProject * associatedPrj;
public:
    explicit treeItemDialog(guiProject * p, childWndDlg * dlg, QTreeWidgetItem * parent = 0);
    void activate();
    void populateMenu(QMenu &);
    void selectProject();
private slots:
    void actSetAsMainDlg(bool checked = false);
    void actDuplicateDlg(bool checked = false);
    void actRenameDlg(bool checked = false);
    void actDelete(bool checked = false);
};

class sourceWnd;
class propertiesWndClass;
class abstractUICCtrl;
class frameDlg;



enum eSelectionState
{
    selectionNone = 0,
    selectionStarted,
    selectionEnd
};

class childWndDlg : public abstractChildWnd, public projectItem
{
    Q_OBJECT
private:
    wdgSrcEditor * srcEdPanel;
    wdgProp *propPanel;

    eSelectionState vSelectionState;
    int xInit;
    int yInit;
    int selWidth;
    int selHeight;
    /* Representa el panel contenido. */
    frameDlg * frm;
    QToolBar * topBar;

protected:
    void closeEvent(QCloseEvent * closeEvent);

public:    
    QList<abstractUICCtrl *> selectedWidgets;
    frameDlg * getFrame() {return this->frm;}
    void setSelectionState(eSelectionState newState);
    eSelectionState selectionState();    
    void setSelXInit(int x) {this->xInit = x;       }
    int getSelXInit(void)   {return this->xInit;    }

    void setSelYInit(int y) {this->yInit = y;       }
    int getSelYInit(void)   {return this->yInit;    }

    void setSelWidth(int w) {this->selWidth = w;    }
    int getSelWidth(void)   {return this->selWidth; }

    void setSelHeight(int h){this->selHeight = h;   }
    int getSelHeight(void)  {return this->selHeight;}

    childWndDlg(QWidget *parent, Qt::WindowFlags flags,
                wdgSrcEditor * srcEdPanel, wdgProp *propPanel, guiProject *prj);

    virtual bool isModified();

    virtual QString getName();

    virtual void focusOutEvent(QFocusEvent * focusOutEvent);
    virtual void focusInEvent(QFocusEvent * focusInEvent);
    virtual ~childWndDlg();

    wdgSrcEditor  * getSrcWnd(){return this->srcEdPanel;}
    wdgProp * getProPanel(){return this->propPanel;}

    void save();
    void save(QString);
    void saveAs();
    void load(QString fn);
    void run(QString path);
    void exportAsScript(QString path);
    void activate(int line = -1, int col = -1);
    void closingProject();

    virtual abstractPrjTreeItem * treeItemWidget(abstractPrjTreeItem *treeItem);

    childWndDlg * clone(QWidget *parent, Qt::WindowFlags flags,
                        wdgSrcEditor * srcEdPanel, wdgProp *propPanel, guiProject *prj);


    /* Métodos asociados a la selección de controles */
    void addToSelection(abstractUICCtrl *ctrToAdd);
    bool isSelected(abstractUICCtrl *ctrl);
    void removeFromSelection(abstractUICCtrl *ctrl);
    void deselectAllUIC();
    void selectionChanged();

    void moveSelection(int dx, int dy);
    void deleteSelection();
    void resizeSelection(int dx, int dy);

private slots:
    void alignHDlg();
    void alignVDlg();

    void alignToLeft();
    void alignToRight();
    void alignToHCenter();
    void alignToTop();
    void alignToVCenter();
    void alignToBottom();
    void widthToSmall();
    void widthToLarge();
    void heightToSmall();
    void heightToLarge();
    void ShowContextMenu(const QPoint &pos);
    void changeParent();
};

#endif // CHILDWND_H
