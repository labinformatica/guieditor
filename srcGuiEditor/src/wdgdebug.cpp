/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgdebug.h"
#include "ui_wdgdebug.h"
#include <QClipboard>
#include <QDebug>
#include "consolelogitem.h"
#include <QListWidgetItem>
#include <QDir>
#include "mainwnd.h"

void wdgDebug::showStdErr()
{
    QBrush fondo;
    QFont font;
    ConsoleLogItem * newItem;
    QRegularExpression error_type_01("^(error: parse error near line )(?<line>[1-9]+[0-9]*)( of file )(?<file>(([a-zA-Z]:)|((\\\\|/){1,2}\\w+)\\$?)((\\\\|/)(\\w[\\w ]*.*))+\\.([a-zA-Z0-9]+))");
    QRegularExpression error_type_02("^(    )(?<function>\\w+)( at line )(?<line>[1-9]+[0-9]*) column (?<column>[1-9]+[0-9]*)");
    QRegularExpressionMatch found_type_01;
    QRegularExpressionMatch found_type_02;
    QString line;

    fondo.setStyle(Qt::SolidPattern);
    line = QString(this->octProc->readAllStandardError());
    QStringList lst = line.split('\n', QString::SkipEmptyParts);
    for(int i=0; i < lst.count(); i++)
        if(lst[i].length()){
            newItem = new ConsoleLogItem(mainWnd::getPrjWnd()->activeProject(), ui->lstConsoleOut);
            found_type_01 = error_type_01.match(lst[i]);
            found_type_02 = error_type_02.match(lst[i]);
            if(found_type_01.hasMatch()){
                font = newItem->font();
                font.setItalic(true);
                newItem->setFont(font);
                newItem->setText(">> " + lst[i]);
                newItem->setFile(found_type_01.captured("file"));
                newItem->setLine(found_type_01.captured("line").toInt());
                ui->lstConsoleOut->addItem(newItem);
            }
            else if(found_type_02.hasMatch() && (found_type_02.captured("function") != "runApp") ){
                font = newItem->font();
                font.setItalic(true);
                newItem->setFont(font);
                newItem->setText(">> " + lst[i]);
                newItem->setFunction(found_type_02.captured("function"));
                newItem->setLine(found_type_02.captured("line").toInt());
                newItem->setColumn(found_type_02.captured("column").toInt());
                ui->lstConsoleOut->addItem(newItem);
            }
            else{
                newItem->setText(">> " + lst[i]);
                ui->lstConsoleOut->addItem(newItem);
            }
        }
    ui->lstConsoleOut->scrollToBottom();
}

void wdgDebug::showStdOut()
{
    QBrush fondo;
    QFont font;
    ConsoleLogItem * newItem;
    QRegularExpression error_type_01("^(error: parse error near line )(?<line>[1-9]+[0-9]*)( of file )(?<file>(([a-zA-Z]:)|((\\\\|/){1,2}\\w+)\\$?)((\\\\|/)(\\w[\\w ]*.*))+\\.([a-zA-Z0-9]+))");
    QRegularExpression error_type_02("^(     )(?<function>\\w+)( at line )(?<line>[1-9]+[0-9]*) column (?<column>[1-9]+[0-9]*)");
    QRegularExpressionMatch found_type_01;
    QRegularExpressionMatch found_type_02;
    QStringList lines;
    fondo.setColor(Qt::red);
    fondo.setStyle(Qt::SolidPattern);
    lines= QString(this->octProc->readAllStandardOutput()).split('\n', QString::SkipEmptyParts);
    for(int i=0; i < lines.count(); i++){
        newItem = new ConsoleLogItem(mainWnd::getPrjWnd()->activeProject(), ui->lstConsoleOut);
        if(lines[i].length()){
            found_type_01 = error_type_01.match(lines[i]);
            found_type_02 = error_type_02.match(lines[i]);
            if(found_type_01.hasMatch()){
                font = newItem->font();
                font.setItalic(true);
                newItem->setFont(font);
                newItem->setText(">> " + lines[i]);
                newItem->setFile(found_type_01.captured("file"));
                newItem->setLine(found_type_01.captured("line").toInt());
                ui->lstConsoleOut->addItem(newItem);
            }
            else if(found_type_02.hasMatch() ){
                font = newItem->font();
                font.setItalic(true);
                newItem->setFont(font);
                newItem->setText(">> " + lines[i]);
                newItem->setFunction(found_type_02.captured("function"));
                newItem->setLine(found_type_02.captured("line").toInt());
                newItem->setColumn(found_type_02.captured("column").toInt());
                ui->lstConsoleOut->addItem(newItem);
            }
            else{
                newItem->setText(">> " + lines[i]);                
                ui->lstConsoleOut->addItem(newItem);
            }
        }
    }
    ui->lstConsoleOut->scrollToBottom();
}
void wdgDebug::availData()
{
    QString line;
    lineOutput = lineOutput + QString(this->octProc->readAll());
    while(lineOutput.indexOf('\n') != -1)
    {
        line = lineOutput.left(lineOutput.indexOf('\n'));
        lineOutput.remove(0, lineOutput.indexOf('\n'));
        line.remove('\n');
        if(line.length())
            ui->lstConsoleOut->addItem(line);
    }
}

wdgDebug::wdgDebug(QProcess * octProc, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wdgDebug)
{
    this->octProc = octProc;
    ui->setupUi(this);
}

wdgDebug::~wdgDebug()
{
    delete ui;
}

void wdgDebug::on_btnExec_clicked()
{
    if(this->octProc)
    {
        if(this->octProc->isOpen())
        {
            this->octProc->write((ui->leCMD->text()+"\n").toLatin1());
            this->ui->lstConsoleOut->addItem("<< " + ui->leCMD->text());
            ui->leCMD->clear();
        }
        else
        {
            ui->lstConsoleOut->addItem(tr("Octave is not running!"));
        }
    }
    else
    {
        ui->lstConsoleOut->addItem("Octave is not running!");
    }
}

void wdgDebug::on_btnCopyOutput_clicked()
{
    QList<QListWidgetItem *> selection = ui->lstConsoleOut->selectedItems();
    QList<QListWidgetItem *>::iterator it;
    QString txt;
    for(it = selection.begin(); it != selection.end(); it++)
    {
        txt = txt + (*it)->text() + "\r\n";
    }
    qApp->clipboard()->setText(txt);
}

void wdgDebug::on_btnClearConsole_clicked()
{
    ui->lstConsoleOut->clear();
}

void wdgDebug::on_lstConsoleOut_itemDoubleClicked(QListWidgetItem *item)
{
    ConsoleLogItem * error = (ConsoleLogItem *) item;
    QList<projectItem *>::iterator it;
    if(error->project()){
        /* Localiza errores de parser */
        if(!error->file().isEmpty()){
            for(it = error->project()->items.begin(); it != error->project()->items.end(); it++){
                if((*it)->getName() == error->file().right(error->file().length() - (error->file().lastIndexOf(QDir::separator()) + 1))){
                    (*it)->activate(error->line(), error->column());
                }
                else if((*it)->getName() + "_def.m"== error->file().right(error->file().length() - (error->file().lastIndexOf(QDir::separator()) + 1))){
                    (*it)->activate(error->line(), error->column());
                }
            }
        }
        /* Otros tipos de errores identificados a través de las funciones involucradas */
        else if(!error->function().isEmpty()){
            QString doIt = error->function().right(error->function().length() - (error->function().lastIndexOf("_") + 1));
            QString dlgName = error->function().left((error->function().indexOf("_")));
            for(it = error->project()->items.begin(); it != error->project()->items.end(); it++){
                /* Identifica un archivo .m con una función */
                if((*it)->getName() == error->function() + ".m"){
                    (*it)->activate(error->line(), error->column());
                } /*
                     Identifica un evento <nombreVentana>_<nombreControl>_doit o
                     la función que hace visible la ventana show_<nombreVentana>
                  */
                else if((((*it)->getName() == dlgName) && (doIt == "doIt"))|| ("show_" + (*it)->getName() == error->function()) ){
                    (*it)->activate(error->line(), error->column());
                }
            }
        }
    }
}
