--
-- Get adc value for vcc input
--
local retCmd = {}
local function doCmd(sm, sck)    
  val = adc.readvdd33();
  allValue = val;
  byte1 = allValue % 256;
  allValue = allValue / 256;
  byte2 = allValue % 256;
  disp:clearBuffer()
  dofile("drawCmdBase.lua")(10)
  local str = "Read adc"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str)
  str = "Value " .. val
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str)
  disp:sendBuffer()

  sck:send(string.char(byte1));
  sck:send(string.char(byte2));
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
