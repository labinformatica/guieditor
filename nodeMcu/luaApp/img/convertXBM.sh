#!/bin/bash
echo "Convirtiendo $1.png a $1.xbm"
if test -f $1".xbm"
then
  rm $1".xbm"
fi

convert +antialias $1".png" -depth 1 $1"_1bpp.png"
convert +antialias $1"_1bpp.png" -background white -alpha Background $1"_1bpp.xbm"
cat $1"_1bpp.xbm" | tr '\n' ' '  | tr -d " " |sed  -e s'#^.*{##g'  | sed s'#,}##' |sed s'/;//' | xxd -r -p > $1".xbm.mono"
rm $1"_1bpp.png"
rm $1"_1bpp.xbm"
mv $1".xbm.mono" $1".xbm"