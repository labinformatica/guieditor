--
--  Reset the ESP8266. Wait 3 sec (prior to do the 
--  reset) to show the state as display
--
local retCmd = {}

local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(12)
  local str = "Reset"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str);

  str = "Wait until reboot"
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()

  connTimer = tmr.create()
  connTimer:register(3000, 
    tmr.ALARM_AUTO, 
    function()                      
      node.restart()
    end)
  connTimer:start()
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
