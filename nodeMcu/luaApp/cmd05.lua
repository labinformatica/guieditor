--
--  Configure duty cicle and clock for pin with pwm
--
local retCmd = {}
local function doCmd(sm, sck)    
  frec = (sm["data"][2] * 256) + sm["data"][3];
  duty = (sm["data"][4] * 256) + sm["data"][5];

  dofile("drawCmdBase.lua")(5)
  local str = "Set pwm val"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 22, str);

  str = "Pin: " .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 36, str);

  str = "F:" .. frec .. " | " .. "D:" .. duty
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 50, str);

  disp:sendBuffer()

  pwm.setduty(sm["data"][1], duty);
  pwm.setclock(sm["data"][1], frec);
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
