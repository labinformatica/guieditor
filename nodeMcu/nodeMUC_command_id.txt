CMD	Operation
0x01	Setup pin
0x02	Get pin state
0x03	Set pin state
0x04	Setup pwm
0x05	Set pwm clock y duty cicle
0x06	Start pwm
0x07	Stop pwm
0x08	Setup adc mode
0x09	Get adc input
0x0A	Get adc vcc value
0x0B	Get DS18B20 value connected at pin 7
0x0C    Restart nodeMCU (execute node.restart())
0x0D    Erase the network configuration.
