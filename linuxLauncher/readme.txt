In this folder you will find the guiEditor launcher called "guiEditor.desktop"
This file should be modified based on where the binary file is located and where you copied its icon. The icon is distributed together with the source code and can be found in the icons folder.
Once the paths to the executable and the icon have been modified, you must copy the previous file to the path where your distribution stores the launchers. This is usually: / usr / share / applications 


